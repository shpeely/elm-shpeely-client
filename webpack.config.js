const path = require('path');
const webpack = require('webpack');
require('dotenv').config();

const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const elmMinify = require("elm-minify")
const CopyWebpackPlugin = require('copy-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin')
const { GenerateSW } = require('workbox-webpack-plugin');

const prod = 'production';
const dev = 'development';

// determine build env
const MODE = process.env.npm_lifecycle_event === 'dist' ? prod : dev;
const isDev = MODE === dev;
const isProd = MODE === prod;

// entry and output path/filename variables
const entryPath = path.join(__dirname, 'src/index.js');
const outputPath = path.join(__dirname, 'dist');
const outputFilename = isProd ? '[name]-[hash].js' : '[name].js'

console.log('Webpack: Building for ' + MODE);

// common webpack config (valid for dev and prod)
const commonConfig = {
    mode: MODE,
    output: {
        path: outputPath,
        filename: `static/js/${outputFilename}`,
        publicPath: '/',
    },
    resolve: {
        extensions: ['.js', '.elm', '.scss', '.png'],
        modules: [path.join(__dirname, "src"), "node_modules"],
    },
    module: {
        rules: [{
            test: /\.(eot|ttf|woff|woff2|svg)$/,
            use: 'file-loader?publicPath=../../&name=static/css/[hash].[ext]'
        }, {
            test: /\.js$/,
            exclude: [/node_modules/],
            use: [{
                loader: 'babel-loader',
                options: {presets: ['es2015']},
            }],
        }, {
            test: /\.scss$/,
            exclude: [/elm-stuff/, /node_modules/],
            // see https://github.com/webpack-contrib/css-loader#url
            loaders: ["style-loader", "css-loader?url=false", "sass-loader"]
        }, {
            test: /\.css$/,
            exclude: [/elm-stuff/, /node_modules/],
            loaders: ["style-loader", "css-loader?url=false", "postcss-loader"]
        }, {
            test: /\.(jpe?g|png|gif|svg)$/i,
            exclude: [/elm-stuff/, /node_modules/],
            loader: "file-loader"
        }]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'src/index.html',
            inject: 'body',
            filename: 'index.html'
        }),
        new webpack.EnvironmentPlugin([
            'AUTH0_CLIENT_ID',
            'AUTH0_DOMAIN',
            'SHPEELY_API_URL',
            'GAME_STATS_API_URL',
            'GRAPHQL_URL',
            'VERSION',
        ])
    ]
}

// additional webpack settings for local env (when invoked by 'npm start')
if (isDev === true) {
    module.exports = merge(commonConfig, {
        entry: [
            'webpack-dev-server/client?http://localhost:9000',
            entryPath
        ],
        devServer: {
            inline: true,
            stats: "errors-only",
            contentBase: './src',
            historyApiFallback: true,
            port: process.env.PORT || '9000',
            host: '0.0.0.0',
            disableHostCheck: true,
            proxy: {
                '/graphql': {
                    target: process.env.GRAPHQL_PROXY_HOST || 'http://localhost:4000',
                    secure: false,
                    pathRewrite: {
                        '^/graphql': process.env.GRAPHQL_PROXY_REWRITE || '/'
                    },
                    changeOrigin: true
                },
                '/game-stats': {
                    target: process.env.GAME_STATS_PROXY_HOST || 'http://localhost:5000',
                    secure: false,
                    pathRewrite: {
                        '^/game-stats': process.env.GAME_STATS_PROXY_REWRITE || '/'
                    },
                    changeOrigin: true
                },
            },

        },
        module: {
            rules: [{
                test: /\.elm$/,
                exclude: [/elm-stuff/, /node_modules/],
                use: [{
                    loader: 'elm-webpack-loader',
                    options: {
                        // verbose: true,
                        debug: true,
                        // runtimeOptions: '-s -A128M -H128M -n8m',
                        pathToElm: '/usr/bin/elm'
                    }
                }]
            }]
        },
    });
}

// additional webpack settings for prod env (when invoked via 'npm run build')
if (isProd === true) {
    module.exports = merge(commonConfig, {
        entry: entryPath,
        optimization: {
            minimize: true,
        },
        module: {
            rules: [{
                test: /\.elm$/,
                exclude: [/elm-stuff/, /node_modules/],
                use: {
                    loader: 'elm-webpack-loader',
                    options: {
                        optimize: true,
                        pathToElm: 'node_modules/elm/bin/elm'
                    }
                }
            }, {
                test: /\.css$/,
                exclude: [/elm-stuff/, /node_modules/],
                loaders: [
                    MiniCssExtractPlugin.loader,
                    "css-loader?url=false",
                    "postcss-loader"
                ]
            }, {
                test: /\.scss$/,
                exclude: [/elm-stuff/, /node_modules/],
                loaders: [
                    MiniCssExtractPlugin.loader,
                    "css-loader?url=false",
                    "sass-loader",
                    "postcss-loader"
                ]
            }]
        },
        plugins: [
            // Minify elm code
            new elmMinify.WebpackPlugin(),
            // Delete everything from /dist directory and report to user
            new CleanWebpackPlugin(["dist"], {
                root: __dirname,
                exclude: [],
                verbose: true,
                dry: false
            }),
            // Copy static assets
            new CopyWebpackPlugin([
                {
                    from: "src/static"
                }
            ]),
            new MiniCssExtractPlugin({
                // Options similar to the same options in webpackOptions.output
                // both options are optional
                filename: "[name]-[hash].css"
            }),
            new CopyWebpackPlugin([{
                from: 'src/static/',
                to: 'static/'
            }]),
            new FaviconsWebpackPlugin({
                logo: './src/static/favicon/favicon.svg',
                prefix: 'static/favicon/icons-[hash]/',
                title: 'Shpeely',
                background: '#2C3E50',
                emitStats: false,
                persistentCache: true,
                android: false,
                opengraph: true,
                twitter: true,
                yandex: true,
                windows: true
            }),
            new GenerateSW({
                swDest: 'sw.js',
                clientsClaim: true,
                skipWaiting: true,
            })
        ]
    });
}
