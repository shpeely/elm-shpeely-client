module TestUtils exposing (..)

import Expect exposing (Expectation)


expectOk : Result err a -> Expectation
expectOk result =
    case result of
        Ok _ ->
            Expect.pass

        Err err ->
            Expect.fail ("Expected Ok but was Err: " ++ toString err)


expectErr : Result err a -> Expectation
expectErr result =
    case result of
        Ok res ->
            Expect.fail ("Expected Err but was Ok: " ++ toString res)

        Err err ->
            Expect.pass
