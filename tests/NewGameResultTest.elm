module NewGameResultTest exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, list, int, string)
import Test exposing (..)
import Model.NewGameResult as Sut exposing (..)
import RemoteData exposing (RemoteData(NotAsked))
import Result exposing (Result(..))
import TestUtils


initialGameResult : NewGameResult
initialGameResult =
    { gameInfo = NotAsked
    , scores = []
    , playerSearchResults = []
    , bggid = Just 123
    }


suite : Test
suite =
    describe "New Game Result Model Validation"
        [ test "Should fail if there are no scores" <|
            \_ ->
                let
                    scores =
                        []

                    gameResult =
                        { initialGameResult | scores = scores }
                in
                    Expect.equal (Result.Err [ NoScoresError ]) <| Sut.validateGameResult gameResult
        , test "Should fail if the score of a player is missing" <|
            \_ ->
                let
                    scores =
                        [ ( 0, { player = Just (NewPlayer "player 1"), score = Nothing } )
                        , ( 1, { player = Just (NewPlayer "player 2"), score = Just 20 } )
                        , ( 2, { player = Just (NewPlayer "player 3"), score = Nothing } )
                        ]

                    gameResult =
                        { initialGameResult | scores = scores }
                in
                    Expect.equal (Result.Err [ MissingScoreError [ 0, 2 ] ]) <| Sut.validateGameResult gameResult
        , test "Should fail if the player is missing to a score" <|
            \_ ->
                let
                    scores =
                        [ ( 0, { player = Nothing, score = Just 10 } )
                        , ( 1, { player = Just (NewPlayer "player 2"), score = Just 20 } )
                        , ( 2, { player = Nothing, score = Just 30 } )
                        ]

                    gameResult =
                        { initialGameResult | scores = scores }
                in
                    Expect.equal (Result.Err [ MissingPlayerError [ 0, 2 ] ]) <| Sut.validateGameResult gameResult
        , test "Should fail if two players have the same name" <|
            \_ ->
                let
                    scores =
                        [ ( 0, { player = Just (NewPlayer "duplicate"), score = Just 10 } )
                        , ( 1, { player = Just (NewPlayer "player 2"), score = Just 20 } )
                        , ( 2, { player = Just (NewPlayer "duplicate"), score = Just 30 } )
                        ]

                    gameResult =
                        { initialGameResult | scores = scores }
                in
                    Expect.equal (Result.Err [ DuplicatePlayerNameError [ 0, 2 ] ]) <| Sut.validateGameResult gameResult
        , test "Should fail if the bggid is missing" <|
            \_ ->
                let
                    scores =
                        [ ( 0, { player = Just (NewPlayer "player 1"), score = Just 10 } )
                        ]

                    gameResult =
                        { initialGameResult | bggid = Nothing, scores = scores }
                in
                    Expect.equal (Result.Err [ MissingBggIdError ]) <| Sut.validateGameResult gameResult
        ]
