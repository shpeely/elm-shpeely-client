FROM node:11.1.0

RUN mkdir /build
WORKDIR /build

# install elm compiler
RUN yarn global add elm@0.19.0

COPY .env yarn.lock package.json elm.json webpack.config.js postcss.config.js /build/
COPY src /build/src

# install dependnecies
RUN yarn install

# configure
ARG AUTH0_CLIENT_ID=jy9qp3TywJNnReUKSXA1uWG4Tx1T5lhw
ARG AUTH0_DOMAIN=shpeely.auth0.com
ARG SHPEELY_API_URL=/api
ARG GAME_STATS_API_URL=/game-stats
ARG GRAPHQL_URL=/graphql

ARG VERSION=latest

# build and clean up
RUN set -xea \
    && VERSION=${VERSION} yarn dist \
    && mv dist /app \
    && rm -rf /build

FROM nginx:1.15.3

COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=0 /app /usr/share/nginx/html