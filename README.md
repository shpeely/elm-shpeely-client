# Elm Shpeely Client

### Installation

```sh
npm run setup
```

### Application Bootstrapping

The bootstrapping of the application is kind of complex and not immediately understandable from the code. This is a flowchart of how the application is bootstrapped:



![Elm Shpeely Client Application Bootstrapping Flowchart](https://www.lucidchart.com/publicSegments/view/f0bfd9be-56ec-4302-b496-5b1199ae471f/image.png "Application Bootstrapping Flowchart")
