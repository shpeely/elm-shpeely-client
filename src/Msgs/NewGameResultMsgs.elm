module Msgs.NewGameResultMsgs exposing (NewGameResultMsg(..))

import Components.LiveDropdown as LiveDropdown
import Debounce
import Model.GraphQL exposing (GraphQLCreateGameResultResponse, GraphQLGameInfoResponse, GraphQLGameSearchResponse)


type NewGameResultMsg
    = OnPlayerInput Int String
    | DebounceMsg Debounce.Msg
    | OnGameSearch String
    | OnGameSelected Int
    | OnPlayerSelected Int String
    | OnScoreInput Int String
    | OnSubmit
    | OnGameSearchResults GraphQLGameSearchResponse
    | OnResultSaved GraphQLCreateGameResultResponse
    | OnGameInfoLoaded GraphQLGameInfoResponse
    | NewResultLiveDropdownMsg (LiveDropdown.State NewGameResultMsg)
    | NewResultNoop
