module Msgs.BootstrapMsgs exposing (BootstrapMsg(..))

import Bootstrap.Carousel as Carousel
import Bootstrap.Dropdown as Dropdown


type BootstrapMsg
    = PlayerFilterDropwdownToggle Dropdown.State
    | TimeFilterDropwdownToggle Dropdown.State
    | NumPlayersDropdownToggle Dropdown.State
    | GameOrderDropdownToggle Dropdown.State
    | ToggleResultMoreDropdown String Dropdown.State
    | ToggleMemberRoleDropdown String Dropdown.State
    | CarouselMsg Carousel.Msg
