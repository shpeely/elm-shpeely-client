module Msgs.PlayersViewMsgs exposing (PlayersViewMsg(..))

import Table


type PlayersViewMsg
    = SetTableState Table.State
