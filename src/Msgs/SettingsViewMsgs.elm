module Msgs.SettingsViewMsgs exposing (SettingsViewMsg(..))

import Bootstrap.Dropdown as Dropdown
import Model.GraphQL exposing (GraphQLLeagueMemberResponse)
import Model.Invitation exposing (Invitation)
import Model.League exposing (League, MemberRole)
import Model.SettingsView exposing (..)
import RemoteData exposing (RemoteData)


type SettingsViewMsg
    = OnInvitationEmailInput String
    | OnSubmitInvitation League
    | OnInvitationCreated Invitation
    | OnChangeMemberRole String MemberRole
    | MemberRoleUpdated GraphQLLeagueMemberResponse
