module Msgs.GamesViewMsgs exposing (GamesViewMsg(..))

import Model.GamesView exposing (..)


type GamesViewMsg
    = SetOrder GamesSortOrder
    | SetFilter GamesFilter
