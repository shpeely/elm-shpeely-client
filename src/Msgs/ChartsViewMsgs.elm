module Msgs.ChartsViewMsgs exposing (ChartsViewMsg(..))

import Model.GraphQL exposing (GraphQLGameResultResponse)
import Model.Stats exposing (PlayerFilter, TimeFilter)


type ChartsViewMsg
    = SelectPlayerFilter PlayerFilter
    | SelectPlayer String Bool
    | SelectAllPlayers
    | UnselectAllPlayers
    | SetTimeFilter TimeFilter
    | RecentGameResultsResponse GraphQLGameResultResponse
