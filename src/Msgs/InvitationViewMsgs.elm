module Msgs.InvitationViewMsgs exposing (InvitationViewMsg(..))

import Model.Invitation exposing (Invitation, InvitationResponse)


type InvitationViewMsg
    = AcceptInvitation Invitation
    | RejectInvitation Invitation
