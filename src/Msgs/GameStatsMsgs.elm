module Msgs.GameStatsMsgs exposing (GameStatsMsg(..))

import Model.Stats exposing (..)
import RemoteData exposing (WebData)


type GameStatsMsg
    = TimeSeriesResponse (WebData TimeSeries)
    | LoadGameResultStats String (List String)
    | GameResultStatsResponse (WebData GameResultStats)
    | GameStatsResponse (WebData (List GameStats))
    | PlayerStatsResponse (WebData (List PlayerStats))
