module Msgs.LeagueMsgs exposing (LeagueMsg(..))


type LeagueMsg
    = OnLeagueNameChange String
    | OnNewLeagueSubmit
