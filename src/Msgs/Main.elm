module Msgs.Main exposing (Msg(..))

import Bootstrap.Modal as Modal
import Bootstrap.Navbar as Navbar
import Browser exposing (UrlRequest)
import Browser.Dom exposing (Viewport)
import Components.LiveDropdown as LiveDropdown
import GraphQL.Client.Http as GraphQLClient
import Model.BasicTypes exposing (Size)
import Model.GameResult exposing (GameResultAndLeague)
import Model.GraphQL exposing (..)
import Model.LoginData exposing (LoginProvider)
import Model.Route exposing (Route)
import Model.User exposing (Auth0UserProfile, User)
import Msgs.BootstrapMsgs exposing (BootstrapMsg)
import Msgs.ChartsViewMsgs exposing (ChartsViewMsg)
import Msgs.EditResultViewMsgs exposing (EditResultViewMsg)
import Msgs.GameStatsMsgs exposing (GameStatsMsg)
import Msgs.GamesViewMsgs exposing (GamesViewMsg)
import Msgs.InvitationViewMsgs exposing (InvitationViewMsg)
import Msgs.LeagueMsgs exposing (LeagueMsg)
import Msgs.LoginModalMsgs exposing (LoginModalMsg)
import Msgs.NewGameResultMsgs exposing (NewGameResultMsg)
import Msgs.PlayersViewMsgs exposing (PlayersViewMsg)
import Msgs.ResultDetailsViewMsgs exposing (ResultDetailsViewMsg)
import Msgs.ResultsViewMsgs exposing (ResultsViewMsg)
import Msgs.SettingsViewMsgs exposing (SettingsViewMsg)
import RemoteData exposing (WebData)
import Time exposing (Posix)
import Url exposing (Url)


type Msg
    = UrlChange Url.Url
    | LinkClicked UrlRequest
    | NavigateTo Route
    | OnLoginDataLoaded (Maybe String)
    | OnRedirectUrlLoaded (Maybe String)
    | OnSaveRedirectUrl String
    | OnGameResultUpdate String
    | OnGameResultDeleted String
    | Login LoginProvider
    | CheckSession
    | Logout
    | OnInternetDisconnect
    | OnInternetConnect
    | Auth0UserProfileResponse (WebData Auth0UserProfile)
    | NavbarMsg Navbar.State
    | OnNow Posix
    | OnViewport Viewport
    | ModalMsg Modal.Visibility
    | LeagueMsg LeagueMsg
    | GameStatsMsg GameStatsMsg
    | NewGameResultMsgs NewGameResultMsg
    | ResultsViewMsgs ResultsViewMsg
    | GamesViewMsgs GamesViewMsg
    | PlayersViewMsgs PlayersViewMsg
    | ChartsViewMsgs ChartsViewMsg
    | InvitationViewMsgs InvitationViewMsg
    | SettingsViewMsgs SettingsViewMsg
    | ResultDetailsViewMsgs ResultDetailsViewMsg
    | EditResultViewMsgs EditResultViewMsg
    | BootstrapMsgs BootstrapMsg
    | OnTime Posix
    | LiveDropdownMsg (LiveDropdown.State Msg)
    | GameResultsResponse GraphQLGameResultResponse
    | UserIdResponse GraphQLUserIdResponse
    | DeleteGameResultResponse GraphQLDeleteGameResultResponse
    | CreateLeagueResponse GraphQLCreateLeagueResponse
    | CreateInvitationResponse GraphQLCreateInvitationResponse
    | LeagueResponse GraphQLLeagueResponse
    | RecentLeaguesResponse GraphQLRecentLeaguesResponse
    | RecentResultsResponse (Result GraphQLClient.Error (List GameResultAndLeague))
    | MyLeaguesResponse GraphQLMyLeaguesResponse
    | GameResultMetaResponse GraphQLGameResultMetaResponse
    | GamesResponse GraphQLGamesResponse
    | InvitationResponse GraphQLInvitationResponse
    | WindowResize Size
    | LoginModalMsgs LoginModalMsg
    | Noop
