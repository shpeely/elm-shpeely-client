module Msgs.LoginModalMsgs exposing (LoginModalMsg(..))

import Bootstrap.Modal as Modal


type LoginModalMsg
    = CloseModal
    | ShowModal
    | AnimateModal Modal.Visibility
