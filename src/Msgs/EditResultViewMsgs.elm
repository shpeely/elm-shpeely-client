module Msgs.EditResultViewMsgs exposing (EditResultViewMsg(..))

import Components.LiveDropdown as LiveDropdown
import Debounce
import Model.GraphQL exposing (GraphQLGameInfoResponse, GraphQLGameResultDetailsResponse, GraphQLGameSearchResponse)


type EditResultViewMsg
    = ResultDetailsLoaded GraphQLGameResultDetailsResponse
    | GameInfoLoaded GraphQLGameInfoResponse
    | OnGameSearch String
    | OnGameSearchResults GraphQLGameSearchResponse
    | OnGameSelected Int
    | OnPlayerInput Int String
    | OnPlayerSelected Int String
    | OnScoreInput Int String
    | DebounceMsg Debounce.Msg
    | OnSubmit
    | EditResultLiveDropdownMsg (LiveDropdown.State EditResultViewMsg)
    | EditResultNoop
    | OnChangesSaved GraphQLGameResultDetailsResponse
