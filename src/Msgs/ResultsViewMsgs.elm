module Msgs.ResultsViewMsgs exposing (ResultsViewMsg(..))

import Bootstrap.Dropdown as Dropdown
import Model.GraphQL exposing (GraphQLGameResultResponse, GraphQLGameSearchResponse)


type ResultsViewMsg
    = GameResultFilterInput String
    | GameResultFilterPlayerInput Int String
    | GameResultFilterPlayerSelected String String
    | GameResultFilterRemovePlayer String
    | SetNumberOfPlayersFilter (Maybe Int)
    | OnSamePlayerCheck Bool
    | ClearPlayerFilter
    | GameFilterSelected String
    | ClearGameFilter
    | DeleteResultConfirmation String
    | DeleteResult String
    | CancelDelete String
    | GameResultsLoaded GraphQLGameResultResponse
