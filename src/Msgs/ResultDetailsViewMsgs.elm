module Msgs.ResultDetailsViewMsgs exposing (ResultDetailsViewMsg(..))

import Model.GraphQL exposing (GraphQLGameInfoResponse, GraphQLGameResultDetailsResponse)


type ResultDetailsViewMsg
    = ResultDetailsLoaded GraphQLGameResultDetailsResponse
    | GameInfoLoaded GraphQLGameInfoResponse
