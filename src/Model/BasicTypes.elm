module Model.BasicTypes exposing (Size)


type alias Size =
    { width : Int
    , height : Int
    }
