module Model.GamesView exposing (GamesFilter, GamesSortOrder(..), GamesViewModel, initialState)


type alias GamesViewModel =
    { sortOrder : GamesSortOrder
    , filter : GamesFilter
    }


type GamesSortOrder
    = NameAsc
    | NameDesc
    | PlayTimeAsc
    | PlayTimeDesc
    | WeightAsc
    | WeightDesc
    | NumberOfGamesAsc
    | NumberOfGamesDesc
    | MaxPlayersAsc
    | MaxPlayersDesc


type alias GamesFilter =
    { name : String }


initialState : GamesViewModel
initialState =
    { sortOrder = NameAsc
    , filter =
        { name = ""
        }
    }
