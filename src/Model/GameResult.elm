module Model.GameResult exposing (BggInfo, GameResult, GameResultAndLeague, GameResultMeta, Score, ScoreInput, UnsavedGameResult)

import Model.League as League
import Time exposing (Posix)


type alias UnsavedGameResult =
    { bggid : Int
    , scores : List ScoreInput
    }


type alias GameResult =
    { id : String
    , time : Posix
    , bggInfo : BggInfo
    , scores : List Score
    }


type alias GameResultAndLeague =
    { id : String
    , time : Posix
    , bggInfo : BggInfo
    , scores : List Score
    , league : League.League
    }


type alias Score =
    { player : League.Player
    , score : Float
    }


type alias ScoreInput =
    { playerName : String
    , score : Float
    }


type alias BggInfo =
    { bggid : Int
    , name : String
    }


type alias GameResultMeta =
    { id : String
    , time : Posix
    }
