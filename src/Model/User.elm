module Model.User exposing (Auth0UserProfile, User)


type alias User =
    { auth0id : String
    , avatar : String
    , username : String
    }


type alias Auth0UserProfile =
    { avatar : String
    , username : String
    }
