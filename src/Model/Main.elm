module Model.Main exposing (Model)

import Bootstrap.Modal as Modal
import Bootstrap.Navbar as Navbar
import Browser.Navigation exposing (Key)
import Components.LiveDropdown as LiveDropdown
import Debounce exposing (Debounce)
import Dict exposing (Dict)
import Model.Bootstrap exposing (BootstrapState)
import Model.ChartsView exposing (ChartsViewModel)
import Model.EditResultView exposing (EditResultViewModel)
import Model.Flags exposing (Flags)
import Model.GameResult exposing (GameResult, GameResultAndLeague, GameResultMeta)
import Model.GamesView exposing (GamesViewModel)
import Model.GraphQL exposing (GraphQLData, GraphQLError)
import Model.Invitation exposing (Invitation)
import Model.League exposing (Game, GameInfo, League, LeagueSummary, UnsavedLeague)
import Model.LoginData exposing (LoginStatus)
import Model.LoginModal exposing (LoginModal)
import Model.NewResultView exposing (NewResultViewModel)
import Model.PlayersView exposing (PlayersViewModel)
import Model.ResultDetailsView exposing (ResultDetailsViewModel)
import Model.ResultsView exposing (ResultsViewModel)
import Model.Route exposing (Route)
import Model.SettingsView exposing (SettingsViewModel)
import Model.Stats exposing (ChartSettings, GameResultStats, GameStats, PlayerStats, TimeSeries)
import Model.User exposing (Auth0UserProfile, User)
import Msgs.Main exposing (Msg)
import RemoteData exposing (RemoteData, WebData)
import Time exposing (Posix)
import Url


type alias Model =
    { createLeagueError : Maybe GraphQLError
    , windowSize : { width : Int, height : Int }
    , currentTime : Posix
    , bootstrapState : BootstrapState
    , url : Url.Url
    , key : Key
    , route : Route
    , flags : Flags
    , userId : Maybe String
    , auth0Profile : WebData Auth0UserProfile
    , recentLeagues : GraphQLData (List LeagueSummary)
    , recentResults : GraphQLData (List GameResultAndLeague)
    , myLeagues : GraphQLData (List LeagueSummary)
    , loginStatus : LoginStatus
    , navbarState : Navbar.State
    , newLeague : UnsavedLeague
    , gameSearch : { value : String, debounce : Debounce String }
    , liveDropdownState : LiveDropdown.State Msg
    , league : GraphQLData League
    , invitation : GraphQLData (Maybe Invitation)
    , timeseries : WebData TimeSeries
    , chartSettings : ChartSettings
    , games : GraphQLData (List Game)
    , resultsView : ResultsViewModel
    , gamesView : GamesViewModel
    , playersView : PlayersViewModel
    , settingsView : SettingsViewModel
    , newResultView : NewResultViewModel
    , editResultView : EditResultViewModel
    , chartsView : ChartsViewModel
    , gameResults : GraphQLData (List GameResult)
    , gameResultStats : Dict String (WebData GameResultStats)
    , gameResultsMeta : GraphQLData (Dict String Posix)
    , gameStats : WebData (List GameStats)
    , playerStats : WebData (List PlayerStats)
    , gameInfos : Maybe (List GameInfo)
    , resultDetailsView : ResultDetailsViewModel
    , modalState : Modal.Visibility
    , loginModal : LoginModal
    , error : String
    }
