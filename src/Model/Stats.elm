module Model.Stats exposing (ChartSettings, ExtremeScore, GameResultStats, GameStats, PlayerFilter(..), PlayerStats, ScoreStat, TimeFilter(..), TimeRange, TimeSeries, customDecoder, decodeExtremeScore, decodeGameResultStats, decodeGameStat, decodeGameStats, decodePlayerStats, decodeScoreStat, decodeTimeSeries, eitherR, floatIntDecoder, initialChartSettings, stringIntDecoder)

import Array exposing (Array)
import Dict exposing (Dict)
import Json.Decode as D
import Set exposing (Set)
import Time exposing (Posix)



-- STATS


type alias TimeSeries =
    { results : List String
    , scores : Dict String (Array Int)
    }


type PlayerFilter
    = SelectedPlayers (Set String)
    | RecentPlayers (Set String)
    | AllPlayers


type alias GameResultStats =
    { id : String
    , scores : List ScoreStat
    , average : Float
    , averageThisGame : Float
    , highscore : ExtremeScore
    , lowscore : ExtremeScore
    }


type alias ScoreStat =
    { player : String
    , score : Int
    , points : Int
    }


type alias ExtremeScore =
    { players : List String
    , score : Int
    }


type alias GameStats =
    { bggid : Int
    , count : Int
    , highscores : List String
    , lowscores : List String
    }


type alias PlayerStats =
    { playerId : String
    , numberOfResults : Int
    , wins : Int
    , losses : Int
    , highscores : Int
    , lowscores : Int
    , winRatio : Float
    , loseRatio : Float
    }



-- SETTINGS


type alias TimeRange =
    { from : Posix
    , to : Posix
    }


type TimeFilter
    = AllTime
    | CurrentYear
    | CurrentMonth
    | Year_ Int
    | PastMonths Int
    | PastYears Int
    | SelectedTimeRange TimeRange


type alias ChartSettings =
    { timeFilter : TimeFilter
    , playerFilter : PlayerFilter
    }


initialChartSettings : ChartSettings
initialChartSettings =
    { timeFilter = AllTime
    , playerFilter = AllPlayers
    }



-- DECODERS


decodeTimeSeries : D.Decoder TimeSeries
decodeTimeSeries =
    D.map2 TimeSeries
        (D.field "gameresults" (D.list D.string))
        (D.field "scores" (D.dict (D.array D.int)))


decodeExtremeScore : D.Decoder ExtremeScore
decodeExtremeScore =
    D.map2 ExtremeScore
        (D.field "players" (D.list D.string))
        (D.field "score" floatIntDecoder)


decodeScoreStat : D.Decoder ScoreStat
decodeScoreStat =
    D.map3 ScoreStat
        (D.field "player" D.string)
        (D.field "score" floatIntDecoder)
        (D.field "points" floatIntDecoder)


decodeGameResultStats : D.Decoder GameResultStats
decodeGameResultStats =
    D.map6 GameResultStats
        (D.field "id" D.string)
        (D.field "scores" (D.list decodeScoreStat))
        (D.field "average" D.float)
        (D.field "average_this_game" D.float)
        (D.field "highscore" decodeExtremeScore)
        (D.field "lowscore" decodeExtremeScore)


decodeGameStat : D.Decoder GameStats
decodeGameStat =
    D.map4 GameStats
        (D.field "game_id" stringIntDecoder)
        (D.field "count" D.int)
        (D.field "highscores" (D.list D.string))
        (D.field "lowscores" (D.list D.string))


decodePlayerStat : D.Decoder PlayerStats
decodePlayerStat =
    D.map8 PlayerStats
        (D.field "player" D.string)
        (D.field "number_of_results" D.int)
        (D.field "wins" D.int)
        (D.field "losses" D.int)
        (D.field "highscores" D.int)
        (D.field "lowscores" D.int)
        (D.field "win_ratio" D.float)
        (D.field "lose_ratio" D.float)



--type alias PlayerStats =
--    { player : String
--    , number_of_results : Int
--    , wins : Int
--    , losses : Int
--    , lowscores : Int
--    , win_ratio : Float
--    , lose_ratio : Float
--    }


decodeGameStats : D.Decoder (List GameStats)
decodeGameStats =
    D.list decodeGameStat


decodePlayerStats : D.Decoder (List PlayerStats)
decodePlayerStats =
    D.list decodePlayerStat


floatIntDecoder : D.Decoder Int
floatIntDecoder =
    D.float
        |> D.andThen (round >> D.succeed)


eitherR : (x -> b) -> (a -> b) -> Result x a -> b
eitherR fErr fOk result =
    case result of
        Err x ->
            fErr x

        Ok a ->
            fOk a


customDecoder : (a -> Result String b) -> D.Decoder a -> D.Decoder b
customDecoder fResult decoder =
    decoder |> D.andThen (fResult >> eitherR D.fail D.succeed)


stringIntDecoder : D.Decoder Int
stringIntDecoder =
    let
        stringToInt : String -> D.Decoder Int
        stringToInt int =
            case String.toInt int of
                Just value ->
                    D.succeed value

                Nothing ->
                    D.fail "Cannot convert value to integer."
    in
    D.string |> D.andThen stringToInt
