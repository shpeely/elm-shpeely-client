module Model.LoginModal exposing (LoginModal, initialModel)

import Bootstrap.Modal as Modal


type alias LoginModal =
    { visibility : Modal.Visibility
    }


initialModel : LoginModal
initialModel =
    { visibility = Modal.hidden }
