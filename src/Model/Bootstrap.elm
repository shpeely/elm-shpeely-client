module Model.Bootstrap exposing (BootstrapState, initialState)

import Bootstrap.Carousel as Carousel
import Bootstrap.Dropdown as Dropdown
import Dict exposing (Dict)


type alias BootstrapState =
    { playerFilterDropdownState : Dropdown.State
    , timeFilterDropdownState : Dropdown.State
    , numPlayersFilterDropdownState : Dropdown.State
    , gameOrderDropdownState : Dropdown.State
    , resultDropdownState : Dict String Dropdown.State
    , memberRoleDropdownState : Dict String Dropdown.State
    , carouselState : Carousel.State
    }


initialState : BootstrapState
initialState =
    { playerFilterDropdownState = Dropdown.initialState
    , timeFilterDropdownState = Dropdown.initialState
    , numPlayersFilterDropdownState = Dropdown.initialState
    , gameOrderDropdownState = Dropdown.initialState
    , resultDropdownState = Dict.empty
    , memberRoleDropdownState = Dict.empty
    , carouselState = Carousel.initialState
    }
