module Model.ResultDetailsView exposing (ResultDetailsViewModel, initialState)

import Model.Bgg exposing (GameInfo)
import Model.GameResult exposing (GameResult)
import Model.GraphQL exposing (GraphQLData)
import RemoteData exposing (RemoteData(..))


initialState : ResultDetailsViewModel
initialState =
    { gameResultDetails = NotAsked
    , gameInfo = NotAsked
    }


type alias ResultDetailsViewModel =
    { gameResultDetails : GraphQLData GameResult
    , gameInfo : GraphQLData GameInfo
    }
