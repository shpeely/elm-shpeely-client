module Model.Invitation exposing (Invitation, InvitationResponse(..))

import Model.League exposing (LeagueSummary)
import Model.User exposing (User)


type InvitationResponse
    = Accepted
    | Rejected
    | Pending


type alias Invitation =
    { state : InvitationResponse
    , league : LeagueSummary
    , inviter : User
    }
