module Model.Route exposing (LeagueSubRoute(..), ResultSubRoute(..), Route(..))


type ResultSubRoute
    = ResultDetails
    | EditResult


type LeagueSubRoute
    = AddNewResult
    | Overview
    | Charts
    | Games
    | Players
    | Settings
    | Results
    | ResultRoute String ResultSubRoute


type Route
    = HomeRoute
    | LoginRoute
    | AboutRoute
    | PrivacyRoute
    | OfflineRoute
    | LeagueRoute String LeagueSubRoute
    | InvitationRoute String
    | NotFoundRoute
