module Model.Bgg exposing (GameInfo, GameSearchResult)

import Json.Decode as Decode exposing (..)
import Json.Decode.Pipeline as Pipeline


type alias GameSearchResult =
    { bggid : Int
    , year : Int
    , name : String
    }


type alias GameInfo =
    { bggid : Int
    , id : String
    , name : String
    , weight : Float
    , maxPlayers : Int
    , thumbnail : String
    , description : String
    , rating : Float
    , rank : Int
    , ownedBy : Int
    , playTime : Int
    , yearPublished : Int
    }
