module Model.GraphQL exposing (GraphQLCreateGameResultResponse, GraphQLCreateInvitationResponse, GraphQLCreateLeagueResponse, GraphQLData, GraphQLDeleteGameResultResponse, GraphQLError(..), GraphQLGameInfoResponse, GraphQLGameResultDetailsResponse, GraphQLGameResultMetaResponse, GraphQLGameResultResponse, GraphQLGameSearchResponse, GraphQLGamesResponse, GraphQLInvitationResponse, GraphQLLeagueMemberResponse, GraphQLLeagueResponse, GraphQLMyLeaguesResponse, GraphQLRecentLeaguesResponse, GraphQLResult(..), GraphQLTokenResponse, GraphQLUserIdResponse, GraphQLValue, GraphQlId, LeagueSummary2, ResultsFilter, UserId)

import GraphQL.Client.Http as GraphQLClient
import Model.Bgg exposing (GameInfo, GameSearchResult)
import Model.GameResult exposing (GameResult, GameResultMeta)
import Model.Invitation exposing (Invitation)
import Model.League exposing (Game, IdOnly, League, LeagueMember, LeagueSummary, Player)
import RemoteData exposing (RemoteData)


type alias ResultsFilter =
    { gameFilter : Maybe String
    , playerFilter : { playerIds : List String, together : Bool }
    , numPlayerFilter : Maybe Int
    , idFilter : Maybe (List String)
    }


type alias GraphQLData a =
    RemoteData GraphQLClient.Error a


type alias GraphQlId =
    { id : String }


type alias UserId =
    { auth0id : String
    }


type alias LeagueSummary2 =
    { id : String
    , name : String
    , slug : String
    , numGames : Int
    , numPlayers : Int
    }


type alias GraphQLValue =
    LeagueSummary2


type GraphQLError
    = UniqueConstraintViolation
    | NodeDoesNotExist
    | UnknownError


type GraphQLResult
    = CreateLeagueResult (Result GraphQLError GraphQLValue)


type alias GraphQLGameResultResponse =
    Result GraphQLClient.Error (List GameResult)


type alias GraphQLUserIdResponse =
    Result GraphQLClient.Error (Maybe UserId)


type alias GraphQLTokenResponse =
    Result GraphQLClient.Error (Maybe UserId)


type alias GraphQLDeleteGameResultResponse =
    Result GraphQLClient.Error IdOnly


type alias GraphQLGameResultDetailsResponse =
    Result GraphQLClient.Error GameResult


type alias GraphQLCreateLeagueResponse =
    Result GraphQLClient.Error League


type alias GraphQLCreateInvitationResponse =
    Result GraphQLClient.Error Invitation


type alias GraphQLLeagueResponse =
    Result GraphQLClient.Error League


type alias GraphQLRecentLeaguesResponse =
    Result GraphQLClient.Error (List LeagueSummary)


type alias GraphQLMyLeaguesResponse =
    Result GraphQLClient.Error (List LeagueSummary)


type alias GraphQLGameResultMetaResponse =
    Result GraphQLClient.Error (List GameResultMeta)


type alias GraphQLGamesResponse =
    Result GraphQLClient.Error (List Game)


type alias GraphQLInvitationResponse =
    Result GraphQLClient.Error (Maybe Invitation)


type alias GraphQLGameSearchResponse =
    Result GraphQLClient.Error (List GameSearchResult)


type alias GraphQLGameInfoResponse =
    Result GraphQLClient.Error GameInfo


type alias GraphQLLeagueMemberResponse =
    Result GraphQLClient.Error LeagueMember


type alias GraphQLCreateGameResultResponse =
    Result GraphQLClient.Error GameResult
