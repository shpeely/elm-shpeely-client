module Model.NewGameResult exposing (GameResultValidationError(..), NewGameResult, PlayerEntry(..), Score, collectErrors, initialScore, initialState, mapError, toGameResult, validateGameResult, validators)

import Dict exposing (Dict)
import Model.Bgg exposing (GameInfo)
import Model.GameResult as GameResult exposing (UnsavedGameResult)
import Model.GraphQL exposing (GraphQLData)
import Model.League exposing (Player)
import RemoteData exposing (RemoteData(..))
import Utils


type alias NewGameResult =
    { gameInfo : GraphQLData GameInfo
    , scores : List ( Int, Score )
    , playerSearchResults : Dict Int (List PlayerEntry)
    }


type alias Score =
    { player : Maybe PlayerEntry
    , score : Maybe Float
    }


initialScore : Score
initialScore =
    { player = Nothing
    , score = Nothing
    }


type PlayerEntry
    = NewPlayer String
    | ExistingPlayer Player


initialState : NewGameResult
initialState =
    { gameInfo = NotAsked
    , scores = []
    , playerSearchResults = Dict.empty
    }


type GameResultValidationError
    = NoScoresError
    | SingleScore
    | MissingScoreError (List Int)
    | MissingPlayerError (List Int)
    | DuplicatePlayerNameError (List Int)
    | MissingGame


{-|

    Converts a NewGameResult (basically the model of the game result form) to an actual GameResult

-}
toGameResult : NewGameResult -> UnsavedGameResult
toGameResult newGameResult =
    let
        mapPlayerEntry : PlayerEntry -> String
        mapPlayerEntry entry =
            case entry of
                NewPlayer name ->
                    name

                ExistingPlayer p ->
                    p.name

        mapScore : ( Int, Score ) -> Maybe GameResult.ScoreInput
        mapScore indexedScore =
            let
                ( _, { player, score } ) =
                    indexedScore
            in
            case ( player, score ) of
                ( Just player_, Just score_ ) ->
                    Just { score = score_, playerName = mapPlayerEntry player_ }

                _ ->
                    Nothing

        scores =
            List.filterMap mapScore newGameResult.scores

        bggid =
            case newGameResult.gameInfo of
                Success info ->
                    info.bggid

                _ ->
                    -- TODO
                    -2
    in
    UnsavedGameResult bggid scores


validators : List (NewGameResult -> Maybe GameResultValidationError)
validators =
    [ \r ->
        case r.gameInfo of
            Success _ ->
                Nothing

            _ ->
                Just MissingGame
    , \r ->
        case r.scores of
            [] ->
                Just NoScoresError

            _ ->
                Nothing
    , \r ->
        case r.scores of
            x :: [] ->
                Just SingleScore

            _ ->
                Nothing
    , \r ->
        r.scores
            |> List.filter (\( i, score ) -> score.player /= Nothing && score.score == Nothing)
            |> List.map Tuple.first
            |> mapError MissingScoreError
    , \r ->
        r.scores
            |> List.filter (\( i, score ) -> score.player == Nothing && score.score /= Nothing)
            |> List.map Tuple.first
            |> mapError MissingPlayerError
    , \r ->
        r.scores
            |> List.filterMap
                -- map to (Maybe (index, playerName))
                (\( i, score ) ->
                    case score.player of
                        Just p ->
                            case p of
                                NewPlayer name ->
                                    Just ( i, name )

                                ExistingPlayer player ->
                                    Just ( i, player.name )

                        Nothing ->
                            Nothing
                )
            |> List.foldl
                (\( i, name ) acc ->
                    case Dict.get name acc of
                        Just ( count, indices ) ->
                            Dict.insert name ( count + 1, i :: indices ) acc

                        Nothing ->
                            Dict.insert name ( 1, [ i ] ) acc
                )
                Dict.empty
            |> Dict.values
            |> List.filterMap
                (\( count, indices ) ->
                    if count > 1 then
                        Just indices

                    else
                        Nothing
                )
            |> (Maybe.withDefault [] << List.head)
            |> List.sort
            |> mapError DuplicatePlayerNameError
    ]


collectErrors : NewGameResult -> List GameResultValidationError
collectErrors gameResult =
    List.filterMap (\f -> f gameResult) validators


validateGameResult : NewGameResult -> Result (List GameResultValidationError) NewGameResult
validateGameResult gameResult =
    case collectErrors gameResult of
        [] ->
            Result.Ok gameResult

        errors ->
            Result.Err errors


mapError : (List a -> GameResultValidationError) -> List a -> Maybe GameResultValidationError
mapError err xe =
    if List.isEmpty xe then
        Nothing

    else
        (Just << err) xe
