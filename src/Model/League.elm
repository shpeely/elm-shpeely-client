module Model.League exposing (Game, GameInfo, IdOnly, League, LeagueMember, LeagueSummary, MemberRole(..), Player, UnsavedLeague)

import Model.User exposing (User)
import Slug exposing (Slug)
import Time exposing (Posix)


type alias IdOnly =
    { id : String }


type alias Player =
    { name : String
    , id : String
    }


type alias Game =
    { id : String
    , name : String
    , bggid : Int
    , weight : Float
    , thumbnail : String
    , playTime : Int
    , maxPlayers : Int
    }


type alias GameInfo =
    { id : String
    , bggid : Int
    , weight : Float
    , name : String
    , thumbnail : String
    , playTime : Int
    , maxPlayers : Int
    , count : Int
    , highscores : List String
    , lowscores : List String
    }


type alias League =
    { name : String
    , slug : String
    , players : List Player
    , members : List LeagueMember
    }


type alias LeagueMember =
    { id : String
    , user : User
    , role : MemberRole
    }


type MemberRole
    = Admin
    | Member


type alias UnsavedLeague =
    { name : String
    , slug : Maybe Slug
    }


type alias LeagueSummary =
    { name : String
    , slug : String
    , numResults : Int
    , numPlayers : Int
    , numGames : Int
    , lastGameresultAt : Maybe Posix
    , createdAt : Posix
    }
