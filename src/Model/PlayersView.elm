module Model.PlayersView exposing (PlayersViewModel, initialState)

import Table


type alias PlayersViewModel =
    { tableState : Table.State
    }


initialState : PlayersViewModel
initialState =
    { tableState = Table.initialSort "Player"
    }
