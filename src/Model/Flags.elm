module Model.Flags exposing (Flags)


type alias Flags =
    { host : String
    , auth0Domain : String
    , auth0ClientId : String
    , shpeelyApiUrl : String
    , gameStatsApiUrl : String
    , graphQlUrl : String
    , version : String
    }
