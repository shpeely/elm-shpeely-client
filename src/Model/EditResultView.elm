module Model.EditResultView exposing (EditResultViewModel, initialState)

import Components.ResultForm as ResultForm exposing (ResultFormState)
import Debounce exposing (Debounce)
import Dict exposing (Dict)
import Model.Bgg exposing (GameInfo, GameSearchResult)
import Model.GameResult exposing (GameResult)
import Model.GraphQL exposing (GraphQLData)
import Model.League exposing (Player)
import Msgs.EditResultViewMsgs exposing (EditResultViewMsg)
import RemoteData exposing (RemoteData(..))


initialState : EditResultViewModel
initialState =
    { gameResultDetails = NotAsked
    , gameInfo = NotAsked
    , searchResults = []
    , playerSearchResults = Dict.empty
    , playerInputs = Dict.empty
    , scores = Dict.empty
    , formSubmitted = False
    , debounceGameSearch = Debounce.init
    , resultFormState = ResultForm.initialState
    }


type alias EditResultViewModel =
    { gameResultDetails : GraphQLData GameResult
    , gameInfo : GraphQLData GameInfo
    , searchResults : List GameSearchResult
    , playerSearchResults : Dict Int (List Player)
    , playerInputs : Dict Int String
    , scores : Dict Int { playerName : Maybe String, score : Maybe Float }
    , formSubmitted : Bool
    , debounceGameSearch : Debounce String
    , resultFormState : ResultFormState EditResultViewMsg
    }
