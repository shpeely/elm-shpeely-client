module Model.LoginData exposing (JwtToken, LoginProvider(..), LoginStatus(..), isLoggedIn, loginProviderToString, toMaybe)


type LoginProvider
    = Facebook
    | GooglePlus
    | Twitter
    | Auth0


loginProviderToString : LoginProvider -> String
loginProviderToString provider =
    case provider of
        Facebook ->
            "facebook"

        GooglePlus ->
            "google-oauth2"

        Auth0 ->
            "Username-Password-Authentication"

        Twitter ->
            "twitter"


type alias JwtToken =
    { iat : Int
    , exp : Int
    , name : String
    }


type LoginStatus
    = LoggedIn String
    | Checking
    | NotLoggedIn



-- Helpers


isLoggedIn : LoginStatus -> Bool
isLoggedIn status =
    case status of
        LoggedIn _ ->
            True

        _ ->
            False


toMaybe : LoginStatus -> Maybe String
toMaybe loginStatus =
    case loginStatus of
        LoggedIn token ->
            Just token

        _ ->
            Nothing
