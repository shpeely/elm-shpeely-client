module Model.NewResultView exposing (NewResultViewModel, initialState)

import Components.ResultForm as ResultForm exposing (ResultFormState)
import Debounce exposing (Debounce)
import Dict exposing (Dict)
import Model.Bgg exposing (GameInfo, GameSearchResult)
import Model.GraphQL exposing (GraphQLData)
import Model.League exposing (Player)
import Msgs.NewGameResultMsgs exposing (NewGameResultMsg)
import RemoteData exposing (RemoteData(..))


initialState : NewResultViewModel
initialState =
    { gameInfo = NotAsked
    , searchResults = []
    , playerSearchResults = Dict.empty
    , playerInputs = Dict.empty
    , scores = Dict.empty
    , formSubmitted = False
    , debounceGameSearch = Debounce.init
    , resultFormState = ResultForm.initialState
    , newResultIsSaving = False
    }


type alias NewResultViewModel =
    { gameInfo : GraphQLData GameInfo
    , searchResults : List GameSearchResult
    , playerSearchResults : Dict Int (List Player)
    , playerInputs : Dict Int String
    , scores : Dict Int { playerName : Maybe String, score : Maybe Float }
    , formSubmitted : Bool
    , debounceGameSearch : Debounce String
    , resultFormState : ResultFormState NewGameResultMsg
    , newResultIsSaving : Bool
    }
