module Model.ChartsView exposing (ChartsViewModel, initialState)

import Model.GameResult exposing (GameResult)
import Model.GraphQL exposing (GraphQLData)
import RemoteData exposing (RemoteData(..))


type alias ChartsViewModel =
    { recentResults : GraphQLData (List GameResult)
    }


initialState : ChartsViewModel
initialState =
    { recentResults = NotAsked
    }
