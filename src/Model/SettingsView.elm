module Model.SettingsView exposing (SettingsViewModel, initialState)

import Bootstrap.Dropdown as Dropdown
import Dict exposing (Dict)
import Model.Invitation exposing (Invitation)


type alias SettingsViewModel =
    { newMemberEmail : String
    , lastInvitedEmail : Maybe String
    }


initialState : SettingsViewModel
initialState =
    { newMemberEmail = ""
    , lastInvitedEmail = Nothing
    }
