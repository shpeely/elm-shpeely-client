module Model.ResultsView exposing (MultiDropdownState, ResultCardState(..), ResultsViewModel, fieldIndex, initialPlayersDropdownState, initialResultsFilterState, initialState, nextState, removeField)

import Dict exposing (Dict)
import Model.GraphQL exposing (GraphQLData, ResultsFilter)


initialState : ResultsViewModel
initialState =
    { resultsFilter = initialResultsFilterState
    , resultFilterPlayerSearch = Dict.empty
    , resultFilterGameSearch = ""
    , dropdownState = initialPlayersDropdownState
    , resultCardStates = Dict.empty
    }


type alias ResultsViewModel =
    { resultsFilter : ResultsFilter
    , resultFilterPlayerSearch : Dict Int String
    , resultFilterGameSearch : String
    , dropdownState : MultiDropdownState
    , resultCardStates : Dict String ResultCardState
    }


type ResultCardState
    = ShowChart
    | ShowDeleteConfirmation
    | ShowDeleted


initialResultsFilterState : ResultsFilter
initialResultsFilterState =
    { playerFilter = { playerIds = [], together = False }
    , gameFilter = Nothing
    , numPlayerFilter = Nothing
    , idFilter = Nothing
    }


type alias MultiDropdownState =
    { counter : Int
    , fieldIds : List String
    }


nextState : MultiDropdownState -> MultiDropdownState
nextState state =
    { state
        | counter = state.counter + 1
        , fieldIds = state.fieldIds ++ [ "player-filter-input-" ++ String.fromInt state.counter ]
    }


initialPlayersDropdownState : MultiDropdownState
initialPlayersDropdownState =
    nextState { counter = 0, fieldIds = [] }


fieldIndex : MultiDropdownState -> String -> Int
fieldIndex state fieldId =
    state.fieldIds
        |> List.indexedMap (\a b -> ( a, b ))
        |> List.filter (\( i, id ) -> id == fieldId)
        |> List.map Tuple.first
        |> List.head
        |> Maybe.withDefault -1


removeField : MultiDropdownState -> String -> MultiDropdownState
removeField state fieldId =
    state.fieldIds
        |> List.filter ((/=) fieldId)
        |> (\ids -> { state | fieldIds = ids })
