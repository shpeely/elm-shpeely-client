'use strict';
import './Styles/main.scss';
import { Elm } from './Main.elm';
import Ports from './Ports';
import { loadRedirectUrl } from './Ports/LocalStorage';

if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
        navigator.serviceWorker.register('/sw.js').then(registration => {
            console.log('SW registered: ', registration);
        }).catch(registrationError => {
            console.log('SW registration failed: ', registrationError);
        });
    });
}

const mountNode = document.getElementById('main');

mountNode.innerText = '';

// The third value on embed are the initial values for incomming ports into Elm
console.log('Start Elm application...')

const app = Elm.Main.init({
    flags: {
        auth0ClientId: process.env.AUTH0_CLIENT_ID,
        auth0Domain: `https://${process.env.AUTH0_DOMAIN}`,
        shpeelyApiUrl: process.env.SHPEELY_API_URL,
        gameStatsApiUrl: process.env.GAME_STATS_API_URL,
        graphQlUrl: process.env.GRAPHQL_URL,
        host: `${document.location.protocol}//${document.location.host}`,
        version: process.env.VERSION || 'snapshot',
    }
});

// initialize ports
Ports.initialize(app.ports);

