module View.Header exposing (view)

import Bootstrap.Button as Button
import Html exposing (..)
import Html.Attributes exposing (alt, class, height, href, src, style, width)
import Html.Events as Events
import Model.LoginData exposing (LoginStatus(..))
import Model.Main exposing (..)
import Model.Route exposing (..)
import Msgs.LoginModalMsgs exposing (LoginModalMsg(..))
import Msgs.Main exposing (..)
import RemoteData exposing (..)
import Routing exposing (viewHref, viewHrefWithOptions, viewLinkWithOptions)


view : Model -> Html Msg
view model =
    nav [ class "navbar navbar-light bg-light navbar-first-level" ]
        [ a ([ class "navbar-brand mr-0" ] ++ viewHref HomeRoute)
            [ img
                [ src "/static/images/shpeely.svg"
                , width 30
                , height 30
                , alt "Shpeely Logo"
                , style "margin-right" ".5em"
                ]
                []
            , span [ class "d-none d-sm-none d-md-inline" ] [ text "Shpeely" ]
            ]
        , leagueName model
        , userControls model
        ]


navNoLink : List (Html Msg) -> Html Msg
navNoLink html =
    span [ class "nav-item" ]
        [ span [ class "nav-no-link" ] html ]


navUl : List (Html Msg) -> Html Msg
navUl =
    navUlWithAttr []


navUlWithAttr : List (Attribute Msg) -> List (Html Msg) -> Html Msg
navUlWithAttr attrs =
    ul ([ class "navbar-nav" ] ++ attrs)


navLiWithAttr : List (Attribute Msg) -> List (Html Msg) -> Html Msg
navLiWithAttr attrs =
    li ([ class "nav-item" ] ++ attrs)


userControls : Model -> Html Msg
userControls model =
    let
        notLoggedIn =
            Button.button
                [ Button.onClick (LoginModalMsgs ShowModal)
                , Button.attrs [ class "btn-link" ]
                ]
                [ text "Login" ]

        loginLoading =
            ul [ class "navbar-nav" ] []
    in
    case model.auth0Profile of
        NotAsked ->
            case model.loginStatus of
                NotLoggedIn ->
                    notLoggedIn

                _ ->
                    loginLoading

        Failure _ ->
            notLoggedIn

        Loading ->
            loginLoading

        Success profile ->
            navUl
                [ navLiWithAttr [ class "d-none d-sm-block" ]
                    [ viewLinkWithOptions HomeRoute "Logout" [ Events.onClick Logout, class "nav-link" ] ]
                , navLiWithAttr [ class "nav-no-link d-block d-sm-none" ]
                    [ a (viewHrefWithOptions HomeRoute [ Events.onClick Logout ])
                        [ i [ class "fa fa-power-off" ] [] ]
                    ]
                , navLiWithAttr [ class "d-none d-sm-block nav-item--no-padding" ]
                    [ img [ class "avatar", src profile.avatar ] [] ]
                ]


leagueName : Model -> Html Msg
leagueName model =
    case model.league of
        Success t ->
            navNoLink [ h1 [ class "h2 mb-0 px-0" ] [ text t.name ] ]

        _ ->
            span [] []
