module View.EditResult exposing (onEnter, update, view)

import Api.ElmGraphQL as ElmGraphQL
import Api.GameStats as GameStats
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Browser.Navigation as Nav
import Components.GamePreview as GamePreview
import Components.LiveDropdown as LiveDropdown
import Components.ResultForm as ResultForm exposing (ResultFormState)
import Debounce exposing (Debounce)
import Dict exposing (Dict)
import ErrorHandler exposing (handleGraphQLError)
import Helper.League as LeagueHelper
import Html exposing (..)
import Html.Attributes exposing (..)
import Model.Bgg exposing (GameInfo, GameSearchResult)
import Model.EditResultView as EditResultView exposing (EditResultViewModel)
import Model.GameResult exposing (GameResult, ScoreInput, UnsavedGameResult)
import Model.GraphQL exposing (GraphQLData, GraphQLGameSearchResponse)
import Model.League exposing (Player)
import Model.Main exposing (Model)
import Model.Route exposing (LeagueSubRoute(..), ResultSubRoute(..), Route(..))
import Msgs.EditResultViewMsgs exposing (EditResultViewMsg(..))
import Msgs.Main exposing (Msg(..))
import RemoteData exposing (RemoteData(..))
import Routing
import Utils exposing (tuple)


view : Model -> Html Msg
view model =
    Grid.row []
        [ Grid.col [ Col.md5, Col.sm12, Col.attrs [ class "mb-5" ] ]
            [ h2 [] [ text "Edit Game Result" ]
            , ResultForm.config "edit-result-form" EditResultNoop
                |> ResultForm.bggid (model.editResultView.gameInfo |> RemoteData.toMaybe |> Maybe.map .bggid)
                |> ResultForm.scores model.editResultView.scores
                |> ResultForm.gameInfo model.editResultView.gameInfo
                |> ResultForm.onGameSearch OnGameSearch
                |> ResultForm.gameSearchResults model.editResultView.searchResults
                |> ResultForm.maxPlayers (model.editResultView.gameInfo |> RemoteData.map .maxPlayers |> RemoteData.withDefault 0)
                |> ResultForm.onGameSelected OnGameSelected
                |> ResultForm.onPlayerInput OnPlayerInput
                |> ResultForm.onScoreInput OnScoreInput
                |> ResultForm.onPlayerSelected OnPlayerSelected
                |> ResultForm.onSubmit OnSubmit
                |> ResultForm.playerSearchResults model.editResultView.playerSearchResults
                |> ResultForm.playerInputs model.editResultView.playerInputs
                |> ResultForm.submitted model.editResultView.formSubmitted
                |> ResultForm.onLiveDropdownStateUpdate EditResultLiveDropdownMsg
                |> ResultForm.isLoading (model.editResultView.gameResultDetails == Loading)
                |> ResultForm.render model.editResultView.resultFormState
                |> Html.map EditResultViewMsgs
            ]
        , Grid.col
            [ Col.md6, Col.sm12, Col.offsetMd1 ]
            [ GamePreview.render model.editResultView.gameInfo 700 ]
        ]


onEnter : Model -> ( Model, Cmd Msg )
onEnter model =
    case model.route of
        LeagueRoute slug (ResultRoute resultId EditResult) ->
            ( model
                |> getViewModel
                |> setResultDetails Loading
                |> setGameInfo Loading
                |> setViewModel model
            , Cmd.batch
                [ GameStats.getGameResultStats model slug resultId
                , ElmGraphQL.loadGameResultDetails model resultId (ResultDetailsLoaded >> EditResultViewMsgs)
                ]
            )

        _ ->
            ( model, Cmd.none )


debounceConfig : Debounce.Config Msg
debounceConfig =
    { strategy = Debounce.later 300
    , transform = EditResultViewMsgs << DebounceMsg
    }


update : EditResultViewMsg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        EditResultNoop ->
            ( model, Cmd.none )

        EditResultLiveDropdownMsg state ->
            let
                msg_ =
                    Maybe.withDefault EditResultNoop (LiveDropdown.getMsg state)

                newModel =
                    model
                        |> getViewModel
                        |> setResultFormState (ResultForm.update model.editResultView.resultFormState (LiveDropdown.update state))
                        |> setViewModel model
            in
            update msg_ newModel

        ResultDetailsLoaded response ->
            handleGraphQLError response
                ( model
                    |> getViewModel
                    |> setResultDetails (RemoteData.fromResult response)
                    |> setScores (RemoteData.fromResult response)
                    |> setViewModel model
                , RemoteData.fromResult response
                    |> RemoteData.map (.bggInfo >> .bggid >> ElmGraphQL.loadGameInfo model (GameInfoLoaded >> EditResultViewMsgs))
                    |> RemoteData.withDefault Cmd.none
                )

        GameInfoLoaded response ->
            let
                gameInfo =
                    RemoteData.fromResult response

                scores =
                    case gameInfo of
                        Success gameInfo_ ->
                            -- remove scores that have an index higher than max players of the loaded game
                            model
                                |> getViewModel
                                |> .scores
                                |> Dict.toList
                                |> List.filter (\( index, _ ) -> index <= gameInfo_.maxPlayers)
                                |> Dict.fromList

                        _ ->
                            model
                                |> getViewModel
                                |> .scores
            in
            ( model
                |> getViewModel
                |> setGameInfo gameInfo
                |> setScoresDict scores
                |> setViewModel model
            , -- players may have changed -> reload league in the background
              case model.league of
                Success { slug } ->
                    ElmGraphQL.loadLeague model slug

                _ ->
                    Cmd.none
            )

        OnGameSearch gameSearch ->
            let
                ( debounce, cmd ) =
                    Debounce.push debounceConfig gameSearch model.editResultView.debounceGameSearch
            in
            ( model
                |> getViewModel
                |> setGameSearchDebounce debounce
                |> setViewModel model
            , cmd
            )

        DebounceMsg msg_ ->
            let
                ( debounce, cmd ) =
                    Debounce.update
                        debounceConfig
                        (Debounce.takeLast (ElmGraphQL.searchGame model (OnGameSearchResults >> EditResultViewMsgs)))
                        msg_
                        model.editResultView.debounceGameSearch
            in
            ( model
                |> getViewModel
                |> setGameSearchDebounce debounce
                |> setViewModel model
            , cmd
            )

        OnGameSearchResults response ->
            ( model
                |> getViewModel
                |> setSearchResults response
                |> setViewModel model
            , Cmd.none
            )

        OnGameSelected bggid ->
            ( model
                |> getViewModel
                |> setGameInfo Loading
                |> setViewModel model
            , ElmGraphQL.loadGameInfo model (GameInfoLoaded >> EditResultViewMsgs) bggid
            )

        OnPlayerInput index playerSearch ->
            let
                foundPlayers =
                    model.league
                        |> RemoteData.map .players
                        |> RemoteData.map (List.filter (.name >> String.toLower >> String.contains (String.toLower playerSearch)))
                        |> RemoteData.withDefault []

                player =
                    if playerSearch == "" then
                        Nothing

                    else
                        Just playerSearch
            in
            ( model
                |> getViewModel
                |> setPlayerSearchResults index foundPlayers
                |> setPlayerInputs index playerSearch
                |> setPlayer index player
                |> setViewModel model
            , Cmd.none
            )

        OnPlayerSelected index name ->
            ( model
                |> getViewModel
                |> setPlayer index (Just name)
                |> setViewModel model
            , Cmd.none
            )

        OnScoreInput index score ->
            ( model
                |> getViewModel
                |> setScore index (String.toFloat score)
                |> setViewModel model
            , Cmd.none
            )

        OnSubmit ->
            let
                unsavedScores =
                    model.editResultView.scores
                        |> Dict.values
                        |> List.filterMap (\{ playerName, score } -> Maybe.map2 ScoreInput playerName score)
            in
            ( model
                |> getViewModel
                |> setResultDetails Loading
                |> setSubmitted True
                |> setViewModel model
            , case ( model.editResultView.gameInfo, model.editResultView.gameResultDetails ) of
                ( Success { bggid }, Success { id } ) ->
                    Cmd.batch
                        [ ElmGraphQL.updateGameResult
                            model
                            id
                            (UnsavedGameResult bggid unsavedScores)
                            (OnChangesSaved >> EditResultViewMsgs)

                        -- reload league in the background -> players may have changed
                        ]

                _ ->
                    -- should never happen: Saving game before game info or result is loaded
                    Cmd.none
            )

        OnChangesSaved response ->
            let
                -- reset league data
                newModel =
                    LeagueHelper.resetLeague model
            in
            case ( RemoteData.fromResult response, newModel.route ) of
                ( Success resultDetails, LeagueRoute slug _ ) ->
                    ResultRoute resultDetails.id ResultDetails
                        |> LeagueRoute slug
                        |> Routing.reverseRoute
                        |> Nav.pushUrl newModel.key
                        |> tuple (setViewModel newModel EditResultView.initialState)

                _ ->
                    handleGraphQLError response ( newModel, Cmd.none )


getViewModel : Model -> EditResultViewModel
getViewModel model =
    model.editResultView


setViewModel : Model -> EditResultViewModel -> Model
setViewModel model viewModel =
    { model | editResultView = viewModel }


setResultDetails : GraphQLData GameResult -> EditResultViewModel -> EditResultViewModel
setResultDetails details viewModel =
    { viewModel | gameResultDetails = details }


setGameInfo : GraphQLData GameInfo -> EditResultViewModel -> EditResultViewModel
setGameInfo info viewModel =
    { viewModel | gameInfo = info }


setGameSearchDebounce : Debounce String -> EditResultViewModel -> EditResultViewModel
setGameSearchDebounce debounce viewModel =
    { viewModel | debounceGameSearch = debounce }


setResultFormState : ResultFormState EditResultViewMsg -> EditResultViewModel -> EditResultViewModel
setResultFormState state viewModel =
    { viewModel | resultFormState = state }


setScores : GraphQLData GameResult -> EditResultViewModel -> EditResultViewModel
setScores result viewModel =
    { viewModel
        | scores =
            result
                |> RemoteData.toMaybe
                |> Maybe.map .scores
                |> Maybe.withDefault []
                |> List.indexedMap Tuple.pair
                |> List.map (Tuple.mapFirst ((+) 1))
                |> List.map (Tuple.mapSecond (\{ player, score } -> { playerName = Just player.name, score = Just score }))
                |> Dict.fromList
    }


setSearchResults : GraphQLGameSearchResponse -> EditResultViewModel -> EditResultViewModel
setSearchResults results viewModel =
    { viewModel | searchResults = RemoteData.fromResult results |> RemoteData.toMaybe |> Maybe.withDefault [] }


setPlayerSearchResults : Int -> List Player -> EditResultViewModel -> EditResultViewModel
setPlayerSearchResults index searchResults viewModel =
    { viewModel | playerSearchResults = Dict.insert index searchResults viewModel.playerSearchResults }


setPlayerInputs : Int -> String -> EditResultViewModel -> EditResultViewModel
setPlayerInputs index playerInput viewModel =
    { viewModel | playerInputs = Dict.insert index playerInput viewModel.playerInputs }


setSubmitted : Bool -> EditResultViewModel -> EditResultViewModel
setSubmitted submitted viewModel =
    { viewModel | formSubmitted = submitted }


setPlayer : Int -> Maybe String -> EditResultViewModel -> EditResultViewModel
setPlayer index playerName viewModel =
    { viewModel
        | scores =
            Dict.update
                index
                (\score ->
                    case score of
                        Just s ->
                            Just { s | playerName = playerName }

                        Nothing ->
                            Just { playerName = playerName, score = Nothing }
                )
                viewModel.scores
    }


setScore : Int -> Maybe Float -> EditResultViewModel -> EditResultViewModel
setScore index score viewModel =
    { viewModel
        | scores =
            Dict.update
                index
                (\score_ ->
                    case score_ of
                        Just s ->
                            Just { s | score = score }

                        Nothing ->
                            Just { playerName = Nothing, score = score }
                )
                viewModel.scores
    }


setScoresDict : Dict Int { playerName : Maybe String, score : Maybe Float } -> EditResultViewModel -> EditResultViewModel
setScoresDict scoresDict viewModel =
    { viewModel | scores = scoresDict }
