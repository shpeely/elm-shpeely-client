module View.Results exposing (deleteConfirmation, filterChanged, gameFilter, gameResultFilterId, gamesToItems, numPlayerFilterName, numPlayersFilter, onEnter, playerFilter, playerFilterIds, playersToItems, reloadResults, resultCard, sameGameCheckbox, setTogether, singlePlayerFilter, update, updateCardState, updateDropdowns, updateFilter, updateGameFilter, updateNumPlayers, updatePlayerFilter, updateResultFilterGameSearch, updateResultFilterPlayerSearch, view)

import Api.ElmGraphQL as ElmGraphQL
import Api.GameStats as GameStats
import Bootstrap.Alert as Alert
import Bootstrap.Button as Button
import Bootstrap.Dropdown as Dropdown
import Bootstrap.Form as Form
import Bootstrap.Form.Checkbox as Checkbox
import Bootstrap.Form.Input as Input
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Grid.Row as Row
import Components.GameResultCol as GameResultCol
import Components.IconButton exposing (iconButton)
import Components.LiveDropdown as LiveDropdown
import Components.Spinner exposing (spinner)
import Dict exposing (Dict)
import ErrorHandler exposing (handleGraphQLError)
import FeatherIcons
import Helper.Grid as GridHelper
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Model.GameResult exposing (GameResult)
import Model.GraphQL exposing (GraphQLData, ResultsFilter)
import Model.League exposing (Game, GameInfo, League)
import Model.Main exposing (Model)
import Model.ResultsView exposing (MultiDropdownState, ResultCardState(..), ResultsViewModel, fieldIndex, nextState, removeField)
import Model.Route exposing (LeagueSubRoute(..), ResultSubRoute(..), Route(..))
import Msgs.BootstrapMsgs as BootstrapMsgs
import Msgs.Main exposing (..)
import Msgs.ResultsViewMsgs exposing (..)
import RemoteData exposing (RemoteData(..), WebData)
import Utils exposing (maybeLoad)


view : Model -> Html Msg
view model =
    div []
        [ Grid.row []
            [ Grid.col []
                [ h2 [] [ text "Results" ] ]
            ]
        , Grid.row [ Row.attrs [ class "mb-2" ] ]
            [ Grid.col [ Col.lg4, Col.md6, Col.sm12 ]
                [ playerFilter model
                , if List.length (playerFilterIds model) > 1 then
                    sameGameCheckbox model.resultsView.resultsFilter.playerFilter.together

                  else
                    text ""
                ]
            , Grid.col [ Col.lg4, Col.md6, Col.sm12 ]
                [ gameFilter model ]
            , Grid.col [ Col.lg4, Col.md6, Col.sm12 ]
                [ numPlayersFilter model ]
            ]
        , case model.gameResults of
            Success gameResults ->
                if List.isEmpty gameResults then
                    Grid.row [ Row.centerXs ]
                        [ Grid.col [ Col.md6 ]
                            [ Alert.simpleInfo []
                                [ Alert.h5 [] [ text "No Results Found" ]
                                , p [] [ text "There are no game results that fulfill your search criteria." ]
                                ]
                            ]
                        ]

                else
                    Grid.row [ Row.centerXs ]
                        (List.map (resultCard model) gameResults)

            Failure _ ->
                Grid.row []
                    [ Grid.col [ Col.xs ]
                        [ p [] [ text "Oops, something went wrong... Try reloading the page." ] ]
                    ]

            _ ->
                Grid.row []
                    [ Grid.col [ Col.xs, Col.attrs [ class "my-3" ] ]
                        [ spinner ]
                    ]
        ]


onEnter : Model -> ( Model, Cmd Msg )
onEnter model =
    case model.route of
        LeagueRoute slug _ ->
            ( model
            , Cmd.batch
                [ ElmGraphQL.loadGameResults model slug (GameResultsLoaded >> ResultsViewMsgs)
                , maybeLoad model.games <| ElmGraphQL.loadGames model slug
                , maybeLoad model.gameStats <| GameStats.getGameStats model slug
                , maybeLoad model.gameResultsMeta <| ElmGraphQL.loadGameResultMeta model slug
                ]
            )

        _ ->
            ( model, Cmd.none )


resultCard : Model -> GameResult -> Grid.Column Msg
resultCard model gameResult =
    let
        getCardState : ResultsViewModel -> String -> ResultCardState
        getCardState viewModel resultId =
            Dict.get resultId viewModel.resultCardStates
                |> Maybe.withDefault ShowChart
    in
    case getCardState model.resultsView gameResult.id of
        ShowChart ->
            GameResultCol.render
                model
                gameResult
                model.league

        ShowDeleteConfirmation ->
            Grid.col [ Col.lg4, Col.md6, Col.sm12 ]
                [ deleteConfirmation gameResult ]

        ShowDeleted ->
            Grid.col [ Col.lg4, Col.md6, Col.sm12 ]
                [ spinner ]


deleteConfirmation : GameResult -> Html Msg
deleteConfirmation result =
    div []
        [ p []
            [ text <|
                "Are you sure you want to delete this result of "
                    ++ result.bggInfo.name
                    ++ "? This action is not reversible!"
            ]
        , div []
            [ Button.button
                [ Button.outlineDanger
                , Button.attrs [ class "mr-2" ]
                , Button.onClick ((ResultsViewMsgs << DeleteResult) result.id)
                ]
                [ text "Yes, delete" ]
            , Button.button
                [ Button.outlinePrimary
                , Button.onClick ((ResultsViewMsgs << CancelDelete) result.id)
                ]
                [ text "Cancel" ]
            ]
        ]


numPlayerFilterName : Maybe Int -> String
numPlayerFilterName numPlayers =
    case numPlayers of
        Just n ->
            String.fromInt n
                ++ (if n == 1 then
                        " player"

                    else
                        " players"
                   )

        Nothing ->
            "Any number of players"


numPlayersFilter : Model -> Html Msg
numPlayersFilter model =
    Form.form []
        [ Form.row [ Row.attrs [ class "no-gutters" ] ]
            [ Form.col [ Col.xs12 ]
                [ Form.label [] [ text "Number of Players Filter" ] ]
            , Form.col [ Col.xs10 ]
                [ Dropdown.dropdown
                    model.bootstrapState.numPlayersFilterDropdownState
                    { options =
                        [ Dropdown.attrs
                            [ class "w-100" ]
                        , Dropdown.menuAttrs
                            [ class "text-center"

                            -- Not sure how to do it in CSS, the dropdown is kind of weird with transforms and stuff.
                            , style "width"
                                (GridHelper.colWidth { xl = 4 / 12 * 10, lg = 4 / 12 * 10, md = 5, sm = 10, xs = 10 } model.windowSize |> String.fromInt |> (\s -> s ++ "px"))
                            ]
                        ]
                    , toggleMsg = BootstrapMsgs << BootstrapMsgs.NumPlayersDropdownToggle
                    , toggleButton =
                        Dropdown.toggle
                            [ Button.outlinePrimary, Button.block ]
                            [ text <| numPlayerFilterName <| model.resultsView.resultsFilter.numPlayerFilter ]
                    , items =
                        List.range 1 8
                            |> List.map
                                (\i ->
                                    Dropdown.buttonItem
                                        [ onClick (ResultsViewMsgs <| SetNumberOfPlayersFilter <| Just i) ]
                                        [ text <| numPlayerFilterName <| Just i ]
                                )
                            |> (::)
                                (Dropdown.buttonItem
                                    [ onClick (ResultsViewMsgs <| SetNumberOfPlayersFilter <| Nothing) ]
                                    [ text <| numPlayerFilterName <| Nothing ]
                                )
                    }
                ]
            , Form.col [ Col.xs12 ]
                [ Form.help [] [ text "Filter game results for a specific number of players" ] ]
            ]
        ]


gamesToItems :
    Maybe (List GameInfo)
    -> String
    ->
        List
            { attrs : List (Html.Attribute Msg)
            , html : List (Html.Html Msg)
            , value : ( String, Msg )
            }
gamesToItems gameInfos search =
    case gameInfos of
        Just results ->
            results
                |> List.filter (.name >> String.toLower >> String.contains (String.toLower search))
                |> List.sortBy (.count >> (*) -1)
                |> List.take 10
                |> List.map
                    (\g ->
                        { attrs = []
                        , html =
                            [ text g.name
                            , small
                                [ style "margin-left" "1em" ]
                                [ text <| "(" ++ String.fromInt g.count ++ " games)" ]
                            ]
                        , value = ( g.name, (ResultsViewMsgs << GameFilterSelected) g.id )
                        }
                    )

        Nothing ->
            LiveDropdown.noItems


gameResultFilterId : String
gameResultFilterId =
    "game-result-filter"


gameFilter : Model -> Html Msg
gameFilter model =
    Form.form []
        [ Form.row [ Row.attrs [ class "no-gutters" ] ]
            [ Form.col [ Col.xs12 ]
                [ Form.label [] [ text "Game Filter" ] ]
            , Form.col [ Col.xs10 ]
                [ LiveDropdown.config gameResultFilterId LiveDropdownMsg Noop
                    |> LiveDropdown.noMargin
                    |> LiveDropdown.inputOptions
                        [ Input.placeholder "Search game..."
                        , Input.attrs [ onInput (ResultsViewMsgs << GameResultFilterInput) ]
                        ]
                    |> LiveDropdown.items (gamesToItems model.gameInfos model.resultsView.resultFilterGameSearch)
                    |> LiveDropdown.view model.liveDropdownState
                ]

            -- show the remove button if a game is selected
            , Form.col
                [ Col.xs2 ]
                [ if model.resultsView.resultsFilter.gameFilter /= Nothing then
                    iconButton FeatherIcons.x (ResultsViewMsgs ClearGameFilter)

                  else
                    text ""
                ]
            , Form.col [ Col.xs12 ]
                [ Form.help [] [ text "Filter game results for a specific game" ] ]
            ]
        ]


playersToItems :
    RemoteData e League
    -> ResultsFilter
    -> String
    -> String
    ->
        List
            { attrs : List (Html.Attribute Msg)
            , html : List (Html.Html Msg)
            , value : ( String, Msg )
            }
playersToItems league resultsFilter id search =
    case league of
        RemoteData.Success t ->
            t.players
                |> List.filter (.name >> String.toLower >> String.contains (String.toLower search))
                |> List.filter (.id >> (\a -> List.member a resultsFilter.playerFilter.playerIds) >> not)
                |> List.sortBy .name
                |> List.map
                    (\p ->
                        { attrs = []
                        , html =
                            [ text p.name
                            ]
                        , value = ( p.name, (ResultsViewMsgs << GameResultFilterPlayerSelected id) p.id )
                        }
                    )

        _ ->
            LiveDropdown.noItems


sameGameCheckbox : Bool -> Html Msg
sameGameCheckbox checked =
    Form.group []
        [ Checkbox.custom
            [ Checkbox.checked checked
            , Checkbox.onCheck (ResultsViewMsgs << OnSamePlayerCheck)
            ]
            "In the same game"
        , Form.help [] [ text "Check this if you want to search for game results where all of the above players participated." ]
        ]


singlePlayerFilter : Model -> Int -> Bool -> String -> Html Msg
singlePlayerFilter model index isLast id =
    let
        searchValue index_ =
            Dict.get index_ model.resultsView.resultFilterPlayerSearch
                |> Maybe.withDefault ""

        playerFilters : List String
        playerFilters =
            playerFilterIds model

        fieldLabel : Form.Col Msg
        fieldLabel =
            if index == 0 then
                Form.colLabel [ Col.xs12 ] [ text "Player Filter" ]

            else
                Form.colLabel
                    [ Col.attrs [ class "sr-only" ] ]
                    [ text (String.fromInt (index + 1) ++ ". Player") ]

        onlyField : Bool
        onlyField =
            (index == 0) && isLast
    in
    Form.row [ Row.attrs [ class "no-gutters" ] ]
        [ fieldLabel
        , Form.col [ Col.xs10 ]
            [ LiveDropdown.config id LiveDropdownMsg Noop
                |> LiveDropdown.noMargin
                |> LiveDropdown.autoSelectSingle False
                |> LiveDropdown.inputOptions
                    [ Input.placeholder "Search player..."
                    , Input.attrs [ onInput (ResultsViewMsgs << GameResultFilterPlayerInput index) ]
                    ]
                |> LiveDropdown.items
                    (playersToItems model.league model.resultsView.resultsFilter id (searchValue index))
                |> LiveDropdown.view model.liveDropdownState
            ]

        -- show the remove button if it's not the last input field
        , Form.col
            [ Col.xs2 ]
            [ if not isLast then
                iconButton FeatherIcons.x (ResultsViewMsgs <| GameResultFilterRemovePlayer id)

              else
                text ""
            ]

        -- show the helptext below the last input field
        , Form.col [ Col.xs12 ]
            [ if onlyField then
                Form.help [] [ text "Filter game results for specific players" ]

              else
                text ""
            ]
        ]


playerFilter : Model -> Html Msg
playerFilter model =
    let
        playerFilters =
            playerFilterIds model

        --        model index isLast id
        isLast : Int -> Bool
        isLast i =
            i >= List.length playerFilters
    in
    Form.form []
        (model.resultsView.dropdownState.fieldIds
            |> List.indexedMap (\i id -> singlePlayerFilter model i (isLast i) id)
        )


update : ResultsViewMsg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GameResultsLoaded response ->
            let
                -- load the game result stats
                cmd =
                    case model.route of
                        LeagueRoute slug _ ->
                            response
                                |> Result.withDefault []
                                |> List.map .id
                                |> List.reverse
                                |> List.map (GameStats.getGameResultStats model slug)
                                |> Cmd.batch

                        _ ->
                            Cmd.none
            in
            handleGraphQLError response ( { model | gameResults = RemoteData.fromResult response }, cmd )

        GameResultFilterInput search ->
            ( { model
                | resultsView = updateResultFilterGameSearch search model.resultsView
              }
            , Cmd.none
            )

        GameFilterSelected gameId ->
            let
                resultsFilter =
                    model.resultsView.resultsFilter

                newViewModel =
                    updateGameFilter resultsFilter (Just gameId)
                        |> (\a -> updateFilter a model.resultsView)

                newModel =
                    { model
                        | resultsView = newViewModel
                        , gameResults = Loading
                    }

                cmd =
                    if filterChanged model newModel then
                        reloadResults newModel

                    else
                        Cmd.none
            in
            ( newModel, cmd )

        ClearGameFilter ->
            let
                resultsFilter =
                    model.resultsView.resultsFilter

                newViewModel =
                    updateGameFilter resultsFilter Nothing
                        |> (\a -> updateFilter a model.resultsView)

                newModel =
                    { model
                        | resultsView = newViewModel
                        , gameResults = Loading
                        , liveDropdownState = LiveDropdown.resetState model.liveDropdownState gameResultFilterId
                    }

                cmd =
                    if filterChanged model newModel then
                        reloadResults newModel

                    else
                        Cmd.none
            in
            ( newModel, cmd )

        GameResultFilterPlayerInput index playerSearch ->
            let
                newViewModel =
                    updateResultFilterPlayerSearch index playerSearch model.resultsView
            in
            ( { model | resultsView = newViewModel }, Cmd.none )

        GameResultFilterPlayerSelected fieldId playerId ->
            let
                currentPlayerIds =
                    playerFilterIds model

                index =
                    fieldIndex model.resultsView.dropdownState fieldId

                newPlayerIds =
                    if List.length currentPlayerIds > index then
                        -- replace the player
                        playerFilterIds model
                            |> List.indexedMap
                                (\i p ->
                                    if i == index then
                                        playerId

                                    else
                                        p
                                )

                    else
                        -- add player to the list
                        currentPlayerIds ++ [ playerId ]

                playerFilterDropdownState =
                    model.resultsView.dropdownState

                shouldAddField =
                    List.length currentPlayerIds <= fieldIndex playerFilterDropdownState fieldId

                newPlayerFilterDropdownState =
                    if shouldAddField then
                        nextState playerFilterDropdownState

                    else
                        playerFilterDropdownState

                newViewModel =
                    newPlayerIds
                        |> updatePlayerFilter model.resultsView.resultsFilter
                        |> (\a -> updateFilter a model.resultsView)
                        |> updateDropdowns newPlayerFilterDropdownState

                newModel =
                    { model
                        | resultsView = newViewModel
                        , gameResults = Loading
                    }
            in
            -- only reload if filter changed
            if newViewModel.resultsFilter /= model.resultsView.resultsFilter then
                ( newModel, reloadResults newModel )

            else
                ( model, Cmd.none )

        ClearPlayerFilter ->
            ( model, Cmd.none )

        OnSamePlayerCheck checked ->
            let
                newResultsView =
                    checked
                        |> setTogether model.resultsView.resultsFilter
                        |> (\a -> updateFilter a model.resultsView)

                newModel =
                    { model
                        | resultsView = newResultsView
                        , gameResults = Loading
                    }
            in
            ( newModel, reloadResults newModel )

        GameResultFilterRemovePlayer fieldId ->
            let
                filter =
                    model.resultsView.resultsFilter

                index =
                    fieldIndex model.resultsView.dropdownState fieldId

                newDropdownState =
                    removeField model.resultsView.dropdownState fieldId

                newViewModel =
                    playerFilterIds model
                        |> List.indexedMap (\a b -> ( a, b ))
                        |> List.filter (\( i, p ) -> i /= index)
                        |> List.map Tuple.second
                        |> updatePlayerFilter model.resultsView.resultsFilter
                        |> (\a -> updateFilter a model.resultsView)
                        |> updateDropdowns newDropdownState

                newModel =
                    { model
                        | resultsView = newViewModel
                        , gameResults = Loading
                    }
            in
            ( newModel, reloadResults newModel )

        SetNumberOfPlayersFilter numPlayers ->
            let
                newViewModel =
                    updateNumPlayers model.resultsView.resultsFilter numPlayers
                        |> (\a -> updateFilter a model.resultsView)

                newModel =
                    { model
                        | resultsView = newViewModel
                        , gameResults = Loading
                    }
            in
            ( newModel, reloadResults newModel )

        DeleteResultConfirmation id ->
            ( { model
                | resultsView = model.resultsView |> updateCardState id ShowDeleteConfirmation
              }
            , Cmd.none
            )

        DeleteResult id ->
            ( { model
                | resultsView = model.resultsView |> updateCardState id ShowDeleted
              }
            , ElmGraphQL.deleteGameResult model id
            )

        CancelDelete id ->
            ( { model
                | resultsView = model.resultsView |> updateCardState id ShowChart
              }
            , Cmd.none
            )


getViewModel : Model -> ResultsViewModel
getViewModel model =
    model.resultsView


setViewModel : Model -> ResultsViewModel -> Model
setViewModel model viewModel =
    { model | resultsView = viewModel }


filterChanged : Model -> Model -> Bool
filterChanged oldModel newModel =
    oldModel.resultsView.resultsFilter /= newModel.resultsView.resultsFilter


reloadResults : Model -> Cmd Msg
reloadResults model =
    case model.route of
        LeagueRoute slug _ ->
            ElmGraphQL.loadGameResults model slug (GameResultsLoaded >> ResultsViewMsgs)

        _ ->
            Cmd.none


playerFilterIds : Model -> List String
playerFilterIds model =
    model.resultsView.resultsFilter.playerFilter.playerIds


updatePlayerFilter : ResultsFilter -> List String -> ResultsFilter
updatePlayerFilter filter playerIds =
    let
        playerFilter_ =
            filter.playerFilter

        newPlayerFilter =
            { playerFilter_ | playerIds = playerIds }
    in
    { filter | playerFilter = newPlayerFilter }


updateGameFilter : ResultsFilter -> Maybe String -> ResultsFilter
updateGameFilter filter gameId =
    { filter | gameFilter = gameId }


updateNumPlayers : ResultsFilter -> Maybe Int -> ResultsFilter
updateNumPlayers filter numPlayers =
    { filter | numPlayerFilter = numPlayers }


setTogether : ResultsFilter -> Bool -> ResultsFilter
setTogether filter together =
    let
        playerFilter_ =
            filter.playerFilter

        newPlayerFilter =
            { playerFilter_ | together = together }
    in
    { filter | playerFilter = newPlayerFilter }


updateFilter : ResultsFilter -> ResultsViewModel -> ResultsViewModel
updateFilter filter resultsView =
    { resultsView | resultsFilter = filter }


updateResultFilterGameSearch : String -> ResultsViewModel -> ResultsViewModel
updateResultFilterGameSearch search resultsView =
    { resultsView | resultFilterGameSearch = search }


updateResultFilterPlayerSearch : Int -> String -> ResultsViewModel -> ResultsViewModel
updateResultFilterPlayerSearch index search resultsView =
    let
        newPlayerSearch =
            Dict.insert index search resultsView.resultFilterPlayerSearch
    in
    { resultsView | resultFilterPlayerSearch = newPlayerSearch }


updateDropdowns : MultiDropdownState -> ResultsViewModel -> ResultsViewModel
updateDropdowns dropdownState resultsView =
    { resultsView | dropdownState = dropdownState }


updateCardState : String -> ResultCardState -> ResultsViewModel -> ResultsViewModel
updateCardState id state viewModel =
    { viewModel | resultCardStates = Dict.insert id state viewModel.resultCardStates }
