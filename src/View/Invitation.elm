module View.Invitation exposing (onEnter, view)

import Api.ElmGraphQL as ElmGraphQL
import Bootstrap.Button as Button exposing (Option)
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Components.Spinner exposing (spinner)
import Html exposing (..)
import Html.Attributes exposing (class, href)
import Model.Invitation exposing (Invitation, InvitationResponse(..))
import Model.LoginData exposing (LoginStatus(..))
import Model.Main exposing (..)
import Model.Route exposing (..)
import Msgs.InvitationViewMsgs exposing (..)
import Msgs.LoginModalMsgs exposing (LoginModalMsg(..))
import Msgs.Main exposing (..)
import RemoteData exposing (..)
import Routing
import Update.LoginModalUpdate as LoginModalUpdate


view : Model -> Html Msg
view model =
    Grid.row []
        [ Grid.col [ Col.xs12 ]
            [ pageTitle model
            , pageContent model
            ]
        ]


onEnter : Model -> ( Model, Cmd Msg )
onEnter model =
    case model.route of
        InvitationRoute token ->
            ( model, ElmGraphQL.loadInvitation model token )

        _ ->
            ( model, Cmd.none )


showLoginModalUpdate : Model -> Maybe ( Model, Cmd Msg )
showLoginModalUpdate model =
    case model.loginStatus of
        LoggedIn _ ->
            Nothing

        _ ->
            Just <| LoginModalUpdate.update ShowModal model


showLoginModalCmd : Model -> Cmd Msg
showLoginModalCmd model =
    showLoginModalUpdate model
        |> Maybe.map Tuple.second
        |> Maybe.withDefault Cmd.none


showLoginModalModel : Model -> Model
showLoginModalModel model =
    showLoginModalUpdate model
        |> Maybe.map Tuple.first
        |> Maybe.withDefault model


pageTitle : Model -> Html Msg
pageTitle model =
    case model.invitation of
        Success maybeInvitation ->
            case maybeInvitation of
                Just _ ->
                    h1 [] [ text "Welcome to the Club!" ]

                Nothing ->
                    h1 [] [ text "Inviation not valid" ]

        Loading ->
            text ""

        NotAsked ->
            text ""

        _ ->
            h1 [] [ text "Welcome to the Club!" ]


pageContent : Model -> Html Msg
pageContent model =
    case model.invitation of
        Success maybeInvitation ->
            case maybeInvitation of
                Just invitation ->
                    renderInvitation model.loginStatus invitation

                Nothing ->
                    p [] [ text "Sorry, this invitation link does not appear to be valid." ]

        Loading ->
            spinner

        Failure _ ->
            text "Error loading invitation :( Please try to reload the page..."

        NotAsked ->
            spinner


explanation : Invitation -> Html msg
explanation invitation =
    text
        ("You have been invited by "
            ++ invitation.inviter.username
            ++ " to join the league "
            ++ invitation.league.name
            ++ ". After accepting this invitation, you will be able to add or delete game results in this league."
        )


maybeLogin : LoginStatus -> Msg -> Msg
maybeLogin loginStatus defaultMsg =
    case loginStatus of
        LoggedIn _ ->
            defaultMsg

        _ ->
            LoginModalMsgs ShowModal


renderInvitation : LoginStatus -> Invitation -> Html Msg
renderInvitation loginStatus invitation =
    Card.config [ Card.attrs [ class "mb-2" ] ]
        |> Card.block []
            ([ Block.titleH4 []
                [ text ("Invitation by " ++ invitation.inviter.username ++ " to join ")
                , i [] [ text invitation.league.name ]
                ]
             ]
                ++ (case invitation.state of
                        Accepted ->
                            [ Block.text []
                                [ text "The invitation has been accepted."
                                ]
                            , Block.text []
                                [ Button.linkButton
                                    [ Button.attrs
                                        [ href <| Routing.reverseRoute <| LeagueRoute invitation.league.slug Charts ]
                                    ]
                                    [ text <| "Go to " ++ invitation.league.name ]
                                ]
                            ]

                        Rejected ->
                            [ Block.text []
                                [ text "The invitation has been rejected."
                                ]
                            ]

                        Pending ->
                            [ Block.text []
                                [ explanation invitation
                                ]
                            , Block.text []
                                [ Button.button
                                    [ Button.roleLink
                                    , Button.onClick <| maybeLogin loginStatus <| InvitationViewMsgs <| AcceptInvitation invitation
                                    ]
                                    [ text "Accept" ]
                                , Button.button
                                    [ Button.roleLink
                                    , Button.onClick <| InvitationViewMsgs <| RejectInvitation invitation
                                    ]
                                    [ text "Decline" ]
                                ]
                            ]
                   )
            )
        |> Card.view
