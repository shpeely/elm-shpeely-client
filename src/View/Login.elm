module View.Login exposing (view)

import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Components.LoginButtons exposing (loginButtons)
import Html exposing (Html, h1, text)
import Model.Main exposing (..)
import Msgs.Main exposing (..)


view : Model -> Html Msg
view model =
    Grid.container []
        [ Grid.row []
            [ Grid.col
                [ Col.offsetMd3
                , Col.md6
                ]
                [ h1 [] [ text "Login" ]
                , loginButtons model.flags
                ]
            ]
        ]
