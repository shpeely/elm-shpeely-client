module View.SubHeader exposing (resolve, view)

import Helper.League as LeagueHelper
import Html exposing (..)
import Html.Attributes exposing (..)
import Model.League exposing (League)
import Model.Main exposing (Model)
import Model.Route exposing (LeagueSubRoute(..), Route(..))
import Msgs.Main exposing (Msg)
import RemoteData exposing (RemoteData(..))
import Routing exposing (subRouteActive, viewHref)


subRouteHref : String -> LeagueSubRoute -> List (Html.Attribute Msg)
subRouteHref slug subRoute =
    viewHref <| LeagueRoute slug subRoute


subNavLink : String -> LeagueSubRoute -> String -> Html Msg
subNavLink slug subRoute name =
    a ([ class "nav-link" ] ++ subRouteHref slug subRoute)
        [ text name ]


view : Model -> Html Msg
view model =
    case model.league of
        Success league ->
            let
                link : LeagueSubRoute -> String -> Html Msg
                link =
                    subNavLink league.slug

                leagueMemberLink : League -> LeagueSubRoute -> String -> Html Msg
                leagueMemberLink league_ subRoute name =
                    if LeagueHelper.isMember league_ model.userId then
                        link subRoute name

                    else
                        text ""

                navItem : Model -> LeagueSubRoute -> String -> Bool -> Html Msg
                navItem model_ subRoute name onlyLeagueMembers =
                    let
                        link_ =
                            if onlyLeagueMembers then
                                leagueMemberLink league

                            else
                                link

                        classes =
                            if subRouteActive model_.route subRoute then
                                "active nav-item"

                            else
                                "nav-item"
                    in
                    li [ class classes ]
                        [ link_ subRoute name
                        ]
            in
            nav [ class "navbar navbar-light bg-light navbar-second-level" ]
                [ ul [ class "navbar-nav" ]
                    [ navItem model AddNewResult "Add New Result" True
                    , navItem model Charts "Charts" False
                    , navItem model Games "Games" False
                    , navItem model Players "Players" False
                    , navItem model Results "Results" False
                    , navItem model Settings "Settings" True
                    ]
                ]

        _ ->
            text ""


resolve : Model -> Cmd Msg
resolve model =
    Cmd.none
