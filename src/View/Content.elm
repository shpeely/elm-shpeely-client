module View.Content exposing (onEnter, view)

import Api.ElmGraphQL as ElmGraphQL
import Bootstrap.Grid as Grid
import Components.Title exposing (pageTitle)
import Html exposing (..)
import Html.Attributes exposing (style)
import Model.Main exposing (Model)
import Model.Route exposing (..)
import Msgs.Main exposing (Msg)
import RemoteData exposing (RemoteData(..))
import Utils
import View.About as About
import View.Home as Home
import View.Invitation as Invitation
import View.League as League
import View.Login as Login
import View.Offline as Offline
import View.Privacy as Privacy


view : Model -> Html Msg
view model =
    case model.route of
        HomeRoute ->
            pageWithTitle "Shpeely" Home.view model

        LoginRoute ->
            page Login.view model

        OfflineRoute ->
            Offline.view model

        AboutRoute ->
            page About.view model

        PrivacyRoute ->
            page Privacy.view model

        InvitationRoute token ->
            page Invitation.view model

        LeagueRoute slug subRoute ->
            page League.view model

        NotFoundRoute ->
            div [] [ text "not found" ]


onEnter : Model -> ( Model, Cmd Msg )
onEnter model =
    case model.route of
        HomeRoute ->
            ( { model
                | gameResults = NotAsked
              }
            , ElmGraphQL.loadRecentResults model
            )

        InvitationRoute token ->
            Invitation.onEnter model

        LeagueRoute _ _ ->
            League.onEnter model

        _ ->
            ( model, Cmd.none )


page : (Model -> Html Msg) -> Model -> Html Msg
page content model =
    Grid.container
        [ style "padding-top" "2em"
        , style "padding-bottom" "4em"
        ]
        [ content model
        ]


pageWithTitle : String -> (Model -> Html Msg) -> Model -> Html Msg
pageWithTitle title content model =
    page (withTitle title content) model


withTitle : String -> (Model -> Html Msg) -> Model -> Html Msg
withTitle title content model =
    Grid.container []
        [ pageTitle title
        , content model
        ]
