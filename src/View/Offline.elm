module View.Offline exposing (onEnter, view)

import Bootstrap.Grid as Grid
import Bootstrap.Grid.Row as Row
import Html exposing (..)
import Html.Attributes exposing (..)
import Model.Main exposing (Model)
import Msgs.Main exposing (Msg)
import Ports.Network.Network as Network


view : Model -> Html Msg
view model =
    Grid.container []
        [ Grid.row
            [ Row.middleXs
            , Row.attrs [ style "min-height" "80vh" ]
            ]
            [ Grid.col []
                [ h1 [] [ text "Shpeely Needs Internet" ]
                , p []
                    [ text "You appear to be offline. Please connect to the internet to use Shpeely." ]
                ]
            ]
        ]


onEnter : Model -> ( Model, Cmd Msg )
onEnter model =
    ( model, Network.isConnected () )
