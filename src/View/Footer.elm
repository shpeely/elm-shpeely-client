module View.Footer exposing (view)

import Html exposing (..)
import Html.Attributes exposing (alt, class, href, src)
import Model.Main exposing (Model)
import Model.Route exposing (Route(..))
import Msgs.Main exposing (Msg)
import Routing exposing (reverseRoute)


showFooter : Route -> Bool
showFooter route =
    List.member route
        [ HomeRoute
        , AboutRoute
        , PrivacyRoute
        , LoginRoute
        ]


view : Model -> Html Msg
view model =
    case showFooter model.route of
        True ->
            footer [ class "bg-light footer" ]
                [ nav [ class "nav-links" ]
                    [ ul
                        []
                        [ li []
                            [ a [ href (reverseRoute AboutRoute) ] [ text "About" ]
                            ]
                        , li []
                            [ a [ href (reverseRoute PrivacyRoute) ] [ text "Privacy" ]
                            ]
                        ]
                    , ul
                        [ class "social-links" ]
                        [ li []
                            [ a [ href "https://facebook.com/shpeely" ]
                                [ img
                                    [ src "/static/images/facebook.svg"
                                    , alt "Facebook Logo"
                                    , Html.Attributes.attribute "aria-hidden" "true"
                                    ]
                                    []
                                , span [ class "sr-only" ] [ text "Shpeely on Facebook" ]
                                ]
                            ]
                        , li []
                            [ a [ href "https://twitter.com/_shpeely_" ]
                                [ img
                                    [ src "/static/images/twitter.svg"
                                    , alt "Twitter Logo"
                                    , Html.Attributes.attribute "aria-hidden" "true"
                                    ]
                                    []
                                , span [ class "sr-only" ] [ text "Shpeely on Twitter" ]
                                ]
                            ]
                        , li []
                            [ a [ href "https://reddit.com/r/shpeely" ]
                                [ img
                                    [ src "/static/images/reddit.svg"
                                    , alt "Reddit Logo"
                                    , Html.Attributes.attribute "aria-hidden" "true"
                                    ]
                                    []
                                , span [ class "sr-only" ] [ text "Shpeely on Reddit" ]
                                ]
                            ]
                        ]
                    ]
                ]

        False ->
            text ""
