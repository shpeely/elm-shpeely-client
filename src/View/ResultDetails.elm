module View.ResultDetails exposing (onEnter, update, view)

import Api.ElmGraphQL as ElmGraphQL
import Api.GameStats as GameStats
import Bootstrap.Alert as Alert
import Components.GamePreview as GamePreview
import Components.GameResultChart as GameResultChart
import Dict
import ErrorHandler exposing (handleGraphQLError)
import Helper.Grid as GridHelper exposing (defaultCol)
import Helper.Time exposing (formatDate)
import Html exposing (..)
import Model.BasicTypes exposing (Size)
import Model.Bgg exposing (GameInfo)
import Model.GameResult exposing (GameResult, GameResultMeta)
import Model.GraphQL exposing (GraphQLData)
import Model.League exposing (League)
import Model.Main exposing (Model)
import Model.ResultDetailsView exposing (ResultDetailsViewModel)
import Model.Route exposing (LeagueSubRoute(..), ResultSubRoute(..), Route(..))
import Model.Stats exposing (GameResultStats)
import Msgs.Main exposing (Msg(..))
import Msgs.ResultDetailsViewMsgs exposing (ResultDetailsViewMsg(..))
import RemoteData exposing (RemoteData(..))


chartInput :
    Model
    -> String
    ->
        Maybe
            { league : League
            , gameResultStats : GameResultStats
            , windowSize : Size
            , colWidth : GridHelper.Col
            }
chartInput model resultId =
    Maybe.map2
        (\t s ->
            { league = t
            , gameResultStats = s
            , windowSize = model.windowSize
            , colWidth = defaultCol
            }
        )
        (RemoteData.toMaybe model.league)
        (model.gameResultStats |> Dict.get resultId |> Maybe.withDefault Loading |> RemoteData.toMaybe)


view : Model -> Html Msg
view model =
    case model.route of
        LeagueRoute slug (ResultRoute resultId ResultDetails) ->
            div []
                [ GamePreview.render model.resultDetailsView.gameInfo 0
                , div [] [ text <| gameTime model ]
                , GameResultChart.render <| chartInput model resultId
                ]

        _ ->
            Alert.simpleDanger [] [ text "Oops, looks like something weird happended. Try reloading the page." ]


gameTime : Model -> String
gameTime model =
    model.resultDetailsView.gameResultDetails
        |> RemoteData.toMaybe
        |> Maybe.map .time
        |> Maybe.map formatDate
        |> Maybe.withDefault ""


onEnter : Model -> ( Model, Cmd Msg )
onEnter model =
    case model.route of
        LeagueRoute slug (ResultRoute resultId ResultDetails) ->
            ( model
                |> getViewModel
                |> setResultDetails Loading
                |> setGameInfo Loading
                |> setViewModel model
            , Cmd.batch
                [ GameStats.getGameResultStats model slug resultId
                , ElmGraphQL.loadGameResultDetails model resultId (ResultDetailsLoaded >> ResultDetailsViewMsgs)
                ]
            )

        _ ->
            ( model, Cmd.none )


update : ResultDetailsViewMsg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ResultDetailsLoaded response ->
            let
                resultDetailsView =
                    model.resultDetailsView

                newResultDetailsView =
                    { resultDetailsView | gameResultDetails = RemoteData.fromResult response }
            in
            handleGraphQLError response
                ( { model
                    | resultDetailsView = newResultDetailsView
                  }
                , RemoteData.fromResult response
                    |> RemoteData.map (.bggInfo >> .bggid >> ElmGraphQL.loadGameInfo model (GameInfoLoaded >> ResultDetailsViewMsgs))
                    |> RemoteData.withDefault Cmd.none
                )

        GameInfoLoaded response ->
            ( model
                |> getViewModel
                |> setGameInfo (RemoteData.fromResult response)
                |> setViewModel model
            , Cmd.none
            )


getViewModel : Model -> ResultDetailsViewModel
getViewModel model =
    model.resultDetailsView


setViewModel : Model -> ResultDetailsViewModel -> Model
setViewModel model viewModel =
    { model | resultDetailsView = viewModel }


setResultDetails : GraphQLData GameResult -> ResultDetailsViewModel -> ResultDetailsViewModel
setResultDetails details viewModel =
    { viewModel | gameResultDetails = details }


setGameInfo : GraphQLData GameInfo -> ResultDetailsViewModel -> ResultDetailsViewModel
setGameInfo info viewModel =
    { viewModel | gameInfo = info }
