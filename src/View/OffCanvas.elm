module View.OffCanvas exposing (view)

import Bootstrap.Button as Button
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Modal as Modal exposing (Visibility)
import Components.LoginModal as LoginModal
import Html exposing (..)
import Html.Attributes exposing (class, style)
import Html.Events exposing (onClick)
import Model.Main exposing (..)
import Msgs.Main exposing (..)


view : Model -> Html Msg
view model =
    div []
        [ genericModal model
        , loginModal model
        ]


loginModal : Model -> Html Msg
loginModal model =
    LoginModal.view model


genericModal : Model -> Html Msg
genericModal model =
    Modal.config (ModalMsg Modal.hidden)
        |> Modal.h5 [] [ text "Error" ]
        |> Modal.body []
            [ Grid.containerFluid []
                [ Grid.row []
                    [ Grid.col
                        [ Col.xs12
                        , Col.attrs
                            [ style "word-break" "break-word"
                            ]
                        ]
                        [ text model.error ]
                    ]
                ]
            ]
        |> Modal.footer []
            [ Button.button
                [ Button.outlinePrimary
                , Button.attrs [ onClick <| ModalMsg Modal.hidden ]
                ]
                [ text "Close" ]
            ]
        |> Modal.view model.modalState
