module View.League exposing (onEnter, view)

import Api.ElmGraphQL as ElmGraphQL
import Html exposing (..)
import Model.Main exposing (..)
import Model.Route exposing (..)
import Msgs.Main exposing (Msg)
import RemoteData exposing (RemoteData(..))
import Update.Utils exposing (batchUpdates)
import View.Charts as Charts
import View.EditResult as EditResult
import View.Games as Games
import View.NewResult as AddNewResult
import View.Players as Players
import View.ResultDetails as ResultDetails
import View.Results as Results
import View.Settings as Settings


view : Model -> Html Msg
view model =
    case getSubRoute model of
        AddNewResult ->
            AddNewResult.view model

        Overview ->
            text "Overview Sub Page"

        Charts ->
            Charts.view model

        Results ->
            Results.view model

        Games ->
            Games.view model

        Players ->
            Players.view model

        Settings ->
            Settings.view model

        ResultRoute resultId resultSubRoute ->
            case resultSubRoute of
                ResultDetails ->
                    ResultDetails.view model

                EditResult ->
                    EditResult.view model


getSubRoute : Model -> LeagueSubRoute
getSubRoute model =
    case model.route of
        LeagueRoute slug subRoute ->
            subRoute

        _ ->
            -- TODO
            Overview


onEnter : Model -> ( Model, Cmd Msg )
onEnter model =
    batchUpdates
        [ leagueOnEnter, subRouteOnEnter ]
        model


leagueOnEnter : Model -> ( Model, Cmd Msg )
leagueOnEnter model =
    case model.route of
        LeagueRoute slug subRoute ->
            if (model.league == NotAsked) || leagueChanged model slug then
                ( clearLeague model, ElmGraphQL.loadLeague model slug )

            else
                ( model, Cmd.none )

        _ ->
            ( model, Cmd.none )


subRouteOnEnter : Model -> ( Model, Cmd Msg )
subRouteOnEnter model =
    case model.route of
        LeagueRoute _ Charts ->
            Charts.onEnter model

        LeagueRoute _ Results ->
            Results.onEnter model

        LeagueRoute _ Players ->
            Players.onEnter model

        LeagueRoute _ Games ->
            Games.onEnter model

        LeagueRoute _ Settings ->
            Settings.onEnter model

        LeagueRoute _ (ResultRoute _ ResultDetails) ->
            ResultDetails.onEnter model

        LeagueRoute _ (ResultRoute _ EditResult) ->
            EditResult.onEnter model

        _ ->
            ( model, Cmd.none )


clearLeague : Model -> Model
clearLeague model =
    { model
        | league = Loading
        , timeseries = NotAsked
        , gameStats = NotAsked
        , gameResultsMeta = NotAsked
        , games = NotAsked
    }


leagueChanged : Model -> String -> Bool
leagueChanged model slug =
    case model.league of
        RemoteData.Success t ->
            t.slug /= slug

        _ ->
            False
