module View.Main exposing (onEnter, view)

import Html exposing (..)
import Html.Attributes exposing (class)
import Model.Main exposing (Model)
import Model.Route exposing (Route(..))
import Msgs.Main exposing (Msg)
import View.Content as Content exposing (view)
import View.Footer as Footer
import View.Header as Header exposing (view)
import View.OffCanvas as OffCanvas exposing (view)
import View.Offline as OfflineView
import View.SubHeader as SubHeader


view : Model -> List (Html Msg)
view model =
    case model.route of
        OfflineRoute ->
            [ OfflineView.view model ]

        _ ->
            [ header []
                [ Header.view model
                , SubHeader.view model
                ]
            , main_ [] [ Content.view model ]
            , Footer.view model
            , OffCanvas.view model
            , div [ class "version" ] [ text model.flags.version ]
            ]


onEnter : Model -> ( Model, Cmd Msg )
onEnter model =
    case model.route of
        OfflineRoute ->
            OfflineView.onEnter model

        _ ->
            Content.onEnter model
