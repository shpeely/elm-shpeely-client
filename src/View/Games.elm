module View.Games exposing (onEnter, update, view)

import Api.ElmGraphQL as ElmGraphQL
import Api.GameStats as GameStats
import Bootstrap.Alert as Alert
import Bootstrap.Button as Button
import Bootstrap.Dropdown as Dropdown exposing (DropdownItem)
import Bootstrap.Form as Form
import Bootstrap.Form.Input as Input
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Components.EmptyStateNotice exposing (renderEmptyState)
import Components.Spinner exposing (spinner)
import Components.Title exposing (pageSubTitle)
import FormatNumber
import FormatNumber.Locales exposing (usLocale)
import Helper.League as LeagueHelper exposing (bggLink, playerIdToName)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Model.Bootstrap exposing (BootstrapState)
import Model.GamesView exposing (GamesFilter, GamesSortOrder(..), GamesViewModel)
import Model.League exposing (GameInfo, League)
import Model.LoginData exposing (isLoggedIn)
import Model.Main exposing (Model)
import Model.Route exposing (Route(..))
import Msgs.BootstrapMsgs exposing (BootstrapMsg(..))
import Msgs.GamesViewMsgs exposing (..)
import Msgs.Main exposing (..)
import RemoteData exposing (RemoteData(..), WebData)
import Utils exposing (count, maybeLoad)


view : Model -> Html Msg
view model =
    div []
        [ pageSubTitle "Games"
        , case ( model.league, model.gameInfos ) of
            ( Success league, Just gameInfos ) ->
                case gameInfos of
                    [] ->
                        renderEmptyState
                            (isLoggedIn model.loginStatus)
                            league.slug
                            "No Games Yet"
                            "This league doesn't have any games yet."
                            "You haven't entered any results yet. To get some game statistics enter you first result!"

                    _ ->
                        div []
                            [ controls model.gamesView model.bootstrapState
                            , gamesList league gameInfos model.gamesView
                            ]

            ( Failure _, _ ) ->
                Alert.simpleDanger [] [ text "Failed to load data :( Please try again." ]

            ( NotAsked, _ ) ->
                Alert.simpleDanger [] [ text "Failed to load data :( Please try again." ]

            _ ->
                spinner
        ]


onEnter : Model -> ( Model, Cmd Msg )
onEnter model =
    case model.route of
        LeagueRoute slug _ ->
            ( model
            , Cmd.batch
                [ maybeLoad model.games <| ElmGraphQL.loadGames model slug
                , maybeLoad model.gameStats <| GameStats.getGameStats model slug
                ]
            )

        _ ->
            ( model, Cmd.none )


controls : GamesViewModel -> BootstrapState -> Html Msg
controls viewModel bootstrapState =
    Grid.row []
        [ Grid.col [ Col.xsAuto, Col.attrs [ class "col mb-3" ] ]
            [ orderDropdown viewModel bootstrapState ]
        , Grid.col [ Col.xsAuto, Col.attrs [ class "col mb-5" ] ]
            [ nameFilter viewModel.filter.name ]
        ]


nameFilter : String -> Html Msg
nameFilter searchText =
    Form.form []
        [ Form.group []
            [ Form.label [ for "game-search" ] [ text "Search Game" ]
            , Input.text
                [ Input.id "game-search"
                , Input.placeholder "Search game..."
                , Input.value searchText
                , Input.onInput (GamesViewMsgs << SetFilter << GamesFilter)
                ]
            ]
        ]


arrowUp : Html msg
arrowUp =
    span [ class "mr-2" ] [ text "↑" ]


arrowDown : Html msg
arrowDown =
    span [ class "mr-2" ] [ text "↓" ]


sortAndFilter : GamesViewModel -> List GameInfo -> List GameInfo
sortAndFilter viewModel games =
    games |> filter viewModel.filter |> sort viewModel.sortOrder


filter : GamesFilter -> List GameInfo -> List GameInfo
filter gamesFilter games =
    List.filter
        (.name >> String.toLower >> String.contains (String.toLower gamesFilter.name))
        games


sort : GamesSortOrder -> List GameInfo -> List GameInfo
sort order games =
    case order of
        NameAsc ->
            List.sortBy .name games

        NameDesc ->
            List.reverse <| sort NameAsc games

        PlayTimeAsc ->
            List.sortBy .playTime games

        PlayTimeDesc ->
            List.reverse <| sort PlayTimeAsc games

        WeightAsc ->
            List.sortBy .weight games

        WeightDesc ->
            List.reverse <| sort WeightAsc games

        NumberOfGamesAsc ->
            List.sortBy .count games

        NumberOfGamesDesc ->
            List.reverse <| sort NumberOfGamesAsc games

        MaxPlayersAsc ->
            List.sortBy .maxPlayers games

        MaxPlayersDesc ->
            List.reverse <| sort MaxPlayersAsc games


dropdownItemContent : Bool -> String -> Html msg
dropdownItemContent asc name =
    span [ class "mr-2" ]
        [ if asc then
            arrowUp

          else
            arrowDown
        , span [] [ text name ]
        ]


orderDesc : GamesSortOrder -> Html msg
orderDesc order =
    case order of
        NameAsc ->
            dropdownItemContent True "Name"

        NameDesc ->
            dropdownItemContent False "Name"

        PlayTimeAsc ->
            dropdownItemContent True "Play Time"

        PlayTimeDesc ->
            dropdownItemContent False "Play Time"

        WeightAsc ->
            dropdownItemContent True "Weight"

        WeightDesc ->
            dropdownItemContent False "Weight"

        NumberOfGamesAsc ->
            dropdownItemContent True "Number of Games"

        NumberOfGamesDesc ->
            dropdownItemContent False "Number of Games"

        MaxPlayersAsc ->
            dropdownItemContent True "Maximum Players"

        MaxPlayersDesc ->
            dropdownItemContent False "Maximum Players"


dropdownItem : GamesSortOrder -> DropdownItem Msg
dropdownItem order =
    Dropdown.buttonItem [ onClick (GamesViewMsgs <| SetOrder order) ] [ orderDesc order ]


orderDropdown : GamesViewModel -> BootstrapState -> Html Msg
orderDropdown { sortOrder } { gameOrderDropdownState } =
    div []
        [ div []
            [ label [] [ text "Sort Order" ] ]
        , Dropdown.dropdown
            gameOrderDropdownState
            { options = []
            , toggleMsg = BootstrapMsgs << GameOrderDropdownToggle
            , toggleButton =
                Dropdown.toggle [ Button.outlinePrimary ] [ orderDesc sortOrder ]
            , items =
                [ dropdownItem NameDesc
                , dropdownItem NameAsc
                , dropdownItem NumberOfGamesDesc
                , dropdownItem NumberOfGamesAsc
                , dropdownItem MaxPlayersDesc
                , dropdownItem MaxPlayersAsc
                , dropdownItem WeightDesc
                , dropdownItem WeightAsc
                , dropdownItem PlayTimeDesc
                , dropdownItem PlayTimeAsc
                ]
            }
        ]


singleGameStat : League -> GameInfo -> Html Msg
singleGameStat league gameStat =
    let
        playerName id =
            LeagueHelper.playerIdToName league id

        renderMaximas playerIds =
            playerIds
                |> List.map playerName
                |> count
                |> List.map
                    (\( name, count ) ->
                        if count > 1 then
                            name ++ " (" ++ String.fromInt count ++ "x)"

                        else
                            name
                    )
                |> String.join ", "
    in
    li [ class "media game-media-item" ]
        [ a
            [ href <| bggLink gameStat.bggid
            , rel "noopener noreferrer"
            , target "_blank"
            , class "game-media-item__thumbnail"
            ]
            [ img
                [ alt (gameStat.name ++ " thumbnail")
                , class "img-fluid pr-3"
                , src gameStat.thumbnail
                ]
                []
            ]
        , div [ class "media-body" ]
            [ h3 [ class "mt-0 mb-1 h4" ]
                [ text gameStat.name ]
            , Grid.row []
                [ Grid.col [ Col.md ]
                    [ dl [ class "row" ]
                        [ dt [ class "col-sm-6" ]
                            [ text "Number of games played" ]
                        , dd [ class "col-sm-6" ]
                            [ text <| String.fromInt gameStat.count ]
                        , dt [ class "col-sm-6" ]
                            [ text "Highscores" ]
                        , dd [ class "col-sm-6" ]
                            [ text <| renderMaximas gameStat.highscores ]
                        , dt [ class "col-sm-6" ]
                            [ text "Lowscores" ]
                        , dd [ class "col-sm-6" ]
                            [ text <| renderMaximas gameStat.lowscores ]
                        ]
                    ]
                , Grid.col [ Col.md, Col.attrs [ class "d-none d-sm-none d-md-block" ] ]
                    [ dl [ class "row" ]
                        [ dt [ class "col-sm-6" ]
                            [ text "Game weight" ]
                        , dd [ class "col-sm-6" ]
                            [ text <| FormatNumber.format usLocale gameStat.weight ]
                        , dt [ class "col-sm-6" ]
                            [ text "Maximum number of players" ]
                        , dd [ class "col-sm-6" ]
                            [ text <| String.fromInt gameStat.maxPlayers ]
                        , dt [ class "col-sm-6" ]
                            [ text "Time to play" ]
                        , dd [ class "col-sm-6" ]
                            [ text <| String.fromInt gameStat.playTime ++ " minutes" ]
                        ]
                    ]
                ]
            ]
        ]


gamesList : League -> List GameInfo -> GamesViewModel -> Html Msg
gamesList league gameInfos viewModel =
    ul
        [ class "list-unstyled" ]
        (gameInfos |> sortAndFilter viewModel |> List.map (singleGameStat league))


update : GamesViewMsg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SetOrder order ->
            ( model.gamesView |> (\a -> updateSortOrder a order) |> updateViewModel model
            , Cmd.none
            )

        SetFilter filter_ ->
            ( model.gamesView |> (\a -> updateGamesFilter a filter_) |> updateViewModel model
            , Cmd.none
            )


updateSortOrder : GamesViewModel -> GamesSortOrder -> GamesViewModel
updateSortOrder viewModel order =
    { viewModel | sortOrder = order }


updateGamesFilter : GamesViewModel -> GamesFilter -> GamesViewModel
updateGamesFilter viewModel filter_ =
    { viewModel | filter = filter_ }


updateViewModel : Model -> GamesViewModel -> Model
updateViewModel model viewModel =
    { model | gamesView = viewModel }
