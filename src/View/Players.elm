module View.Players exposing (onEnter, update, view)

import Api.GameStats as GameStatsApi
import Bootstrap.Alert as Alert
import Bootstrap.Button as Button
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Bootstrap.Grid as Grid
import Components.EmptyStateNotice exposing (renderEmptyState)
import Components.Spinner exposing (fullPageSpinner, spinner)
import Components.Title exposing (pageSubTitle)
import FormatNumber as FormatNumber
import FormatNumber.Locales exposing (usLocale)
import Helper.League as LeagueHelper
import Html exposing (Attribute, Html, div, p, span, text, th)
import Html.Attributes exposing (class, href)
import Model.League exposing (League)
import Model.LoginData exposing (isLoggedIn)
import Model.Main exposing (Model)
import Model.PlayersView exposing (PlayersViewModel)
import Model.Route exposing (LeagueSubRoute(..), Route(..))
import Model.Stats exposing (PlayerStats)
import Msgs.Main exposing (Msg(..))
import Msgs.PlayersViewMsgs exposing (PlayersViewMsg(..))
import RemoteData exposing (RemoteData(..), WebData)
import Routing
import Table exposing (HtmlDetails, Status(..), defaultCustomizations)


view : Model -> Html Msg
view model =
    div []
        [ pageSubTitle "Players"
        , Grid.row []
            [ Grid.col []
                [ case model.league of
                    Success league ->
                        case league.players of
                            [] ->
                                renderEmptyState
                                    (isLoggedIn model.loginStatus)
                                    league.slug
                                    "No Players Yet"
                                    "This league doesn't have any players yet."
                                    "You haven't entered any results yet. To get some player statistics enter you first result!"

                            _ ->
                                renderTable model league

                    Failure _ ->
                        Alert.simpleDanger [] [ text "Failed to league :( Please try to reload the page." ]

                    _ ->
                        fullPageSpinner
                ]
            ]
        ]


renderTable : Model -> League -> Html Msg
renderTable model league =
    case model.playerStats of
        Success stats ->
            Table.view (tableConfig league) model.playersView.tableState stats
                |> Html.map PlayersViewMsgs

        Failure _ ->
            Alert.simpleDanger [] [ text "Failed to load player stats :( Please try to reload the page." ]

        _ ->
            spinner


tableConfig : League -> Table.Config PlayerStats PlayersViewMsg
tableConfig league =
    Table.customConfig
        { toId = .playerId
        , toMsg = SetTableState
        , columns =
            [ Table.stringColumn "Player" (.playerId >> LeagueHelper.playerIdToName league)
            , Table.intColumn "Results" .numberOfResults
            , Table.intColumn "Wins" .wins
            , percentColumn "Win Ratio" .winRatio
            , Table.intColumn "High Scores" .highscores
            , Table.intColumn "Losses" .losses
            , percentColumn "Lose Ratio" .loseRatio
            , Table.intColumn "Low Scores" .lowscores
            ]
        , customizations =
            { defaultCustomizations
                | tableAttrs = [ class "table table-responsive table-hover player-stats-table" ]
                , thead = simpleThead
            }
        }


clickable : String -> Html msg
clickable text_ =
    span [ class "cursor-pointer" ] [ text text_ ]


simpleThead : List ( String, Status, Attribute msg ) -> HtmlDetails msg
simpleThead headers =
    HtmlDetails [] (List.map simpleTheadHelp headers)


simpleTheadHelp : ( String, Status, Attribute msg ) -> Html msg
simpleTheadHelp ( name, status, click ) =
    let
        content =
            case status of
                Unsortable ->
                    [ text name ]

                Sortable selected ->
                    [ clickable name
                    , if selected then
                        text " ↓"

                      else
                        text " ↓"
                    ]

                Reversible Nothing ->
                    [ clickable name
                    ]

                Reversible (Just isReversed) ->
                    [ clickable name
                    , text
                        (if isReversed then
                            " ↑"

                         else
                            " ↓"
                        )
                    ]
    in
    th [ click ] content


percentColumn : String -> (data -> Float) -> Table.Column data msg
percentColumn name accessorFn =
    Table.customColumn
        { name = name
        , viewData = accessorFn >> (*) 100 >> FormatNumber.format { usLocale | decimals = 2 } >> append "%"
        , sorter = Table.increasingOrDecreasingBy accessorFn
        }


onEnter : Model -> ( Model, Cmd Msg )
onEnter model =
    case model.route of
        LeagueRoute slug _ ->
            ( model
            , GameStatsApi.getPlayerStats model slug
            )

        _ ->
            ( model, Cmd.none )


update : PlayersViewMsg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SetTableState state ->
            ( model
                |> getViewModel
                |> setTableState state
                |> setViewModel model
            , Cmd.none
            )


append : String -> String -> String
append suffix s =
    s ++ suffix


setTableState : Table.State -> PlayersViewModel -> PlayersViewModel
setTableState tableState viewModel =
    { viewModel | tableState = tableState }


getViewModel : Model -> PlayersViewModel
getViewModel model =
    model.playersView


setViewModel : Model -> PlayersViewModel -> Model
setViewModel model viewModel =
    { model | playersView = viewModel }
