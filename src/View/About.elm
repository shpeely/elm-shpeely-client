module View.About exposing (view)

import Html exposing (..)
import Html.Attributes exposing (class, href)
import Model.Main exposing (Model)
import Msgs.Main exposing (Msg)


view : Model -> Html Msg
view model =
    div []
        [ h1 [] [ text "About" ]
        , p [] [ text "Shpeely is a web application to keep track of your board game scores and compare your performance among other players." ]
        , h2 [] [ text "Statistics" ]
        , p [] [ text "For each game with a certain number of players (eg. a 3 player game of “Through the Ages”) the following statistics are calculated:" ]
        , ul []
            [ li [] [ text "High score" ]
            , li [] [ text "Low score" ]
            , li [] [ text "Average score of this result" ]
            , li [] [ text "Average score of all results" ]
            , li [] [ text "The normalized and weighted score of each player (more on that further below)" ]
            ]
        , p [] [ text "The statistics for a specific game always take the number of players into account. This means that eg. a 3 player game of “Through the Ages” has a completely separate high-, low- and average score than a 4 player game of “Through the Ages”. This is because in many games players can achieve a higher or a lower score based on how many players take part." ]
        , p [] [ text "All statistics are always based only on the league where the result has been added to. The results of other users or other leagues will never affect your statistics in your league. In your league you will never see scores or results of players you don’t know." ]
        , h2 [] [ text "Normalized and Weighted Scoring" ]
        , p [] [ text "The unique feature that only Shpeely offers is the normalized scoring. The normalized scoring allows you to compare your board game performance with other players, no matter how often each player plays or how many different games you play. " ]
        , p [] [ text "The normalized scoring works as follows: Whenever you add a result to a league, each score of each player is normalized like so:" ]
        , code [ class "d-inline-block mb-3" ] [ text "([score of player X] - [average score of all games]) / [scores standard deviation of all games]" ]
        , p [] [ text "After that the normalized score is weighted by the BoardGameGeek’s weight of a game. If you’re not familiar with the BoardGameGeek’s weight this is how it’s explained on their website:" ]
        , p [] [ i [] [ text "Community rating for how difficult a game is to understand. Lower rating (lighter weight) means easier." ] ]
        , p [] [ text "Finally, the score is multiplied by 100 to avoid scores like 3.14 and get beautiful scores like 314." ]
        , h2 [] [ text "Free and Open-Source" ]
        , p []
            [ text "Shpeely is completely free to use and open-source. The entire code is available at "
            , a [ href "https://github.com/shpeely" ] [ text "https://github.com/shpeely" ]
            , text "."
            ]
        , p [] [ text "If you find a bug please open an issue on GitLab. Feature requests are also welcome." ]
        , p [] [ text "Are you a developer with too much free time? Feel free to contribute to Shpeely and open a pull request." ]
        , h2 [] [ text "Get in Touch" ]
        , p []
            [ text "Do you have a question about Shpeely? Feel free to write an email to "
            , a [ href "mailto:info@shpeely.com" ] [ text "info@shpeely.com" ]
            , text "."
            ]
        ]
