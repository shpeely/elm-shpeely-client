module View.Home exposing (view)

import Bootstrap.Alert as Alert
import Bootstrap.Button as Button
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Bootstrap.Carousel as Carousel
import Bootstrap.Carousel.Slide as Slide
import Bootstrap.Form as Form
import Bootstrap.Form.Input as Input
import Bootstrap.Grid as Grid exposing (Column)
import Bootstrap.Grid.Col as Col
import Bootstrap.Grid.Row as Row
import Components.GameResultCol as GameResultCol
import Components.LoginButtons exposing (loginButtons)
import Components.Spinner exposing (fullPageSpinner, spinner)
import Helper.Time exposing (formatDate)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onSubmit)
import Model.GraphQL exposing (GraphQLData, GraphQLError(..))
import Model.League exposing (LeagueSummary)
import Model.LoginData exposing (LoginStatus(..))
import Model.Main exposing (..)
import Model.Route exposing (..)
import Msgs.BootstrapMsgs exposing (BootstrapMsg(..))
import Msgs.LeagueMsgs exposing (LeagueMsg(..))
import Msgs.Main exposing (..)
import RemoteData exposing (..)
import Routing exposing (..)
import String.Extra exposing (pluralize)
import Template exposing (render, template, withString, withValue)


listLeagues : RemoteData e (List LeagueSummary) -> Html Msg
listLeagues leagues =
    case leagues of
        NotAsked ->
            spinner

        Loading ->
            spinner

        Failure _ ->
            div [] [ text "Error loading leagues :(" ]

        Success ts ->
            if List.length ts > 0 then
                Grid.row []
                    (ts
                        |> List.partition (.lastGameresultAt >> (==) Nothing)
                        |> (\( x, y ) -> List.append y x)
                        |> List.map leagueCard
                    )

            else
                Alert.simpleInfo [ class "mt-2" ]
                    [ Alert.h5 [] [ text "No Leagues yet..." ]
                    , p [] [ text "You are not yet a member of any league. You can either create a new league or ask a friend to invite you to an existing league." ]
                    ]


view : Model -> Html Msg
view model =
    div []
        (case model.loginStatus of
            LoggedIn _ ->
                [ createLeagueForm model
                , myLeagues model
                ]

            NotLoggedIn ->
                [ intro
                , carousel model
                , description
                , login model
                , recentResults model
                , recentLeagues model
                ]

            Checking ->
                [ fullPageSpinner ]
        )


login : Model -> Html Msg
login model =
    Grid.row [ Row.attrs [ class "my-5" ] ]
        [ Grid.col [ Col.sm4, Col.offsetSm4, Col.attrs [ class "my-1" ] ]
            [ loginButtons model.flags
            ]
        ]


intro : Html msg
intro =
    div []
        [ h2 [] [ text "Keeping Track of Board Game Results" ]
        ]


uspCard : List (Html.Html msg) -> Column msg
uspCard html_ =
    Grid.col [ Col.sm4, Col.attrs [ class "my-1" ] ]
        [ Card.config []
            |> Card.block []
                [ Block.text [] html_ ]
            |> Card.view
        ]


description : Html Msg
description =
    Grid.row [ Row.attrs [ class "my-5" ] ]
        [ uspCard
            [ h2 [] [ text "Awesome Statistics" ]
            , text "Shpeely will track all of your board game result and compute interesting statistics."
            ]
        , uspCard
            [ h2 [] [ text "Sophisticated Scoring" ]
            , text "Each result is scored with a normalized score and weighted by the complexity."
            ]
        , uspCard [ h2 [] [ text "Free and Open-Source" ], text "Shpeely is completely free to use and open-source." ]
        ]


carousel : Model -> Html Msg
carousel model =
    Carousel.config (BootstrapMsgs << CarouselMsg) []
        |> Carousel.withControls
        |> Carousel.withIndicators
        |> Carousel.slides
            [ Slide.config [] (Slide.image [] "/static/images/slide1.png")
                |> Slide.caption []
                    [ h4 [] [ text "Keep track of results" ]
                    , p [] [ text "See highscores, lowescores, averages at a glance and filter all results by player, game or number of players." ]
                    ]
            , Slide.config [] (Slide.image [] "/static/images/slide2.png")
                |> Slide.caption []
                    [ h4 [] [ text "Leaderboard and Trends" ]
                    , p [] [ text "Using a normalized and weighted score, all results can be compared and aggregated, resulting in a single player score" ]
                    ]
            , Slide.config [] (Slide.image [] "/static/images/slide3.png")
                |> Slide.caption []
                    [ h4 [] [ text "BoardGameGeek statistics directly built in" ]
                    , p []
                        [ text "Data from BoardGameGeek is synchronized and directly visible. The BoardGameGeek is used to weight the game results "
                        , i [] [ text "weight" ]
                        , text " is used to weight the game results"
                        ]
                    ]
            ]
        |> Carousel.view model.bootstrapState.carouselState


mapErrorMessage : Maybe GraphQLError -> String
mapErrorMessage err =
    case err of
        Just UniqueConstraintViolation ->
            "Sorry, this league name already exists."

        Nothing ->
            ""

        _ ->
            "Unknown GraphQL Error"


createLeagueForm : Model -> Html Msg
createLeagueForm model =
    Grid.row []
        [ Grid.col []
            [ h2 [] [ text "Create New League" ]
            ]
        , Grid.colBreak []
        , Grid.col [ Col.lg4 ]
            [ Form.form
                [ onSubmit (LeagueMsg OnNewLeagueSubmit)
                , class "form mb-5"
                ]
                [ Form.group []
                    [ Form.label [] [ text "League Name" ]
                    , Input.text
                        [ Input.placeholder "Enter League Name..."
                        , Input.onInput (LeagueMsg << OnLeagueNameChange)
                        ]
                    , Form.invalidFeedback []
                        [ text (mapErrorMessage model.createLeagueError) ]
                    ]
                , Form.group []
                    [ Button.button
                        [ Button.primary
                        , Button.attrs [ type_ "submit" ]
                        , Button.disabled <| model.newLeague.slug == Nothing
                        ]
                        [ text "Create League" ]
                    ]
                ]
            ]
        ]


recentLeagues : Model -> Html Msg
recentLeagues model =
    Grid.row []
        [ Grid.col []
            [ h2 [] [ text "Recent Leagues" ] ]
        , Grid.colBreak []
        , Grid.col [] [ listLeagues model.recentLeagues ]
        ]


recentResults : Model -> Html Msg
recentResults model =
    Grid.row [ Row.attrs [ class "mb-5" ] ]
        ([ Grid.col []
            [ h2 [] [ text "Recent Results" ] ]
         , Grid.colBreak []
         ]
            ++ (case model.recentResults of
                    Success results ->
                        List.map (\result -> GameResultCol.render model result (Success result.league)) results

                    _ ->
                        []
               )
        )


myLeagues : Model -> Html Msg
myLeagues model =
    case model.loginStatus of
        LoggedIn _ ->
            Grid.row []
                [ Grid.col []
                    [ h2 [] [ text "My Leagues" ] ]
                , Grid.colBreak []
                , Grid.col [] [ listLeagues model.myLeagues ]
                ]

        _ ->
            Grid.row [] []


leagueCard : LeagueSummary -> Grid.Column Msg
leagueCard league =
    Grid.col [ Col.xs12, Col.sm6, Col.md6, Col.lg4, Col.xl3 ]
        [ Card.config [ Card.attrs [ class "mb-5" ] ]
            |> Card.block []
                [ Block.titleH3 [ class "h5 eclipse-text" ] [ text league.name ]
                , Block.custom <|
                    div []
                        [ div
                            [ class "mb-2" ]
                            [ template ""
                                |> withValue (.numResults >> pluralize "result" "results")
                                |> withString ", "
                                |> withValue (.numPlayers >> pluralize "player" "players")
                                |> withString ", "
                                |> withValue (.numGames >> pluralize "game" "games")
                                |> render league
                                |> text
                            ]
                        , p [ class "text-muted" ]
                            [ template ""
                                |> withValue (.lastGameresultAt >> Maybe.map (always "Last updated ") >> Maybe.withDefault "Created ")
                                |> withValue (.lastGameresultAt >> Maybe.withDefault league.createdAt >> formatDate)
                                |> render league
                                |> text
                            ]
                        , Button.linkButton
                            [ Button.secondary
                            , Button.block
                            , Button.attrs (class "stretched-link" :: (viewHref <| LeagueRoute league.slug Charts))
                            ]
                            [ text "View League" ]
                        ]
                ]
            |> Card.view
        ]
