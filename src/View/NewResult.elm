module View.NewResult exposing (update, view)

import Api.ElmGraphQL as ElmGraphQL
import Bootstrap.Alert as Alert
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Components.GamePreview as GamePreview
import Components.LiveDropdown as LiveDropdown
import Components.ResultForm as ResultForm exposing (ResultFormState)
import Components.Spinner exposing (fullPageSpinner)
import Debounce exposing (Debounce)
import Dict exposing (Dict)
import ErrorHandler exposing (handleGraphQLError)
import Helper.League as LeagueHelper
import Html exposing (..)
import Html.Attributes exposing (class)
import Model.Bgg exposing (GameInfo, GameSearchResult)
import Model.GameResult exposing (ScoreInput, UnsavedGameResult)
import Model.GraphQL exposing (GraphQLData, GraphQLGameInfoResponse, GraphQLGameSearchResponse)
import Model.League exposing (Player)
import Model.LoginData exposing (LoginStatus(..), isLoggedIn)
import Model.Main exposing (..)
import Model.NewResultView exposing (NewResultViewModel, initialState)
import Model.Route exposing (Route(..))
import Msgs.Main exposing (Msg(..))
import Msgs.NewGameResultMsgs exposing (..)
import RemoteData exposing (RemoteData(..), WebData)


view : Model -> Html Msg
view model =
    case model.league of
        RemoteData.Success _ ->
            Grid.row []
                [ -- add new game result form if logged in
                  case model.loginStatus of
                    NotLoggedIn ->
                        Grid.col []
                            [ Alert.simpleInfo [] [ text "Please log in..." ]
                            ]

                    Checking ->
                        Grid.col []
                            [ fullPageSpinner ]

                    LoggedIn _ ->
                        newGameResultSection model
                ]

        _ ->
            fullPageSpinner


newGameResultSection : Model -> Grid.Column Msg
newGameResultSection model =
    Grid.col [ Col.sm12 ]
        [ Grid.row []
            [ Grid.col [ Col.md5, Col.sm12, Col.attrs [ class "mb-5" ] ]
                [ h2 [] [ text "Add New Game Result" ]
                , ResultForm.config "new-result-form" NewResultNoop
                    |> ResultForm.bggid (model.newResultView.gameInfo |> RemoteData.toMaybe |> Maybe.map .bggid)
                    |> ResultForm.scores model.newResultView.scores
                    |> ResultForm.onGameSearch OnGameSearch
                    |> ResultForm.gameSearchResults model.newResultView.searchResults
                    |> ResultForm.maxPlayers (model.newResultView.gameInfo |> RemoteData.map .maxPlayers |> RemoteData.withDefault 0)
                    |> ResultForm.onGameSelected OnGameSelected
                    |> ResultForm.onPlayerInput OnPlayerInput
                    |> ResultForm.onScoreInput OnScoreInput
                    |> ResultForm.onPlayerSelected OnPlayerSelected
                    |> ResultForm.onSubmit OnSubmit
                    |> ResultForm.playerSearchResults model.newResultView.playerSearchResults
                    |> ResultForm.playerInputs model.newResultView.playerInputs
                    |> ResultForm.submitted model.newResultView.formSubmitted
                    |> ResultForm.onLiveDropdownStateUpdate NewResultLiveDropdownMsg
                    |> ResultForm.isLoading model.newResultView.newResultIsSaving
                    |> ResultForm.render model.newResultView.resultFormState
                    |> Html.map NewGameResultMsgs
                ]
            , Grid.col
                [ Col.md6, Col.sm12, Col.offsetMd1 ]
                [ GamePreview.render model.newResultView.gameInfo 700 ]
            ]
        ]


debounceConfig : Debounce.Config Msg
debounceConfig =
    { strategy = Debounce.later 300
    , transform = NewGameResultMsgs << DebounceMsg
    }


update : NewGameResultMsg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NewResultNoop ->
            ( model, Cmd.none )

        NewResultLiveDropdownMsg state ->
            let
                msg_ =
                    Maybe.withDefault NewResultNoop (LiveDropdown.getMsg state)

                newModel =
                    model
                        |> getViewModel
                        |> setResultFormState (ResultForm.update model.newResultView.resultFormState (LiveDropdown.update state))
                        |> setViewModel model
            in
            update msg_ newModel

        OnGameSearch gameSearch ->
            let
                ( debounce, cmd ) =
                    Debounce.push debounceConfig gameSearch model.newResultView.debounceGameSearch
            in
            ( model
                |> getViewModel
                |> setGameSearchDebounce debounce
                |> setViewModel model
            , cmd
            )

        DebounceMsg msg_ ->
            let
                ( debounce, cmd ) =
                    Debounce.update
                        debounceConfig
                        (Debounce.takeLast (ElmGraphQL.searchGame model (OnGameSearchResults >> NewGameResultMsgs)))
                        msg_
                        model.newResultView.debounceGameSearch
            in
            ( model
                |> getViewModel
                |> setGameSearchDebounce debounce
                |> setViewModel model
            , cmd
            )

        OnGameSearchResults response ->
            ( model
                |> getViewModel
                |> setSearchResults response
                |> setViewModel model
            , Cmd.none
            )

        OnPlayerInput index playerSearch ->
            let
                foundPlayers =
                    model.league
                        |> RemoteData.map .players
                        |> RemoteData.map (List.filter (.name >> String.toLower >> String.contains (String.toLower playerSearch)))
                        |> RemoteData.withDefault []

                player =
                    if playerSearch == "" then
                        Nothing

                    else
                        Just playerSearch
            in
            ( model
                |> getViewModel
                |> setPlayerSearchResults index foundPlayers
                |> setPlayerInputs index playerSearch
                |> setPlayer index player
                |> setViewModel model
            , Cmd.none
            )

        OnScoreInput index score ->
            ( model
                |> getViewModel
                |> setScore index (String.toFloat score)
                |> setViewModel model
            , Cmd.none
            )

        OnPlayerSelected index playerName ->
            ( model
                |> getViewModel
                |> setPlayerInputs index playerName
                |> setPlayer index (Just playerName)
                |> setViewModel model
            , Cmd.none
            )

        OnSubmit ->
            let
                unsavedScores =
                    model.newResultView.scores
                        |> Dict.values
                        |> List.filterMap (\{ playerName, score } -> Maybe.map2 ScoreInput playerName score)
            in
            ( model
                |> getViewModel
                |> setSubmitted True
                |> setNewResultIsSaving True
                |> setViewModel model
            , case ( model.newResultView.gameInfo, model.league ) of
                ( Success { bggid }, Success { slug } ) ->
                    Cmd.batch
                        [ ElmGraphQL.createGameResult
                            model
                            slug
                            (UnsavedGameResult bggid unsavedScores)
                            (OnResultSaved >> NewGameResultMsgs)
                        ]

                _ ->
                    -- should never happen: Saving game before game info or result is loaded
                    Cmd.none
            )

        OnGameSelected bggid ->
            ( model
                |> getViewModel
                |> setGameInfo Loading
                |> setViewModel model
            , ElmGraphQL.loadGameInfo model (OnGameInfoLoaded >> NewGameResultMsgs) bggid
            )

        OnResultSaved result ->
            let
                newModel =
                    model
                        |> getViewModel
                        |> setNewResultIsSaving False
                        |> setViewModel model
                        |> LeagueHelper.resetLeague
            in
            -- reload league & recent leagues
            case newModel.route of
                LeagueRoute slug _ ->
                    ( initialState
                        |> setViewModel newModel
                    , Cmd.batch
                        [ ElmGraphQL.loadLeague newModel slug
                        , ElmGraphQL.loadMyLeagues model
                        ]
                    )

                _ ->
                    handleGraphQLError result ( newModel, Cmd.none )

        OnGameInfoLoaded response ->
            ( model
                |> getViewModel
                |> setGameInfo (RemoteData.fromResult response)
                |> setViewModel model
            , Cmd.none
            )


getViewModel : Model -> NewResultViewModel
getViewModel model =
    model.newResultView


setSubmitted : Bool -> NewResultViewModel -> NewResultViewModel
setSubmitted submitted viewModel =
    { viewModel | formSubmitted = submitted }


setViewModel : Model -> NewResultViewModel -> Model
setViewModel model viewModel =
    { model | newResultView = viewModel }


setResultFormState : ResultFormState NewGameResultMsg -> NewResultViewModel -> NewResultViewModel
setResultFormState state viewModel =
    { viewModel | resultFormState = state }


setSearchResults : GraphQLGameSearchResponse -> NewResultViewModel -> NewResultViewModel
setSearchResults results viewModel =
    { viewModel | searchResults = RemoteData.fromResult results |> RemoteData.toMaybe |> Maybe.withDefault [] }


setGameSearchDebounce : Debounce String -> NewResultViewModel -> NewResultViewModel
setGameSearchDebounce debounce viewModel =
    { viewModel | debounceGameSearch = debounce }


setGameInfo : GraphQLData GameInfo -> NewResultViewModel -> NewResultViewModel
setGameInfo gameInfo viewModel =
    { viewModel | gameInfo = gameInfo }


setPlayerSearchResults : Int -> List Player -> NewResultViewModel -> NewResultViewModel
setPlayerSearchResults index searchResults viewModel =
    { viewModel | playerSearchResults = Dict.insert index searchResults viewModel.playerSearchResults }


setPlayerInputs : Int -> String -> NewResultViewModel -> NewResultViewModel
setPlayerInputs index playerInput viewModel =
    { viewModel | playerInputs = Dict.insert index playerInput viewModel.playerInputs }


setNewResultIsSaving : Bool -> NewResultViewModel -> NewResultViewModel
setNewResultIsSaving newResultIsSaving viewModel =
    { viewModel | newResultIsSaving = newResultIsSaving }


setPlayer : Int -> Maybe String -> NewResultViewModel -> NewResultViewModel
setPlayer index playerName viewModel =
    { viewModel
        | scores =
            Dict.update
                index
                (\score ->
                    case score of
                        Just s ->
                            Just { s | playerName = playerName }

                        Nothing ->
                            Just { playerName = playerName, score = Nothing }
                )
                viewModel.scores
    }


setScore : Int -> Maybe Float -> NewResultViewModel -> NewResultViewModel
setScore index score viewModel =
    { viewModel
        | scores =
            Dict.update
                index
                (\score_ ->
                    case score_ of
                        Just s ->
                            Just { s | score = score }

                        Nothing ->
                            Just { playerName = Nothing, score = score }
                )
                viewModel.scores
    }
