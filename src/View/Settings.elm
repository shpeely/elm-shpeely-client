module View.Settings exposing (onEnter, update, view)

import Api.ElmGraphQL as ElmGraphQL
import Bootstrap.Alert as Alert
import Bootstrap.Button as Button
import Bootstrap.Dropdown as Dropdown exposing (DropdownItem)
import Bootstrap.Form as Form
import Bootstrap.Form.Input as Input
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Table as Table
import Components.Spinner exposing (spinner)
import Components.Title exposing (pageSubTitle)
import Dict
import Helper.League as LeagueHelper exposing (hasRole)
import Html exposing (..)
import Html.Attributes exposing (class, href, src, type_)
import Html.Events exposing (onClick, onSubmit)
import Model.Bootstrap exposing (BootstrapState)
import Model.GraphQL exposing (GraphQLData)
import Model.Invitation exposing (Invitation)
import Model.League exposing (League, LeagueMember, MemberRole(..))
import Model.LoginData exposing (LoginStatus(..))
import Model.Main exposing (..)
import Model.SettingsView exposing (SettingsViewModel)
import Model.User exposing (User)
import Msgs.BootstrapMsgs exposing (BootstrapMsg(..))
import Msgs.Main exposing (..)
import Msgs.SettingsViewMsgs exposing (..)
import RemoteData exposing (RemoteData(..))


view : Model -> Html Msg
view model =
    Grid.container []
        [ Grid.row []
            [ Grid.col
                []
                [ pageSubTitle "League Settings"
                ]
            ]
        , case model.league of
            Loading ->
                Grid.row [] [ Grid.col [] [ spinner ] ]

            Success league ->
                Grid.row []
                    [ Grid.col [ Col.xs12 ] [ h3 [] [ text "Members" ] ]
                    , Grid.col [ Col.xs12 ]
                        [ renderMembers model league ]
                    , Grid.col [ Col.xs12 ] [ h4 [] [ text "Invite User" ] ]
                    , Grid.col [ Col.lg4 ]
                        [ Form.form
                            [ onSubmit (SettingsViewMsgs <| OnSubmitInvitation league)
                            , class "form"
                            ]
                            [ Form.group
                                []
                                [ Form.label [] [ text "Email Address of User to Invite" ]
                                , Input.email
                                    [ Input.placeholder "Enter email address of user to invite..."
                                    , Input.onInput (SettingsViewMsgs << OnInvitationEmailInput)
                                    , Input.value model.settingsView.newMemberEmail
                                    ]
                                , Form.invalidFeedback []
                                    [ text "" ]
                                ]
                            , Form.group []
                                [ Button.button
                                    [ Button.primary
                                    , Button.attrs [ type_ "submit" ]
                                    ]
                                    [ text "Send Invitation" ]
                                ]
                            ]
                        ]
                    , Grid.col [ Col.lg8 ]
                        [ invitationSentAlert model.settingsView.lastInvitedEmail
                        ]
                    ]

            NotAsked ->
                text ""

            Failure _ ->
                text "Oops, looks like something went wrong... Please reload the page. Sorry for the inconvenience."
        ]


invitationSentAlert : Maybe String -> Html msg
invitationSentAlert maybeEmail =
    case maybeEmail of
        Just email ->
            Alert.simpleSuccess
                [ class "mt-lg-4" ]
                [ text ("An email with the invitation link was sent to " ++ email ++ ".") ]

        Nothing ->
            text ""


renderMembers : Model -> League -> Html Msg
renderMembers model league =
    Table.table
        { options =
            [ Table.small
            , Table.attr (class "table-vertical-center")
            ]
        , thead =
            Table.simpleThead
                []
        , tbody =
            Table.tbody []
                (List.map (renderMember model) league.members)
        }


roleToText : MemberRole -> String
roleToText role =
    case role of
        Admin ->
            "Admin"

        Member ->
            "Member"


shouldAllowRoleChange : Model -> LeagueMember -> Bool
shouldAllowRoleChange model leagueMember =
    case model.league of
        Success t ->
            case model.loginStatus of
                LoggedIn _ ->
                    if
                        (List.map
                            (\m ->
                                if m.role == Admin then
                                    1

                                else
                                    0
                            )
                            t.members
                            |> List.sum
                        )
                            > 1
                    then
                        True

                    else
                    -- Admin cannot be changed if there is only one admin
                    if
                        leagueMember.role == Admin
                    then
                        False

                    else
                        True

                _ ->
                    False

        _ ->
            False


renderMember : Model -> LeagueMember -> Table.Row Msg
renderMember model member =
    Table.tr []
        [ Table.td []
            [ img [ src member.user.avatar, class "avatar mx-2" ] []
            , text member.user.username
            ]
        , Table.td
            [ Table.cellAttr (class "text-right") ]
            [ case shouldAllowRoleChange model member of
                True ->
                    renderRoleDropdown model.bootstrapState member

                False ->
                    text (roleToText member.role)
            ]
        , Table.td
            [ Table.cellAttr (class "font-italic text-right pl-3 d-none d-sm-none d-md-table-cell")
            ]
            [ span [] [ text <| getCapabilities member.role ] ]
        ]


renderRoleDropdown : BootstrapState -> LeagueMember -> Html Msg
renderRoleDropdown bootstrapState member =
    let
        dropdown state =
            Dropdown.dropdown state
                { options = []
                , toggleMsg = ToggleMemberRoleDropdown member.id >> BootstrapMsgs
                , toggleButton =
                    Dropdown.toggle [ Button.outlinePrimary ] [ text <| roleToText member.role ]
                , items =
                    memberDropdownItems member
                }
    in
    case Dict.get member.id bootstrapState.memberRoleDropdownState of
        Just state ->
            dropdown state

        Nothing ->
            dropdown Dropdown.initialState


roleText : MemberRole -> String
roleText role =
    case role of
        Member ->
            "Member"

        Admin ->
            "Admin"


memberDropdownItems : LeagueMember -> List (DropdownItem Msg)
memberDropdownItems member =
    [ Dropdown.buttonItem
        [ onClick (OnChangeMemberRole member.id Admin |> SettingsViewMsgs) ]
        [ text <| roleText Admin ]
    , Dropdown.buttonItem
        [ onClick (OnChangeMemberRole member.id Member |> SettingsViewMsgs) ]
        [ text <| roleText Member ]
    ]


getCapabilities : MemberRole -> String
getCapabilities role =
    case role of
        Admin ->
            "Can add, edit and delete results, invite members, change member roles"

        Member ->
            "Can add results and invite members"


onEnter : Model -> ( Model, Cmd Msg )
onEnter model =
    ( model, Cmd.none )


update : SettingsViewMsg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        OnInvitationEmailInput input ->
            let
                settingsViewModel =
                    model.settingsView
            in
            ( { model | settingsView = updateNewMemberEmail input model.settingsView }, Cmd.none )

        OnSubmitInvitation league ->
            ( model, ElmGraphQL.createInvitation model league.slug model.settingsView.newMemberEmail )

        OnInvitationCreated invitation ->
            let
                newModel =
                    model.settingsView
                        |> updateLastInvitedEmail
                        |> updateNewMemberEmail ""
            in
            ( { model | settingsView = newModel }, Cmd.none )

        OnChangeMemberRole memberId role ->
            -- optimistic model update
            ( model.league
                |> setMemberRole memberId role
                |> setLeague model
            , ElmGraphQL.setMemberRole model memberId role (MemberRoleUpdated >> SettingsViewMsgs)
            )

        MemberRoleUpdated response ->
            ( model, Cmd.none )


getViewModel : Model -> SettingsViewModel
getViewModel model =
    model.settingsView


setViewModel : Model -> SettingsViewModel -> Model
setViewModel model viewModel =
    { model | settingsView = viewModel }


updateNewMemberEmail : String -> SettingsViewModel -> SettingsViewModel
updateNewMemberEmail email viewModel =
    { viewModel | newMemberEmail = email }


updateLastInvitedEmail : SettingsViewModel -> SettingsViewModel
updateLastInvitedEmail viewModel =
    { viewModel | lastInvitedEmail = Just viewModel.newMemberEmail }


setLeague : Model -> GraphQLData League -> Model
setLeague model league =
    { model | league = league }


setMemberRole : String -> MemberRole -> GraphQLData League -> GraphQLData League
setMemberRole memberId role league =
    RemoteData.map (changeMemberRole memberId role) league


changeMemberRole : String -> MemberRole -> League -> League
changeMemberRole memberId role league =
    let
        members =
            league.members
                |> List.map
                    (\m ->
                        if m.id == memberId then
                            { m | role = role }

                        else
                            m
                    )
    in
    { league | members = members }
