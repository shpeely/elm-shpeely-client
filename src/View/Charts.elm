module View.Charts exposing (chartInput, onEnter, pastMonthsDropdownItems, pastYearsDropdown, playerCheckbox, playerCheckboxes, playerFilterDropdown, playerFilterName, playerSelection, scoresChart, settings, timeFilterDropdown, timeFilterDropdownItem, timeFilterName, timeseriesChart, view, yearsDropdownItems)

import Api.ElmGraphQL as ElmGraphQL
import Api.GameStats as GameStats
import Bootstrap.Button as Button
import Bootstrap.Dropdown as Dropdown
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Components.ScoresChart as ScoresChart
import Components.TimeSeriesChart as TimeSeriesChart
import Components.Title exposing (pageSubTitle, subSubTitle)
import Config exposing (numberOfResultsForRecentPlayers)
import Dict exposing (Dict)
import Helper.ChartSettings as ChartSettingsHelper
import Helper.League as LeagueHelper
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Maybe exposing (Maybe)
import Model.BasicTypes exposing (Size)
import Model.League exposing (League, Player)
import Model.Main exposing (Model)
import Model.Route exposing (Route(..))
import Model.Stats exposing (..)
import Msgs.BootstrapMsgs exposing (..)
import Msgs.ChartsViewMsgs exposing (..)
import Msgs.Main exposing (..)
import RemoteData exposing (RemoteData(..))
import Set exposing (Set)
import Time exposing (Posix)
import Update.ChartsViewUpdate exposing (getViewModel, setRecentResults, setViewModel)
import Utils exposing (maybeLoad)


view : Model -> Html Msg
view model =
    div []
        [ pageSubTitle "Charts"
        , settings model
        , subSubTitle "Scores"
        , scoresChart model
        , subSubTitle "Performance over Time"
        , timeseriesChart model
        ]


onEnter : Model -> ( Model, Cmd Msg )
onEnter model =
    case model.route of
        LeagueRoute slug _ ->
            ( if model.chartsView.recentResults == NotAsked then
                model
                    |> getViewModel
                    |> setRecentResults Loading
                    |> setViewModel model

              else
                model
            , Cmd.batch
                [ maybeLoad model.timeseries (GameStats.getTimeseries model slug)
                , maybeLoad model.gameStats (GameStats.getGameStats model slug)
                , maybeLoad model.gameResultsMeta (ElmGraphQL.loadGameResultMeta model slug)
                , maybeLoad model.chartsView.recentResults (ElmGraphQL.loadGameResultsWithLimit numberOfResultsForRecentPlayers model slug (RecentGameResultsResponse >> ChartsViewMsgs))
                ]
            )

        _ ->
            ( model, Cmd.none )


chartInput :
    Model
    ->
        Maybe
            { league : League
            , timeseries : TimeSeries
            , gameResultMeta : Dict String Posix
            , currentTime : Posix
            , windowSize : Size
            , settings : ChartSettings
            }
chartInput model =
    Maybe.map3
        (\s t m ->
            { timeseries = s
            , league = t
            , gameResultMeta = m
            , currentTime = model.currentTime
            , windowSize = model.windowSize
            , settings = model.chartSettings
            }
        )
        (RemoteData.toMaybe model.timeseries)
        (RemoteData.toMaybe model.league)
        (RemoteData.toMaybe model.gameResultsMeta)


timeseriesChart : Model -> Html Msg
timeseriesChart model =
    TimeSeriesChart.render <| chartInput model


scoresChart : Model -> Html Msg
scoresChart model =
    ScoresChart.render <| chartInput model


settings : Model -> Html Msg
settings model =
    div []
        [ Grid.row []
            [ Grid.col [ Col.xsAuto, Col.attrs [ class "col mb-2" ] ]
                [ timeFilterDropdown model ]
            , Grid.col [ Col.xsAuto, Col.attrs [ class "col mb-2" ] ]
                [ playerFilterDropdown model ]
            ]
        , Grid.row []
            [ Grid.col []
                [ playerSelection model ]
            ]
        ]


playerFilterName : PlayerFilter -> String
playerFilterName filter =
    case filter of
        AllPlayers ->
            "All Players"

        RecentPlayers _ ->
            "Recent Players"

        SelectedPlayers _ ->
            "Select Players..."


playerFilterDropdown : Model -> Html Msg
playerFilterDropdown model =
    div []
        [ Dropdown.dropdown
            model.bootstrapState.playerFilterDropdownState
            { options = []
            , toggleMsg = BootstrapMsgs << PlayerFilterDropwdownToggle
            , toggleButton =
                Dropdown.toggle
                    [ Button.outlinePrimary ]
                    [ text <| playerFilterName model.chartSettings.playerFilter ]
            , items =
                [ Dropdown.buttonItem
                    [ onClick (ChartsViewMsgs (SelectPlayerFilter AllPlayers)) ]
                    [ text <| playerFilterName AllPlayers ]
                , Dropdown.buttonItem
                    [ onClick (ChartsViewMsgs (SelectPlayerFilter <| RecentPlayers <| LeagueHelper.recentPlayerIds model.chartsView.recentResults)) ]
                    [ text <| playerFilterName (RecentPlayers Set.empty) ]
                , Dropdown.buttonItem
                    [ onClick (ChartsViewMsgs <| SelectPlayerFilter <| SelectedPlayers (LeagueHelper.playerIds model.league |> Set.fromList)) ]
                    [ text <| playerFilterName (SelectedPlayers Set.empty) ]
                ]
            }
        ]


timeFilterName : TimeFilter -> String
timeFilterName filter =
    case filter of
        AllTime ->
            "All Time"

        CurrentYear ->
            "Current Year"

        CurrentMonth ->
            "Current Month"

        Year_ year ->
            "Year " ++ String.fromInt year

        PastMonths months ->
            "Past " ++ String.fromInt months ++ " Months"

        PastYears years ->
            "Past " ++ String.fromInt years ++ " Years"

        _ ->
            -- TODO
            ""


yearsDropdownItems : Model -> List (Dropdown.DropdownItem Msg)
yearsDropdownItems model =
    case model.gameResultsMeta of
        Success gameResultMetaDict ->
            ChartSettingsHelper.pastLeagueYears
                (Dict.values gameResultMetaDict)
                model.currentTime
                |> List.map (timeFilterDropdownItem << Year_)

        _ ->
            []


pastMonthsDropdownItems : List (Dropdown.DropdownItem Msg)
pastMonthsDropdownItems =
    [ 3, 6 ]
        |> List.map (timeFilterDropdownItem << PastMonths)


pastYearsDropdown : Model -> List (Dropdown.DropdownItem Msg)
pastYearsDropdown model =
    case model.gameResultsMeta of
        Success gameResultsMeta ->
            let
                maxPastYears =
                    ChartSettingsHelper.pastLeagueYears (Dict.values gameResultsMeta) model.currentTime
                        |> List.length
            in
            List.range 1 maxPastYears
                |> List.map (timeFilterDropdownItem << PastYears)

        _ ->
            []


timeFilterDropdownItem : TimeFilter -> Dropdown.DropdownItem Msg
timeFilterDropdownItem timeFilter =
    Dropdown.buttonItem
        [ onClick (ChartsViewMsgs (SetTimeFilter timeFilter)) ]
        [ text <| timeFilterName timeFilter ]


timeFilterDropdown : Model -> Html Msg
timeFilterDropdown model =
    div []
        [ Dropdown.dropdown
            model.bootstrapState.timeFilterDropdownState
            { options = []
            , toggleMsg = BootstrapMsgs << TimeFilterDropwdownToggle
            , toggleButton =
                Dropdown.toggle
                    [ Button.outlinePrimary ]
                    [ text <| timeFilterName model.chartSettings.timeFilter ]
            , items =
                [ timeFilterDropdownItem AllTime
                , timeFilterDropdownItem CurrentYear
                , timeFilterDropdownItem CurrentMonth
                ]
                    ++ yearsDropdownItems model
                    ++ pastMonthsDropdownItems
                    ++ pastYearsDropdown model
            }
        ]


playerCheckbox : ChartSettings -> Player -> Html Msg
playerCheckbox settings_ player =
    let
        active =
            ChartSettingsHelper.acceptPlayer settings_.playerFilter player.id
    in
    div
        [ class "custom-control custom-checkbox"
        , onClick (ChartsViewMsgs (SelectPlayer player.id (not active)))
        ]
        [ input
            [ class "custom-control-input"
            , checked active
            , onCheck (ChartsViewMsgs << SelectPlayer player.id)
            , type_ "checkbox"
            , id ("check-" ++ player.id)
            ]
            []
        , label
            [ class "custom-control-label" ]
            [ text player.name ]
        ]


playerSelection : Model -> Html Msg
playerSelection model =
    case model.chartSettings.playerFilter of
        AllPlayers ->
            text ""

        RecentPlayers _ ->
            text ""

        SelectedPlayers _ ->
            div
                [ class "my-2" ]
                [ div
                    [ class "my-1" ]
                    [ Button.button
                        [ Button.attrs [ class "mr-2" ]
                        , Button.outlinePrimary
                        , Button.small
                        , Button.onClick (ChartsViewMsgs SelectAllPlayers)
                        ]
                        [ text "select all" ]
                    , Button.button
                        [ Button.outlinePrimary
                        , Button.small
                        , Button.onClick (ChartsViewMsgs UnselectAllPlayers)
                        ]
                        [ text "unselect all" ]
                    ]
                , playerCheckboxes model
                ]


playerCheckboxes : Model -> Html Msg
playerCheckboxes model =
    div []
        (LeagueHelper.players model.league
            |> List.sortBy .name
            |> List.map (playerCheckbox model.chartSettings)
        )
