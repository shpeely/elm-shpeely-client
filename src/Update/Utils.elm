module Update.Utils exposing (batchUpdates)

import Model.Main exposing (Model)
import Msgs.Main exposing (Msg)


batchUpdates : List (Model -> ( Model, Cmd Msg )) -> Model -> ( Model, Cmd Msg )
batchUpdates updateFns model =
    List.foldl (\fn ( m, c ) -> fn m |> foldUpdate ( m, c )) ( model, Cmd.none ) updateFns


foldUpdate : ( Model, Cmd Msg ) -> ( Model, Cmd Msg ) -> ( Model, Cmd Msg )
foldUpdate ( m1, c1 ) ( m2, c2 ) =
    ( m2, Cmd.batch [ c1, c2 ] )
