module Update.LoginModalUpdate exposing (update)

import Bootstrap.Modal as Modal
import Model.LoginModal exposing (LoginModal)
import Model.Main exposing (Model)
import Msgs.LoginModalMsgs exposing (LoginModalMsg(..))
import Msgs.Main exposing (Msg)
import Ports.LocalStorage.LocalStorage as LocalStorage
import Url


update : LoginModalMsg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        CloseModal ->
            getModalModel model
                |> setModalVisibility Modal.hidden
                |> setModel model
                |> toModelCmd (LocalStorage.deleteRedirectUrl ())

        ShowModal ->
            getModalModel model
                |> setModalVisibility Modal.shown
                |> setModel model
                |> toModelCmd (LocalStorage.saveRedirectUrl <| Url.toString model.url)

        AnimateModal visibility ->
            getModalModel model
                |> setModalVisibility visibility
                |> setModel model
                |> toModelCmdNone


getModalModel : Model -> LoginModal
getModalModel model =
    .loginModal model


setModalModel : LoginModal -> Model -> Model
setModalModel loginModal model =
    { model | loginModal = loginModal }


setModalVisibility : Modal.Visibility -> LoginModal -> LoginModal
setModalVisibility visibility loginModal =
    { loginModal | visibility = visibility }


setModel : Model -> LoginModal -> Model
setModel model loginModal =
    { model | loginModal = loginModal }


toModelCmd : Cmd msg -> Model -> ( Model, Cmd msg )
toModelCmd cmd model =
    ( model, cmd )


toModelCmdNone : Model -> ( Model, Cmd msg )
toModelCmdNone =
    toModelCmd Cmd.none
