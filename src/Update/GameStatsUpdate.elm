module Update.GameStatsUpdate exposing (update)

import Api.GameStats as GameStatsApi
import Dict
import ErrorHandler
import Helper.League as LeagueHelper
import Model.Main exposing (..)
import Msgs.GameStatsMsgs exposing (..)
import Msgs.Main exposing (..)
import RemoteData exposing (RemoteData(..))


update : GameStatsMsg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        TimeSeriesResponse timeseries ->
            ErrorHandler.handleRequestError timeseries ( { model | timeseries = timeseries }, Cmd.none )

        GameResultStatsResponse gameResultStats ->
            let
                newStats =
                    case gameResultStats of
                        Success stats ->
                            Dict.insert stats.id gameResultStats model.gameResultStats

                        _ ->
                            model.gameResultStats
            in
            ErrorHandler.handleRequestError gameResultStats ( { model | gameResultStats = newStats }, Cmd.none )

        LoadGameResultStats leagueId resultIds ->
            let
                newStats =
                    resultIds
                        |> List.map (\id -> ( id, Loading ))
                        |> Dict.fromList
                        |> Dict.union model.gameResultStats

                cmd =
                    resultIds
                        |> List.map (GameStatsApi.getGameResultStats model leagueId)
                        |> Cmd.batch
            in
            ( { model | gameResultStats = newStats }, cmd )

        GameStatsResponse stats ->
            let
                newModel =
                    { model
                        | gameStats = stats
                        , gameInfos = LeagueHelper.mergeGameInfos model.games stats
                    }
            in
            ErrorHandler.handleRequestError stats ( newModel, Cmd.none )

        PlayerStatsResponse stats ->
            ErrorHandler.handleRequestError stats
                ( { model
                    | playerStats = stats
                  }
                , Cmd.none
                )
