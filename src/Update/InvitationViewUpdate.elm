module Update.InvitationViewUpdate exposing (update)

import Api.ElmGraphQL as ElmGraphQL
import Helper.League as LeagueHelper
import Model.Invitation exposing (..)
import Model.Main exposing (Model)
import Model.Route exposing (Route(..))
import Msgs.InvitationViewMsgs exposing (InvitationViewMsg(..))
import Msgs.Main exposing (Msg)


update : InvitationViewMsg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AcceptInvitation invitation ->
            case model.route of
                InvitationRoute token ->
                    ( model, ElmGraphQL.acceptInvitation model token )

                _ ->
                    ( model, Cmd.none )

        RejectInvitation invitation ->
            case model.route of
                InvitationRoute token ->
                    ( model, ElmGraphQL.rejectInvitation model token )

                _ ->
                    ( model, Cmd.none )
