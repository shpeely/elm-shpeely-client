module Update.LeagueUpdate exposing (update)

import Api.ElmGraphQL as ElmGraphQL exposing (createLeague)
import Model.Main exposing (..)
import Msgs.LeagueMsgs exposing (..)
import Msgs.Main exposing (..)
import Slug


update : LeagueMsg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        OnNewLeagueSubmit ->
            ( model, createLeague model )

        OnLeagueNameChange name ->
            let
                newLeague =
                    { name = name
                    , slug = Slug.generate name
                    }
            in
            ( { model | newLeague = newLeague }, Cmd.none )
