module Update.BootstrapUpdate exposing (update)

import Bootstrap.Carousel as Carousel
import Bootstrap.Dropdown as Dropdown
import Dict exposing (Dict)
import Model.Bootstrap exposing (BootstrapState)
import Model.Main exposing (Model)
import Msgs.BootstrapMsgs exposing (BootstrapMsg(..))
import Msgs.Main exposing (Msg)


update : BootstrapMsg -> BootstrapState -> ( BootstrapState, Cmd Msg )
update msg model =
    case msg of
        PlayerFilterDropwdownToggle state ->
            ( { model | playerFilterDropdownState = state }, Cmd.none )

        TimeFilterDropwdownToggle state ->
            ( { model | timeFilterDropdownState = state }, Cmd.none )

        NumPlayersDropdownToggle state ->
            ( { model | numPlayersFilterDropdownState = state }, Cmd.none )

        GameOrderDropdownToggle state ->
            ( { model | gameOrderDropdownState = state }, Cmd.none )

        ToggleResultMoreDropdown id state ->
            let
                newModel =
                    model
                        |> getResultDropdownStates
                        |> toggleDropdownStateDict id state
                        |> setResultDropdownStates model
            in
            ( newModel, Cmd.none )

        ToggleMemberRoleDropdown id state ->
            let
                newModel =
                    model
                        |> getMembersDropdownStates
                        |> toggleDropdownStateDict id state
                        |> setMembersDropdownStates model
            in
            ( newModel, Cmd.none )

        CarouselMsg carouselMsg ->
            ( { model | carouselState = Carousel.update carouselMsg model.carouselState }, Cmd.none )


getResultDropdownStates : BootstrapState -> Dict String Dropdown.State
getResultDropdownStates bootstrapState =
    bootstrapState.resultDropdownState


getMembersDropdownStates : BootstrapState -> Dict String Dropdown.State
getMembersDropdownStates bootstrapState =
    bootstrapState.memberRoleDropdownState


toggleDropdownStateDict : String -> Dropdown.State -> Dict String Dropdown.State -> Dict String Dropdown.State
toggleDropdownStateDict id newState currentStates =
    Dict.insert id newState currentStates


setResultDropdownStates : BootstrapState -> Dict String Dropdown.State -> BootstrapState
setResultDropdownStates bootstrapState resultDropdownStates =
    { bootstrapState | resultDropdownState = resultDropdownStates }


setMembersDropdownStates : BootstrapState -> Dict String Dropdown.State -> BootstrapState
setMembersDropdownStates bootstrapState memberRoleDropdownState =
    { bootstrapState | memberRoleDropdownState = memberRoleDropdownState }
