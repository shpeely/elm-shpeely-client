module Update.ChartsViewUpdate exposing (getViewModel, setRecentResults, setViewModel, update)

import ErrorHandler
import Helper.League as LeagueHelper
import Model.ChartsView exposing (ChartsViewModel)
import Model.GameResult exposing (GameResult)
import Model.GraphQL exposing (GraphQLData)
import Model.Main exposing (Model)
import Model.Stats exposing (ChartSettings, PlayerFilter(..))
import Msgs.ChartsViewMsgs exposing (ChartsViewMsg(..))
import Msgs.Main exposing (..)
import RemoteData
import Set


update : ChartsViewMsg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SetTimeFilter timeFilter ->
            let
                chartSettings =
                    model.chartSettings
            in
            ( { model | chartSettings = { chartSettings | timeFilter = timeFilter } }, Cmd.none )

        SelectPlayerFilter playerFilter ->
            let
                chartSettings =
                    model.chartSettings
            in
            ( { model | chartSettings = { chartSettings | playerFilter = playerFilter } }, Cmd.none )

        SelectPlayer playerId selected ->
            let
                chartSettings =
                    model.chartSettings

                filterPlayers : PlayerFilter -> PlayerFilter
                filterPlayers playerFilter =
                    case playerFilter of
                        RecentPlayers playerIds ->
                            filterPlayers <| SelectedPlayers playerIds

                        AllPlayers ->
                            filterPlayers <| SelectedPlayers (LeagueHelper.playerIds model.league |> Set.fromList)

                        SelectedPlayers playerIds ->
                            playerIds
                                |> Set.filter (\id -> playerId /= id)
                                |> Set.union
                                    (if selected then
                                        Set.singleton playerId

                                     else
                                        Set.empty
                                    )
                                |> SelectedPlayers
            in
            ( { model
                | chartSettings =
                    { chartSettings | playerFilter = filterPlayers chartSettings.playerFilter }
              }
            , Cmd.none
            )

        SelectAllPlayers ->
            let
                chartSettings =
                    model.chartSettings

                playerFilter =
                    SelectedPlayers <| Set.fromList <| LeagueHelper.playerIds model.league
            in
            ( { model | chartSettings = { chartSettings | playerFilter = playerFilter } }, Cmd.none )

        UnselectAllPlayers ->
            let
                chartSettings =
                    model.chartSettings
            in
            ( { model | chartSettings = { chartSettings | playerFilter = SelectedPlayers Set.empty } }, Cmd.none )

        RecentGameResultsResponse response ->
            -- if the "Recent Players" filter is currently selected update the model
            let
                playerFilter =
                    case model.chartSettings.playerFilter of
                        RecentPlayers _ ->
                            RecentPlayers <| LeagueHelper.recentPlayerIds (RemoteData.fromResult response)

                        _ ->
                            model.chartSettings.playerFilter

                newModel =
                    model
                        |> getChartSettings
                        |> setPlayerFilter playerFilter
                        |> setChartSettings model
            in
            ErrorHandler.handleGraphQLError
                response
                ( newModel
                    |> getViewModel
                    |> setRecentResults (RemoteData.fromResult response)
                    |> setViewModel newModel
                , Cmd.none
                )


getViewModel : Model -> ChartsViewModel
getViewModel model =
    model.chartsView


getChartSettings : Model -> ChartSettings
getChartSettings model =
    model.chartSettings


setRecentResults : GraphQLData (List GameResult) -> ChartsViewModel -> ChartsViewModel
setRecentResults results viewModel =
    { viewModel | recentResults = results }


setPlayerFilter : PlayerFilter -> ChartSettings -> ChartSettings
setPlayerFilter playerFilter chartSettings =
    { chartSettings | playerFilter = playerFilter }


setViewModel : Model -> ChartsViewModel -> Model
setViewModel model viewModel =
    { model | chartsView = viewModel }


setChartSettings : Model -> ChartSettings -> Model
setChartSettings model chartSettings =
    { model | chartSettings = chartSettings }
