module Update.Main exposing (update)

import Api.Auth0 as Auth0
import Api.ElmGraphQL as ElmGraphQL
import Api.GameStats as GameStatsApi
import Browser
import Browser.Navigation as Nav
import Components.LiveDropdown as LiveDropdown
import Dict
import ErrorHandler exposing (handleGraphQLError)
import Helper.League as LeagueHelper
import Jwt
import Model.BasicTypes exposing (Size)
import Model.LoginData exposing (LoginStatus(..), loginProviderToString)
import Model.Main exposing (Model)
import Model.Route exposing (..)
import Msgs.Main exposing (..)
import Msgs.SettingsViewMsgs exposing (SettingsViewMsg(..))
import Ports.Analytics.Analytics as Analytics
import Ports.Auth0.Auth0 as Auth0Port
import Ports.LocalStorage.LocalStorage as LocalStorage
import RemoteData exposing (RemoteData(..), WebData)
import Routing exposing (reverseRoute)
import Update.BootstrapUpdate as BootstrapUpdate
import Update.ChartsViewUpdate as ChartsViewUpdate
import Update.GameStatsUpdate as GameStatsUpdate
import Update.InvitationViewUpdate as InvitationViewUpdate
import Update.LeagueUpdate as LeagueUpdate
import Update.LoginModalUpdate as LoginModalUpdate
import Url
import Utils exposing (joinMaybe)
import View.EditResult as EditResultView
import View.Games as GamesView
import View.Main as View
import View.NewResult as NewResult
import View.Players as PlayersView
import View.ResultDetails as ResultDetailsView
import View.Results as ResultsView
import View.Settings as SettingsView


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Noop ->
            ( model, Cmd.none )

        WindowResize size ->
            ( { model | windowSize = size }, Cmd.none )

        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        NavigateTo route ->
            ( model, Nav.pushUrl model.key <| Routing.reverseRoute route )

        UrlChange url ->
            let
                ( newModel, cmd ) =
                    View.onEnter
                        { model
                            | url = url
                            , route = Routing.parseLocation url
                        }
            in
            ( newModel
            , Cmd.batch
                [ cmd
                , Analytics.gtag
                    { location = url.path
                    , pageTitle = Routing.parseLocation url |> Routing.pageTitle
                    }
                ]
            )

        Auth0UserProfileResponse response ->
            ErrorHandler.handleRequestError response ( { model | auth0Profile = response }, Cmd.none )

        MyLeaguesResponse response ->
            ErrorHandler.handleGraphQLError response ( { model | myLeagues = RemoteData.fromResult response }, Cmd.none )

        NavbarMsg state ->
            ( { model | navbarState = state }, Cmd.none )

        OnTime now ->
            let
                loginStatus =
                    case model.loginStatus of
                        LoggedIn token ->
                            case Jwt.isExpired now token of
                                Result.Ok expired ->
                                    if expired then
                                        NotLoggedIn

                                    else
                                        model.loginStatus

                                Result.Err _ ->
                                    NotLoggedIn

                        _ ->
                            model.loginStatus

                newModel =
                    { model | loginStatus = loginStatus }
            in
            case loginStatus of
                LoggedIn _ ->
                    ( { model | loginStatus = loginStatus, auth0Profile = Loading }
                    , Cmd.batch
                        [ Auth0.loadUserProfile newModel
                        , ElmGraphQL.authenticateUser newModel
                        ]
                    )

                _ ->
                    ( { model | loginStatus = loginStatus }
                    , Cmd.batch
                        [ Auth0Port.logout ()
                        , ElmGraphQL.loadRecentLeagues model
                        ]
                    )

        OnLoginDataLoaded maybeToken ->
            let
                newModel =
                    case maybeToken of
                        Just token ->
                            { model | loginStatus = LoggedIn token }

                        Nothing ->
                            { model | loginStatus = NotLoggedIn, userId = Nothing, myLeagues = NotAsked, auth0Profile = NotAsked }
            in
            case maybeToken of
                Just _ ->
                    ( { newModel | auth0Profile = Loading }
                    , Cmd.batch
                        [ Auth0.loadUserProfile newModel
                        , ElmGraphQL.authenticateUser newModel
                        ]
                    )

                _ ->
                    ( newModel
                    , Cmd.batch
                        [ ElmGraphQL.loadRecentLeagues newModel
                        ]
                    )

        OnSaveRedirectUrl url ->
            ( model, LocalStorage.saveRedirectUrl url )

        OnRedirectUrlLoaded url ->
            case Maybe.andThen Url.fromString url of
                Just url_ ->
                    update (NavigateTo <| Routing.parseLocation url_) model

                Nothing ->
                    ( model, Cmd.none )

        Logout ->
            ( model, Auth0Port.logout () )

        LeagueMsg leagueMsg ->
            LeagueUpdate.update leagueMsg model

        LeagueResponse response ->
            ErrorHandler.handleGraphQLError response ( { model | league = RemoteData.fromResult response }, Cmd.none )

        ModalMsg state ->
            ( { model | modalState = state }, Cmd.none )

        LiveDropdownMsg state ->
            let
                msg_ =
                    Maybe.withDefault Noop (LiveDropdown.getMsg state)
            in
            update msg_ { model | liveDropdownState = LiveDropdown.update state }

        NewGameResultMsgs msg_ ->
            NewResult.update msg_ model

        ResultsViewMsgs msg_ ->
            ResultsView.update msg_ model

        GamesViewMsgs msg_ ->
            GamesView.update msg_ model

        PlayersViewMsgs msg_ ->
            PlayersView.update msg_ model

        ResultDetailsViewMsgs msg_ ->
            ResultDetailsView.update msg_ model

        EditResultViewMsgs msg_ ->
            EditResultView.update msg_ model

        GameResultsResponse response ->
            let
                -- load the game result stats
                cmd =
                    case model.league of
                        Success t ->
                            response
                                |> Result.withDefault []
                                |> List.map .id
                                |> List.reverse
                                |> List.map (GameStatsApi.getGameResultStats model t.slug)
                                |> Cmd.batch

                        _ ->
                            Cmd.none
            in
            handleGraphQLError response ( { model | gameResults = RemoteData.fromResult response }, cmd )

        UserIdResponse response ->
            let
                userId =
                    response |> Result.toMaybe |> joinMaybe

                newModel =
                    { model | userId = Maybe.map .auth0id userId }
            in
            ( newModel
            , Cmd.batch
                [ ElmGraphQL.loadMyLeagues newModel
                , ElmGraphQL.loadRecentLeagues newModel
                ]
            )
                |> handleGraphQLError response

        CreateLeagueResponse response ->
            case response of
                Result.Ok league ->
                    ( { model
                        | recentLeagues = Loading
                        , myLeagues = Loading
                      }
                    , Cmd.batch
                        [ Nav.pushUrl model.key (Routing.reverseRoute (LeagueRoute league.slug AddNewResult))
                        , ElmGraphQL.loadRecentLeagues model
                        , ElmGraphQL.loadMyLeagues model
                        ]
                    )

                Result.Err _ ->
                    handleGraphQLError response ( model, Cmd.none )

        CreateInvitationResponse response ->
            case RemoteData.fromResult response of
                Success invitation ->
                    SettingsView.update (OnInvitationCreated invitation) model

                _ ->
                    handleGraphQLError response ( model, Cmd.none )

        RecentLeaguesResponse response ->
            ErrorHandler.handleGraphQLError response ( { model | recentLeagues = RemoteData.fromResult response }, Cmd.none )

        RecentResultsResponse response ->
            ErrorHandler.handleGraphQLError response
                ( { model | recentResults = RemoteData.fromResult response }
                , case RemoteData.fromResult response of
                    -- load the stats for the recent results
                    Success results ->
                        Cmd.batch <|
                            List.map (\res -> GameStatsApi.getGameResultStats model res.league.slug res.id) results

                    _ ->
                        Cmd.none
                )

        GameResultMetaResponse response ->
            handleGraphQLError response
                ( { model
                    | gameResultsMeta =
                        RemoteData.fromResult response
                            |> RemoteData.map (\xs -> List.map (\{ id, time } -> ( id, time )) xs |> Dict.fromList)
                  }
                , Cmd.none
                )

        GamesResponse response ->
            let
                games =
                    RemoteData.fromResult response

                newModel =
                    { model
                        | games = games
                        , gameInfos = LeagueHelper.mergeGameInfos games model.gameStats
                    }
            in
            handleGraphQLError response ( newModel, Cmd.none )

        InvitationResponse response ->
            let
                invitation =
                    RemoteData.fromResult response
            in
            handleGraphQLError response ( { model | invitation = invitation }, Cmd.none )

        OnGameResultUpdate _ ->
            let
                newModel =
                    { model
                        | games = Loading
                        , gameResults = Loading
                        , gameResultsMeta = Loading
                        , timeseries = Loading
                    }

                -- reload league and statistics etc...
                cmds =
                    case model.league of
                        Success t ->
                            Cmd.batch
                                [ ElmGraphQL.loadLeague model t.slug
                                , ElmGraphQL.loadGames model t.slug
                                , ElmGraphQL.loadGameResults model t.slug GameResultsResponse
                                , ElmGraphQL.loadGameResultMeta model t.slug
                                , GameStatsApi.getTimeseries model t.slug
                                ]

                        _ ->
                            Cmd.none
            in
            ( newModel, cmds )

        DeleteGameResultResponse response ->
            let
                newModel =
                    { model
                        | timeseries = NotAsked
                        , gameStats = NotAsked
                    }
            in
            View.onEnter newModel |> handleGraphQLError response

        OnGameResultDeleted gameresultId ->
            -- Currently deleting a result is handled the same way as adding a new result.
            -- This will probably change at some time which is why I created an extra message.
            update (OnGameResultUpdate gameresultId) model

        GameStatsMsg msg_ ->
            GameStatsUpdate.update msg_ model

        BootstrapMsgs msg_ ->
            let
                ( bootstrapState, cmd ) =
                    BootstrapUpdate.update msg_ model.bootstrapState
            in
            ( { model | bootstrapState = bootstrapState }, cmd )

        ChartsViewMsgs msg_ ->
            ChartsViewUpdate.update msg_ model

        InvitationViewMsgs msg_ ->
            InvitationViewUpdate.update msg_ model

        SettingsViewMsgs msg_ ->
            SettingsView.update msg_ model

        OnNow date ->
            ( { model | currentTime = date }, Cmd.none )

        OnViewport { scene } ->
            ( { model
                | windowSize = Size (round scene.width) (round scene.height)
              }
            , Cmd.none
            )

        OnInternetDisconnect ->
            ( { model | route = OfflineRoute }, Cmd.none )

        OnInternetConnect ->
            ( model, Nav.pushUrl model.key (Routing.reverseRoute HomeRoute) )

        LoginModalMsgs msg_ ->
            LoginModalUpdate.update msg_ model

        Login provider ->
            ( model, provider |> loginProviderToString |> Auth0Port.authorize )

        CheckSession ->
            case model.loginStatus of
                LoggedIn _ ->
                    ( model, Auth0Port.checkSession () )

                _ ->
                    ( model, Cmd.none )
