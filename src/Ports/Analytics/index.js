// initial tag without pageview
gtag('config', process.env.GTAG_ID);

export function initialize(ports) {
    console.log('Analytics Port');

    ports.gtag.subscribe(({location, pageTitle}) => {
        gtag('config', process.env.GOOGLE_ANALYTICS_ID, {
            page_path: location,
            page_location: location,
            page_title: pageTitle,
        })
    });
}

export default {
    initialize
}