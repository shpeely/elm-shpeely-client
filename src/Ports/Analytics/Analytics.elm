port module Ports.Analytics.Analytics exposing (gtag)


port gtag : { location : String, pageTitle : String } -> Cmd msg
