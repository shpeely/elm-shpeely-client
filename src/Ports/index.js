import localStorage from './LocalStorage'
import network from './Network'
import auth0 from './Auth0'
import analytics from './Analytics'

const modules = [
    localStorage,
    network,
    auth0,
    analytics,
]

export function initialize (ports) {
    console.log('Initializing ports...')
    modules.forEach(p => p.initialize(ports))
    console.log('Finished initialization of ports')
}

export default {
    initialize
}