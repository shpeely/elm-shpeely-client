const REDIRECT_URL_KEY = 'redirectPath'

function deleteRedirectUrl() {
    sessionStorage.removeItem(REDIRECT_URL_KEY)
}

/*
redirect url
 */

function saveRedirectUrl(url) {
    console.log('Saving redirect url', url);
    sessionStorage.setItem(REDIRECT_URL_KEY, url);
}

/**
 * Load *and deletes* the redirect url stored in local storage. Returns null if no redirect url is stored.
 */
export function loadRedirectUrl() {
    const redirectPath = sessionStorage.getItem(REDIRECT_URL_KEY)
    sessionStorage.removeItem(REDIRECT_URL_KEY)
    return redirectPath;
}

/**
 * If a port is never used the port is undefined (due to treeshaking?) This is a simple ehlper function to
 * subscribe to ports that are defined. This allows making ports optional
 * @param port
 * @param fn
 */
function subscribe(port, fn) {
    if (port) {
        port.subscribe(fn)
    }
}

export function initialize (ports) {
    console.log('LocalStorage Port');

    // save
    subscribe(ports.saveRedirectUrl, saveRedirectUrl)

    subscribe(ports.loadRedirectUrl, () => {
        console.log('Loading redirect url...');
        try {
            ports.redirectUrlLoaded.send(loadRedirectUrl());
        } catch (err) {
            console.error('Failed to load redirect url', err)
        }
    })

    // remove
    subscribe(ports.deleteRedirectUrl, deleteRedirectUrl)
}

export default {
    initialize
}

