port module Ports.LocalStorage.LocalStorage exposing (deleteRedirectUrl, loadRedirectUrl, redirectUrlLoaded, saveRedirectUrl)

import Maybe exposing (Maybe)


port saveRedirectUrl : String -> Cmd msg


port deleteRedirectUrl : () -> Cmd msg


port loadRedirectUrl : () -> Cmd msg


port redirectUrlLoaded : (Maybe String -> msg) -> Sub msg
