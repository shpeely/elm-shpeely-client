export function initialize(ports) {
    console.log('Network Port');

    window.addEventListener('online',  () => ports.onConnect.send(null));
    window.addEventListener('offline', () => ports.onDisconnect.send(null));

    ports.isConnected.subscribe(() => {
        console.log('Connected to the internet: ', navigator.onLine);
        if (navigator.onLine) {
            ports.onConnect.send(null)
        } else {
            ports.onDisconnect.send(null)
        }
    });
}

export default {
    initialize
}