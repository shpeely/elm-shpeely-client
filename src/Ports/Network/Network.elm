port module Ports.Network.Network exposing (isConnected, onConnect, onDisconnect)


port onConnect : (() -> msg) -> Sub msg


port onDisconnect : (() -> msg) -> Sub msg


port isConnected : () -> Cmd msg
