port module Ports.Auth0.Auth0 exposing (authorize, checkSession, logout, sessionChecked)


port authorize : String -> Cmd msg


port logout : () -> Cmd msg


port checkSession : () -> Cmd msg


port sessionChecked : (Maybe String -> msg) -> Sub msg
