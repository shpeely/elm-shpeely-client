import createAuth0Client from '@auth0/auth0-spa-js';

function auth0client() {
    return createAuth0Client({
        domain: process.env.AUTH0_DOMAIN,
        client_id: process.env.AUTH0_CLIENT_ID,
        redirect_uri: window.location.origin,
    });
}

function justLoggedIn() {
    return window.location.search.includes('?code=');
}

export function initialize(ports) {
    console.log('auth0 port')

    ports.logout.subscribe(() => {
        auth0client().then(auth0 => auth0.logout({
            returnTo: window.location.origin
        }))
    })

    ports.authorize.subscribe(async (connection) => {
        console.log(`Login with ${connection}`)
        auth0client().then(auth0 => auth0.loginWithRedirect({connection}))
    })

    ports.checkSession.subscribe(() => {
        if (!justLoggedIn()) {
            auth0client().then(auth0 => {
                auth0.isAuthenticated()
                    .then(loggedIn => (loggedIn ? auth0.getTokenSilently() : null))
                    .then(accessToken => ports.sessionChecked.send(accessToken))
            })
        }
    })

    if (justLoggedIn()) {
        auth0client().then(auth0 => {
            auth0.handleRedirectCallback()
                .then(() => {
                    window.history.replaceState({}, document.title, window.location.pathname);

                    return auth0.isAuthenticated()
                })
                .then(loggedIn => (loggedIn ? auth0.getTokenSilently() : null))
                .then(accessToken => ports.sessionChecked.send(accessToken))
                .catch(err => console.error('Auth0 error: ', err));
        })
    }
}

export default {
    initialize
}