module Main exposing (init, initialModel, main, subscriptions)

import Api.Auth0 as Auth0
import Bootstrap.Carousel as Carousel
import Bootstrap.Dropdown as Dropdown exposing (State(..))
import Bootstrap.Modal as Modal
import Bootstrap.Navbar as Navbar
import Browser
import Browser.Dom as Dom
import Browser.Events as Events
import Browser.Navigation as Nav
import Components.LiveDropdown as LiveDropdown
import Debounce
import Dict exposing (Dict)
import Model.BasicTypes exposing (Size)
import Model.Bootstrap as BootstrapState
import Model.ChartsView as ChartsViewModel
import Model.EditResultView as EditResultViewModel
import Model.Flags exposing (Flags)
import Model.GamesView as GamesViewModel
import Model.LoginData as LoginData exposing (LoginStatus(..))
import Model.LoginModal as LoginModal
import Model.Main exposing (..)
import Model.NewResultView as NewResultViewModel
import Model.PlayersView as PlayersViewModel
import Model.ResultDetailsView as ResultDetailsView exposing (ResultDetailsViewModel)
import Model.ResultsView as ResultsViewModel exposing (ResultsViewModel)
import Model.SettingsView as SettingsViewModel
import Model.Stats exposing (initialChartSettings)
import Msgs.BootstrapMsgs exposing (..)
import Msgs.LoginModalMsgs exposing (LoginModalMsg(..))
import Msgs.Main exposing (..)
import Msgs.NewGameResultMsgs exposing (NewGameResultMsg(..))
import Ports.Auth0.Auth0 as Auth0Port
import Ports.LocalStorage.LocalStorage as LocalStorage
import Ports.Network.Network as Network
import RemoteData exposing (..)
import Routing exposing (..)
import Task
import Time
import Tuple
import Update.Main as Update
import Url
import Utils
import View.Main as View


initialModel : Flags -> Url.Url -> Nav.Key -> Model
initialModel flags url key =
    { createLeagueError = Nothing
    , bootstrapState = BootstrapState.initialState
    , windowSize = { width = 0, height = 0 }
    , currentTime = Time.millisToPosix 0
    , url = url
    , key = key
    , route = Routing.parseLocation url
    , loginStatus = Checking
    , flags = flags
    , userId = Nothing
    , auth0Profile = NotAsked
    , recentLeagues = NotAsked
    , recentResults = Loading
    , myLeagues = NotAsked
    , navbarState = Tuple.first <| Navbar.initialState NavbarMsg
    , newLeague = { name = "", slug = Nothing }
    , gameSearch = { value = "", debounce = Debounce.init }
    , liveDropdownState = LiveDropdown.initialState
    , league = NotAsked
    , invitation = NotAsked
    , timeseries = NotAsked
    , chartSettings = initialChartSettings
    , games = NotAsked
    , resultsView = ResultsViewModel.initialState
    , gamesView = GamesViewModel.initialState
    , playersView = PlayersViewModel.initialState
    , settingsView = SettingsViewModel.initialState
    , newResultView = NewResultViewModel.initialState
    , editResultView = EditResultViewModel.initialState
    , chartsView = ChartsViewModel.initialState
    , gameResults = NotAsked
    , gameResultStats = Dict.empty
    , gameResultsMeta = NotAsked
    , gameStats = NotAsked
    , playerStats = NotAsked
    , gameInfos = Nothing
    , resultDetailsView = ResultDetailsView.initialState
    , modalState = Modal.hidden
    , loginModal = LoginModal.initialModel
    , error = ""
    }


subscriptions : Model -> Sub Msg
subscriptions model =
    let
        -- subscription of dynamically created components
        resultDropdowns =
            model.bootstrapState.resultDropdownState
                |> Dict.toList
                |> List.map (\( id, state ) -> Dropdown.subscriptions state (BootstrapMsgs << ToggleResultMoreDropdown id))

        memberDropdowns =
            model.bootstrapState.memberRoleDropdownState
                |> Dict.toList
                |> List.map (\( id, state ) -> Dropdown.subscriptions state (BootstrapMsgs << ToggleMemberRoleDropdown id))

        liveDropdowns =
            (LiveDropdown.subscriptions model.newResultView.resultFormState.liveDropdownsState NewResultLiveDropdownMsg |> List.map (Sub.map NewGameResultMsgs))
                ++ LiveDropdown.subscriptions model.liveDropdownState LiveDropdownMsg
    in
    Sub.batch
        ([ Navbar.subscriptions model.navbarState NavbarMsg
         , LocalStorage.redirectUrlLoaded OnRedirectUrlLoaded
         , Events.onResize (\w h -> Size w h |> WindowResize)
         , Network.onDisconnect (\_ -> OnInternetDisconnect)
         , Network.onConnect (\_ -> OnInternetConnect)

         -- check session every 15mins
         , Time.every (1000 * 60 * 15) (\_ -> CheckSession)
         , Auth0Port.sessionChecked OnLoginDataLoaded

         -- bootstrap boilerplate
         , Dropdown.subscriptions
            model.bootstrapState.playerFilterDropdownState
            (BootstrapMsgs << PlayerFilterDropwdownToggle)
         , Dropdown.subscriptions
            model.bootstrapState.gameOrderDropdownState
            (BootstrapMsgs << GameOrderDropdownToggle)
         , Dropdown.subscriptions
            model.bootstrapState.timeFilterDropdownState
            (BootstrapMsgs << TimeFilterDropwdownToggle)
         , Dropdown.subscriptions
            model.bootstrapState.numPlayersFilterDropdownState
            (BootstrapMsgs << NumPlayersDropdownToggle)
         , Modal.subscriptions model.loginModal.visibility (LoginModalMsgs << AnimateModal)
         , Carousel.subscriptions model.bootstrapState.carouselState (BootstrapMsgs << CarouselMsg)
         ]
            ++ resultDropdowns
            ++ memberDropdowns
            ++ liveDropdowns
        )


init : Flags -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    let
        ( model, cmd ) =
            initialModel flags url key
                |> View.onEnter
    in
    ( model
    , Cmd.batch
        [ cmd

        -- check if logged in
        , Auth0Port.checkSession ()

        -- load redirect url
        , LocalStorage.loadRedirectUrl ()

        -- initial window size
        , Task.perform OnViewport Dom.getViewport

        -- current date
        , Task.perform OnNow Time.now

        -- boilerplate
        , Tuple.second <| Navbar.initialState NavbarMsg
        ]
    )


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , view =
            \model ->
                { title = "Shpeely"
                , body = View.view model
                }
        , update = Update.update
        , subscriptions = subscriptions
        , onUrlChange = UrlChange
        , onUrlRequest = LinkClicked
        }
