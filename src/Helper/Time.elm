module Helper.Time exposing (formatDate)

import DateFormat
import Time exposing (Posix, utc)


formatDate : Posix -> String
formatDate time =
    time
        |> DateFormat.format
            [ DateFormat.monthNameAbbreviated
            , DateFormat.text " "
            , DateFormat.dayOfMonthSuffix
            , DateFormat.text ", "
            , DateFormat.yearNumber
            ]
            utc
