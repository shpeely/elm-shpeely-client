module Helper.Charts exposing (formatTick)


internalRanges : List { divider : Float, suffix : String }
internalRanges =
    [ { divider = 10 ^ 3, suffix = "k" }
    , { divider = 10 ^ 6, suffix = "M" }
    ]


formatNumber : Float -> String
formatNumber float =
    List.filter (.divider >> (>) float) internalRanges
        |> List.head
        |> Maybe.map (\range -> { value = float / range.divider, suffix = range.suffix })
        |> Maybe.map (\x -> String.fromFloat x.value ++ x.suffix)
        |> Maybe.withDefault (String.fromFloat float)


formatTick : Float -> String
formatTick =
    abs >> formatNumber
