module Helper.ChartSettings exposing (acceptPlayer, acceptTime, dataDesc, filterPlayers, filterTime, filterTimeSeries, pastLeagueYears, timeFilterDesc)

import Array
import DateFormat
import Dict exposing (Dict)
import Model.GameResult exposing (GameResult)
import Model.GraphQL exposing (GraphQLData)
import Model.Stats as Stats exposing (ChartSettings, PlayerFilter(..), TimeFilter(..), TimeRange, TimeSeries)
import Set
import Time exposing (Month(..), Posix, Zone, utc)
import Time.Extra as TimeExtra exposing (Interval(..), Parts)


pastLeagueYears : List Posix -> Posix -> List Int
pastLeagueYears gameresultTimes currentTime =
    let
        firstGame =
            gameresultTimes
                |> List.map Time.posixToMillis
                |> List.minimum
                |> Maybe.map Time.millisToPosix
                |> Maybe.withDefault currentTime

        yearList : Posix -> Posix -> List Int
        yearList curr it =
            if Time.posixToMillis curr < Time.posixToMillis (TimeExtra.add Year 1 utc it) then
                []

            else
                yearList curr (TimeExtra.add Year 1 utc it) ++ [ Time.toYear utc it ]
    in
    yearList currentTime firstGame


acceptTime : Posix -> TimeFilter -> Posix -> Bool
acceptTime currentTime timeFilter time =
    case timeFilter of
        AllTime ->
            True

        CurrentYear ->
            Time.posixToMillis (TimeExtra.floor Year utc currentTime) < Time.posixToMillis time

        CurrentMonth ->
            Time.posixToMillis (TimeExtra.floor Month utc currentTime) < Time.posixToMillis time

        Stats.Year_ year ->
            let
                start =
                    TimeExtra.partsToPosix utc
                        (Parts year Jan 0 0 0 0 0)

                end =
                    TimeExtra.partsToPosix utc
                        (Parts (year + 1) Jan 0 0 0 0 0)
            in
            (Time.posixToMillis start < Time.posixToMillis time) && (Time.posixToMillis end > Time.posixToMillis time)

        PastMonths months ->
            TimeExtra.add Month (months * -1) utc currentTime
                |> Time.posixToMillis
                |> (>) (Time.posixToMillis time)

        PastYears years ->
            TimeExtra.add Month (years * -1) utc currentTime
                |> Time.posixToMillis
                |> (>) (Time.posixToMillis time)

        SelectedTimeRange { from, to } ->
            Time.posixToMillis from < Time.posixToMillis currentTime && Time.posixToMillis to > Time.posixToMillis currentTime


acceptPlayer : PlayerFilter -> String -> Bool
acceptPlayer playerFilter player =
    case playerFilter of
        AllPlayers ->
            True

        RecentPlayers players ->
            Set.member player players

        SelectedPlayers players ->
            Set.member player players


filterPlayers : PlayerFilter -> TimeSeries -> TimeSeries
filterPlayers playerFilter timeseries =
    case playerFilter of
        AllPlayers ->
            timeseries

        SelectedPlayers players ->
            let
                scores =
                    timeseries.scores
                        |> Dict.toList
                        |> List.filter (Tuple.first >> (\a -> Set.member a players))
                        |> Dict.fromList
            in
            { timeseries | scores = scores }

        RecentPlayers playerIds ->
            filterPlayers (SelectedPlayers playerIds) timeseries


filterTime : TimeFilter -> Dict String Posix -> Posix -> TimeSeries -> TimeSeries
filterTime timeFilter gameResultMeta currentTime timeseries =
    let
        firstIndex =
            gameResultMeta
                |> Dict.values
                |> List.map Time.posixToMillis
                |> List.sort
                |> List.indexedMap Tuple.pair
                |> List.filter (Tuple.second >> Time.millisToPosix >> acceptTime currentTime timeFilter)
                |> List.head
                |> Maybe.map Tuple.first
                |> Maybe.withDefault 0

        lastIndex =
            gameResultMeta
                |> Dict.values
                |> List.map Time.posixToMillis
                |> List.sort
                |> List.reverse
                |> List.indexedMap (\a b -> ( a, b ))
                |> List.filter (Tuple.second >> Time.millisToPosix >> acceptTime currentTime timeFilter)
                |> List.head
                |> Maybe.map Tuple.first
                |> Maybe.withDefault (Dict.size gameResultMeta)
                |> (-) (Dict.size gameResultMeta)

        firstScores =
            timeseries.scores
                |> Dict.toList
                |> List.map (Tuple.mapSecond (Array.get (firstIndex - 1)))
                |> List.map (Tuple.mapSecond (Maybe.withDefault 0))
                |> Dict.fromList

        subtractScore player scores_ =
            let
                score =
                    Dict.get player firstScores
                        |> Maybe.withDefault 0
            in
            scores_
                |> Array.map (\a -> (-) a score)

        scores =
            timeseries.scores
                |> Dict.map (\_ scoreArray -> Array.slice firstIndex lastIndex scoreArray)
                |> Dict.map subtractScore
    in
    { timeseries | scores = scores }


filterTimeSeries : ChartSettings -> Dict String Posix -> Posix -> TimeSeries -> TimeSeries
filterTimeSeries settings gameResultMeta currentTime timeseries =
    timeseries
        |> filterPlayers settings.playerFilter
        |> filterTime settings.timeFilter gameResultMeta currentTime


dataDesc : TimeSeries -> String
dataDesc timeseries =
    let
        numGames =
            Dict.values timeseries.scores
                |> List.head
                |> Maybe.withDefault Array.empty
                |> Array.length

        numPlayers =
            Dict.keys timeseries.scores
                |> List.length
    in
    String.join " "
        [ String.fromInt numGames
        , if numGames /= 1 then
            "games of"

          else
            "game of"
        , String.fromInt numPlayers
        , if numPlayers /= 1 then
            "players"

          else
            "player"
        ]


timeFilterDesc : TimeFilter -> String
timeFilterDesc timeFilter =
    case timeFilter of
        AllTime ->
            ""

        CurrentYear ->
            " from the start of this year until now."

        CurrentMonth ->
            " from the start of this month until now."

        Stats.Year_ year ->
            " during the year " ++ String.fromInt year

        PastMonths months ->
            " during the past "
                ++ (if months == 1 then
                        "year"

                    else
                        String.fromInt months ++ " years"
                   )

        PastYears years ->
            " during the past "
                ++ (if years == 1 then
                        "year"

                    else
                        String.fromInt years ++ " years"
                   )

        SelectedTimeRange { from, to } ->
            " between " ++ prettyDate from ++ " and " ++ prettyDate to


prettyDate : Posix -> String
prettyDate =
    DateFormat.format
        [ DateFormat.monthNameFull
        , DateFormat.text " "
        , DateFormat.dayOfMonthSuffix
        , DateFormat.text ", "
        , DateFormat.yearNumber
        ]
        utc
