module Helper.Grid exposing (Col, colWidth, defaultCol, fullWidth)

import Model.BasicTypes exposing (Size)


type alias Col =
    { xs : Float
    , sm : Float
    , md : Float
    , lg : Float
    , xl : Float
    }


type alias Breakpoints =
    { xs : Int
    , sm : Int
    , md : Int
    , lg : Int
    , xl : Int
    }


type alias ContainerWidth =
    { sm : Int
    , md : Int
    , lg : Int
    , xl : Int
    }


padding : Int
padding =
    15


cols : Int
cols =
    12


gutters : Breakpoints
gutters =
    { xs = 0
    , sm = 576
    , md = 768
    , lg = 992
    , xl = 1200
    }


maxContainerWidth : ContainerWidth
maxContainerWidth =
    { sm = 540
    , md = 720
    , lg = 960
    , xl = 1140
    }


defaultCol : Col
defaultCol =
    { xs = 12
    , sm = 12
    , md = 12
    , lg = 12
    , xl = 12
    }


colWidth : Col -> Size -> Int
colWidth col size =
    if size.width > gutters.xl then
        calcWidth maxContainerWidth.xl col.xl

    else if size.width > gutters.lg then
        calcWidth maxContainerWidth.lg col.lg

    else if size.width > gutters.md then
        calcWidth maxContainerWidth.md col.md

    else if size.width > gutters.sm then
        calcWidth maxContainerWidth.sm col.sm

    else
        calcWidth (size.width - padding) col.xs


fullWidth : Size -> Int
fullWidth size =
    colWidth defaultCol size


calcWidth : Int -> Float -> Int
calcWidth t v =
    ((toFloat t * v) / toFloat cols) - (2 * toFloat padding) |> round


width : Int -> Int -> Float
width gridSize w =
    toFloat w



-- testgrid
--Grid.row []
--            (List.range 0 10
--                |> List.map
--                    (\_ ->
--                        Grid.col [ Col.xs12, Col.sm6, Col.md6, Col.lg4, Col.xl3 ]
--                            [ div
--                                [ style
--                                    [ ( "width", (pxWidth model) ++ "px" )
--                                    , ( "background-color", "#CCC" )
--                                    , ( "height", "100px" )
--                                    , ( "margin-bottom", "10px" )
--                                    ]
--                                ]
--                                [ text <| pxWidth model ]
--                            ]
--                    )
--            )
