module Helper.League exposing (bggLink, getMemberRole, hasRole, isMember, mergeGameInfos, mergeSingle, playerIdToName, playerIds, players, recentPlayerIds, resetLeague)

import Model.GameResult exposing (GameResult)
import Model.GraphQL exposing (GraphQLData)
import Model.League exposing (Game, GameInfo, League, MemberRole, Player)
import Model.Main exposing (Model)
import Model.Stats exposing (GameStats)
import RemoteData exposing (RemoteData(..), WebData)
import Set exposing (Set)
import Utils exposing (find)


getMemberRole : Maybe String -> League -> Maybe MemberRole
getMemberRole maybeUserId league =
    case maybeUserId of
        Nothing ->
            Nothing

        Just userId ->
            league.members
                |> List.filter (.user >> .auth0id >> (==) userId)
                |> List.head
                |> Maybe.map .role


isMember : League -> Maybe String -> Bool
isMember league maybeUserId =
    Maybe.map2
        (\league_ auth0id ->
            league_.members
                |> List.map (.user >> .auth0id)
                |> List.any (\x -> x == auth0id)
        )
        (Just league)
        maybeUserId
        |> Maybe.withDefault False


hasRole : League -> Maybe String -> MemberRole -> Bool
hasRole league maybeUserId role =
    getMemberRole maybeUserId league
        |> Maybe.map ((==) role)
        |> Maybe.withDefault False


playerIdToName : League -> String -> String
playerIdToName league playerId =
    league.players
        |> find (\player -> player.id == playerId)
        |> Maybe.map .name
        |> Maybe.withDefault ""


players : RemoteData e League -> List Player
players league =
    league
        |> RemoteData.map .players
        |> RemoteData.withDefault []


playerIds : RemoteData e League -> List String
playerIds league =
    players league
        |> List.map .id


mergeSingle : Game -> GameStats -> GameInfo
mergeSingle game stat =
    { id = game.id
    , bggid = game.bggid
    , weight = game.weight
    , name = game.name
    , thumbnail = game.thumbnail
    , playTime = game.playTime
    , maxPlayers = game.maxPlayers
    , count = stat.count
    , highscores = stat.highscores
    , lowscores = stat.lowscores
    }


mergeGameInfos : RemoteData e (List Game) -> WebData (List GameStats) -> Maybe (List GameInfo)
mergeGameInfos games stats =
    let
        data : Maybe ( List Game, List GameStats )
        data =
            Maybe.map2
                (\a b -> ( a, b ))
                (RemoteData.toMaybe games)
                (RemoteData.toMaybe stats)

        mergeData : ( List Game, List GameStats ) -> List GameInfo
        mergeData ( games_, stats_ ) =
            games_
                |> List.filterMap
                    (\g ->
                        case find (\e -> e.bggid == g.bggid) stats_ of
                            Nothing ->
                                Nothing

                            Just s ->
                                Just <| mergeSingle g s
                    )
    in
    data
        |> Maybe.map mergeData


bggLink : Int -> String
bggLink bggid =
    "https://boardgamegeek.com/boardgame/" ++ String.fromInt bggid


recentPlayerIds : GraphQLData (List GameResult) -> Set String
recentPlayerIds recentResults =
    RemoteData.withDefault [] recentResults
        |> List.map .scores
        |> List.concat
        |> List.map (.player >> .id)
        |> Set.fromList


{-| Resets all data of the league that needs to be reset
after a game result has changed or a new result was added
-}
resetLeague : Model -> Model
resetLeague model =
    let
        chartsViewModel =
            model.chartsView

        newChartsViewModel =
            { chartsViewModel
                | recentResults = NotAsked
            }
    in
    { model
        | chartsView = newChartsViewModel
        , timeseries = NotAsked
        , gameStats = NotAsked
        , gameResultsMeta = NotAsked
    }
