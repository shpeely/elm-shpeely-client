module Routing exposing (leagueSubRoute, pageTitle, parseLocation, resultSubRoute, reverseRoute, routeParser, subRouteActive, viewHref, viewHrefWithOptions, viewLinkWithOptions)

import Html exposing (..)
import Html.Attributes exposing (href)
import Model.Route exposing (..)
import Msgs.Main exposing (..)
import Url
import Url.Parser as UrlParser exposing ((</>))


pageTitle : Route -> String
pageTitle route =
    case route of
        HomeRoute ->
            "Home"

        LoginRoute ->
            "Login"

        AboutRoute ->
            "About"

        PrivacyRoute ->
            "Privacy"

        OfflineRoute ->
            "Offline"

        InvitationRoute token ->
            "Invitation"

        LeagueRoute slug subRoute ->
            case subRoute of
                Overview ->
                    "League Overview"

                AddNewResult ->
                    "New Result"

                Charts ->
                    "Charts"

                Results ->
                    "Results"

                Players ->
                    "Players"

                Games ->
                    "Games"

                Settings ->
                    "Settings"

                ResultRoute resultId resultSubRoute_ ->
                    case resultSubRoute_ of
                        ResultDetails ->
                            "Result Details"

                        EditResult ->
                            "Edit Result"

        NotFoundRoute ->
            "Not Found"


viewHref : Route -> List (Html.Attribute Msg)
viewHref route =
    [ href (reverseRoute route) ]


subRouteActive : Route -> LeagueSubRoute -> Bool
subRouteActive currentRoute subRoute =
    case currentRoute of
        LeagueRoute slug sub ->
            subRoute == sub

        _ ->
            False


viewHrefWithOptions : Route -> List (Attribute Msg) -> List (Html.Attribute Msg)
viewHrefWithOptions route options =
    [ href (reverseRoute route)
    ]
        ++ options


viewLinkWithOptions : Route -> String -> List (Attribute Msg) -> Html Msg
viewLinkWithOptions route linkText options =
    a ([ href <| reverseRoute route ] ++ options) [ text linkText ]


reverseRoute : Route -> String
reverseRoute route =
    case route of
        HomeRoute ->
            "/"

        LoginRoute ->
            "/login"

        AboutRoute ->
            "/about"

        PrivacyRoute ->
            "/privacy"

        OfflineRoute ->
            "/offline"

        InvitationRoute token ->
            "/invitation/" ++ token

        LeagueRoute slug subRoute ->
            case subRoute of
                Overview ->
                    "/league/" ++ slug

                AddNewResult ->
                    "/league/" ++ slug ++ "/add-result"

                Charts ->
                    "/league/" ++ slug ++ "/charts"

                Results ->
                    "/league/" ++ slug ++ "/results"

                Players ->
                    "/league/" ++ slug ++ "/players"

                Games ->
                    "/league/" ++ slug ++ "/games"

                Settings ->
                    "/league/" ++ slug ++ "/settings"

                ResultRoute resultId resultSubRoute_ ->
                    case resultSubRoute_ of
                        ResultDetails ->
                            "/league/" ++ slug ++ "/results/" ++ resultId

                        EditResult ->
                            "/league/" ++ slug ++ "/results/" ++ resultId ++ "/edit"

        NotFoundRoute ->
            ""


routeParser : UrlParser.Parser (Route -> a) a
routeParser =
    UrlParser.oneOf
        [ UrlParser.map HomeRoute UrlParser.top
        , UrlParser.map LoginRoute (UrlParser.s "login")
        , UrlParser.map AboutRoute (UrlParser.s "about")
        , UrlParser.map PrivacyRoute (UrlParser.s "privacy")
        , UrlParser.map InvitationRoute (UrlParser.s "invitation" </> UrlParser.string)
        , UrlParser.map OfflineRoute (UrlParser.s "offline")
        , UrlParser.oneOf
            [ UrlParser.map
                (\a -> LeagueRoute a Overview)
                (UrlParser.s "league" </> UrlParser.string)
            , leagueSubRoute AddNewResult "add-result"
            , leagueSubRoute Charts "charts"
            , leagueSubRoute Results "results"
            , leagueSubRoute Players "players"
            , leagueSubRoute Games "games"
            , leagueSubRoute Settings "settings"
            , UrlParser.oneOf
                [ UrlParser.map (\t r -> LeagueRoute t <| ResultRoute r ResultDetails) (UrlParser.s "league" </> UrlParser.string </> UrlParser.s "results" </> UrlParser.string)
                , resultSubRoute EditResult "edit"
                ]
            ]
        ]


leagueSubRoute : LeagueSubRoute -> String -> UrlParser.Parser (Route -> a) a
leagueSubRoute subRoute url =
    UrlParser.map
        (\a -> LeagueRoute a subRoute)
        (UrlParser.s "league" </> UrlParser.string </> UrlParser.s url)


resultSubRoute : ResultSubRoute -> String -> UrlParser.Parser (Route -> a) a
resultSubRoute subRoute url =
    UrlParser.map
        (\t r -> LeagueRoute t (ResultRoute r subRoute))
        (UrlParser.s "league" </> UrlParser.string </> UrlParser.s "results" </> UrlParser.string </> UrlParser.s url)


parseLocation : Url.Url -> Route
parseLocation url =
    url
        |> UrlParser.parse routeParser
        |> Maybe.withDefault NotFoundRoute
