module Utils exposing (count, countToString, find, getAt, ifNothingCmd, joinMaybe, maybeCmd, maybeLoad, maybeRunCmd, toTuple, tuple)

import Dict exposing (Dict)
import RemoteData exposing (RemoteData(..), WebData)


{-| Returns `Just` the element at the given index in the list,
or `Nothing` if the index is out of range.
-}
getAt : Int -> List a -> Maybe a
getAt idx xs =
    if idx < 0 then
        Nothing

    else
        List.head <| List.drop idx xs


toTuple : List a -> Maybe ( a, a )
toTuple xs =
    let
        first =
            getAt 0 xs

        second =
            getAt 1 xs
    in
    case first of
        Nothing ->
            Nothing

        Just x ->
            case second of
                Nothing ->
                    Nothing

                Just y ->
                    Just ( x, y )


maybeRunCmd : (a -> Cmd msg) -> Maybe a -> Cmd msg
maybeRunCmd f maybe =
    maybe
        |> Maybe.map f
        |> Maybe.withDefault Cmd.none


maybeCmd : Cmd msg -> Maybe a -> Cmd msg
maybeCmd cmd maybe =
    case maybe of
        Nothing ->
            Cmd.none

        Just x ->
            cmd


ifNothingCmd : Cmd msg -> Maybe a -> Cmd msg
ifNothingCmd cmd maybe =
    case maybe of
        Nothing ->
            cmd

        Just x ->
            Cmd.none


maybeLoad : RemoteData e a -> Cmd msg -> Cmd msg
maybeLoad webData cmd =
    case webData of
        RemoteData.Success _ ->
            Cmd.none

        RemoteData.Loading ->
            Cmd.none

        _ ->
            cmd


find : (a -> Bool) -> List a -> Maybe a
find condition xs =
    case xs of
        [] ->
            Nothing

        x :: rest ->
            if condition x then
                Just x

            else
                find condition rest


count : List comparable -> List ( comparable, Int )
count list =
    let
        collector : comparable -> Dict comparable Int -> Dict comparable Int
        collector item acc =
            Dict.update item
                (\x ->
                    case x of
                        Just count_ ->
                            Just (count_ + 1)

                        Nothing ->
                            Just 1
                )
                acc
    in
    List.foldl collector Dict.empty list
        |> Dict.toList
        |> List.sortBy (Tuple.second >> (*) -1)


countToString : ( String, Int ) -> String
countToString ( item, count_ ) =
    if count_ > 1 then
        item ++ " (" ++ String.fromInt count_ ++ ")"

    else
        item


joinMaybe : Maybe (Maybe a) -> Maybe a
joinMaybe mx =
    case mx of
        Just x ->
            x

        Nothing ->
            Nothing


tuple : a -> b -> ( a, b )
tuple a b =
    ( a, b )
