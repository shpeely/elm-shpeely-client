module Components.LoginButtons exposing (loginButtons, socialLoginButton)

import Api.Auth0 as Auth0 exposing (..)
import Bootstrap.Button as Button
import Html exposing (Html, div, text)
import Html.Attributes exposing (class, href)
import Model.Flags exposing (Flags)
import Model.LoginData exposing (LoginProvider(..))
import Msgs.Main exposing (Msg(..))


socialLoginButton : Flags -> LoginProvider -> Html Msg
socialLoginButton flags provider =
    let
        properties =
            case provider of
                Facebook ->
                    { class = "btn-facebook", name = "Login with Facebook" }

                GooglePlus ->
                    { class = "btn-google-plus", name = "Login with Google" }

                Twitter ->
                    { class = "btn-twitter", name = "Login with Twitter" }

                Auth0 ->
                    { class = "btn-primary btn-primary-outline", name = "Login / Register" }
    in
    Button.button
        [ Button.primary
        , Button.block
        , Button.onClick (Login provider)
        , Button.attrs [ class properties.class ]
        ]
        [ text properties.name ]


loginButtons : Flags -> Html Msg
loginButtons flags =
    div []
        [ socialLoginButton flags Auth0
        , socialLoginButton flags Facebook
        , socialLoginButton flags GooglePlus
        , socialLoginButton flags Twitter
        ]
