module Components.GameResultChart exposing (render)

import Axis
import Components.Spinner exposing (spinner)
import FormatNumber as FormatNumber
import FormatNumber.Locales exposing (usLocale)
import Helper.Grid as GridHelper
import Helper.League as LeagueHelper
import Html exposing (Html)
import Model.BasicTypes exposing (Size)
import Model.League exposing (League)
import Model.Stats exposing (..)
import Msgs.Main exposing (..)
import Scale exposing (BandConfig, BandScale, ContinuousScale, defaultBandConfig)
import TypedSvg exposing (..)
import TypedSvg.Attributes exposing (alignmentBaseline, class, textAnchor, transform)
import TypedSvg.Attributes.InPx exposing (..)
import TypedSvg.Core exposing (Svg, text)
import TypedSvg.Types exposing (AlignmentBaseline(..), AnchorAlignment(..), Fill(..), Transform(..))
import Utils exposing (count, countToString)


w : GridHelper.Col -> Size -> Float
w col screenSize =
    GridHelper.colWidth col screenSize |> toFloat


h : Float
h =
    240


paddingBottom : Float
paddingBottom =
    40


paddingLeft : Float
paddingLeft =
    4


paddingRight : Float
paddingRight =
    9


type BarType
    = HighScoreBar ExtremeScore
    | LowScoreBar ExtremeScore
    | AverageBar Float
    | AverageThisGameBar Float
    | ScoreBar ScoreStat


toBars : GameResultStats -> List BarType
toBars result =
    let
        scores =
            List.sortBy .score result.scores
                |> List.map ScoreBar

        highscores =
            HighScoreBar result.highscore

        lowscores =
            LowScoreBar result.lowscore

        average =
            AverageBar result.average

        averageThisGame =
            AverageThisGameBar result.averageThisGame

        scoresWithAverage =
            (average :: averageThisGame :: scores)
                |> List.sortBy
                    (\s ->
                        case s of
                            ScoreBar stat ->
                                toFloat stat.score

                            AverageBar a ->
                                a

                            AverageThisGameBar a ->
                                a

                            _ ->
                                -- TODO
                                -1
                    )
                |> List.reverse
    in
    (highscores :: scoresWithAverage) ++ [ lowscores ]


yScale : GameResultStats -> League -> BandScale String
yScale gameresultStats league =
    let
        cols =
            toBars gameresultStats
                |> List.map (barLabel league)
    in
    Scale.band { defaultBandConfig | paddingInner = 0.3, paddingOuter = 0.3 } ( 0, h - paddingBottom ) cols


xScale : Float -> GameResultStats -> ContinuousScale Float
xScale chartWidth gameresultStats =
    let
        maxScore =
            gameresultStats
                |> toBars
                |> List.map barValue
                |> List.maximum
                |> Maybe.withDefault 0
    in
    Scale.linear ( 0, chartWidth - (paddingLeft + paddingRight) ) ( 0, maxScore )


yAxis : GameResultStats -> League -> Svg msg
yAxis result league =
    Axis.left
        [ Axis.tickFormat (\x -> x) ]
        (Scale.toRenderable (\x -> x) (yScale result league))


xAxis : ContinuousScale Float -> Svg msg
xAxis xScale_ =
    Axis.bottom [ Axis.tickCount 7 ] xScale_


barValue : BarType -> Float
barValue bar =
    case bar of
        LowScoreBar lowscores ->
            lowscores.score |> toFloat

        HighScoreBar highscores ->
            highscores.score |> toFloat

        ScoreBar score ->
            toFloat score.score

        AverageBar a ->
            a

        AverageThisGameBar a ->
            a


renderPlayerList : League -> List String -> String
renderPlayerList league playerIds =
    playerIds
        |> List.map (LeagueHelper.playerIdToName league)
        |> count
        |> List.map countToString
        |> String.join ", "


barLabel : League -> BarType -> String
barLabel league bar =
    case bar of
        LowScoreBar lowscores ->
            lowscores
                |> .players
                |> renderPlayerList league
                |> (++) "Lowscore: "

        HighScoreBar highscores ->
            highscores
                |> .players
                |> renderPlayerList league
                |> (++) "Highscore: "

        ScoreBar score ->
            score.player
                |> LeagueHelper.playerIdToName league

        AverageBar a ->
            "Average of all games"

        AverageThisGameBar a ->
            "Average of this game"


barClass : BarType -> String
barClass bar =
    case bar of
        LowScoreBar _ ->
            "lowscore"

        HighScoreBar _ ->
            "highscore"

        ScoreBar score ->
            "score"

        AverageBar _ ->
            "average"

        AverageThisGameBar _ ->
            "average-this-game"


column : ContinuousScale Float -> BandScale String -> Float -> League -> BarType -> Svg msg
column xScale_ yScale_ maxWidth league bar =
    let
        x_ =
            Scale.convert xScale_ (barValue bar)

        barWidth =
            abs <| Scale.convert xScale_ (toFloat 0) - Scale.convert xScale_ (barValue bar)

        label =
            barLabel league bar

        value =
            formatFloat <| barValue bar

        points =
            case bar of
                ScoreBar score ->
                    (if score.points > 0 then
                        "+"

                     else
                        ""
                    )
                        |> (\signum -> "(" ++ signum ++ String.fromInt score.points ++ ")     ")

                _ ->
                    ""

        formatFloat : Float -> String
        formatFloat =
            FormatNumber.format { usLocale | decimals = 1 }
    in
    g [ class <| [ barClass bar ] ]
        [ rect
            [ y <| Scale.convert yScale_ label
            , x <| 0
            , height <| Scale.bandwidth yScale_
            , width <| barWidth
            ]
            []
        , text_
            [ transform [ Translate 10 (Scale.convert (Scale.toRenderable (\x -> x) yScale_) label |> (+) 1) ]
            , textAnchor AnchorStart
            , alignmentBaseline AlignmentCentral
            ]
            [ text label ]
        , text_
            [ transform [ Translate (maxWidth - paddingLeft - 5) (Scale.convert (Scale.toRenderable (\x -> x) yScale_) label) ]
            , transform [ Translate (maxWidth - paddingLeft - 5) (Scale.convert (Scale.toRenderable (\x -> x) yScale_) label) ]
            , class [ "points" ]
            , textAnchor AnchorEnd
            , alignmentBaseline AlignmentCentral
            ]
            [ text <| (points ++ value) ]
        ]


view :
    { league : League
    , gameResultStats : GameResultStats
    , windowSize : Size
    , colWidth : GridHelper.Col
    }
    -> Svg msg
view { gameResultStats, windowSize, colWidth, league } =
    let
        chartWidth =
            w colWidth windowSize

        scaleY =
            yScale gameResultStats league

        scaleX =
            xScale chartWidth gameResultStats

        cols =
            toBars gameResultStats
    in
    svg [ width chartWidth, height h ]
        [ g [ transform [ Translate paddingLeft (h - paddingBottom) ] ]
            [ xAxis scaleX ]
        , g
            [ transform [ Translate paddingLeft 0 ]
            , class [ "series" ]
            ]
          <|
            List.map (column scaleX scaleY chartWidth league) cols
        ]


render :
    Maybe
        { league : League
        , gameResultStats : GameResultStats
        , windowSize : Size
        , colWidth : GridHelper.Col
        }
    -> Html Msg
render model =
    case model of
        Just chartInput ->
            Html.div
                [ class [ "chart gameresult-chart fade-in" ]
                ]
                [ view chartInput ]

        Nothing ->
            Html.div
                [ class [ "gameresult-chart" ]
                ]
                [ spinner ]
