module Components.Title exposing (pageSubTitle, pageTitle, subSubTitle)

import Bootstrap.Grid as Grid
import Html exposing (Html, h1, h2, h3, text)
import Html.Attributes exposing (class)


pageTitle : String -> Html msg
pageTitle title =
    Grid.row []
        [ Grid.col []
            [ h1 [] [ text title ] ]
        ]


pageSubTitle : String -> Html msg
pageSubTitle title =
    Grid.row []
        [ Grid.col []
            [ h2 [] [ text title ] ]
        ]


subSubTitle : String -> Html msg
subSubTitle title =
    Grid.row []
        [ Grid.col []
            [ h3 [] [ text title ] ]
        ]
