module Components.ScoresChart exposing (render)

import Array
import Axis
import Color exposing (Color)
import Components.Spinner exposing (spinner)
import Dict exposing (Dict)
import Helper.ChartSettings as ChartSettingsHelper
import Helper.Charts as ChartsHelper
import Helper.Grid as GridHelper
import Helper.League exposing (playerIdToName)
import Html exposing (Html)
import List
import Model.BasicTypes exposing (Size)
import Model.League exposing (League)
import Model.Stats exposing (ChartSettings, TimeSeries)
import Msgs.Main exposing (..)
import Scale exposing (BandScale, ContinuousScale, OrdinalScale, defaultBandConfig)
import Time exposing (Posix)
import TypedSvg exposing (..)
import TypedSvg.Attributes exposing (alignmentBaseline, class, fill, fontFamily, stroke, textAnchor, transform)
import TypedSvg.Attributes.InPx exposing (..)
import TypedSvg.Core exposing (Svg, text)
import TypedSvg.Types exposing (AlignmentBaseline(..), AnchorAlignment(..), Fill(..), Transform(..))


w : Size -> Float
w screenSize =
    GridHelper.fullWidth screenSize |> toFloat


h : Float
h =
    450


font : String
font =
    "Questrial"


lineColor : Color
lineColor =
    Color.rgb255 41 43 44


textColorFill : Fill
textColorFill =
    Fill lineColor


padding : Float
padding =
    40


column : BandScale String -> ContinuousScale Float -> ( String, Int ) -> Svg msg
column xScale yScale ( player, value ) =
    let
        barHeight =
            abs <| Scale.convert yScale (toFloat 0) - Scale.convert yScale (toFloat value)

        startY =
            Scale.convert yScale (toFloat 0)
                - (if value > 0 then
                    barHeight

                   else
                    0
                  )

        columnMiddle : Float
        columnMiddle =
            Scale.convert yScale (toFloat 0)

        columnClass : String
        columnClass =
            String.join " "
                [ "column"
                , if value > 0 then
                    "column-positive"

                  else
                    "column-negative"
                ]

        colWidth : Float
        colWidth =
            Scale.bandwidth xScale

        playerNameFontSize : Float
        playerNameFontSize =
            colWidth
                |> Basics.min 18
    in
    g [ class [ columnClass ] ]
        [ rect
            [ x <| Scale.convert xScale player
            , y startY
            , width <| Scale.bandwidth xScale
            , height barHeight
            ]
            []
        , text_
            [ transform
                [ Translate (Scale.convert (Scale.toRenderable (\_ -> player) xScale) player) columnMiddle
                , Rotate 270 0 0
                ]
            , textAnchor AnchorStart
            , alignmentBaseline AlignmentCentral
            , fill textColorFill
            , fontSize playerNameFontSize
            ]
            [ text (player ++ " (" ++ String.fromInt value ++ ")") ]
        ]


view :
    { timeseries : TimeSeries
    , windowSize : Size
    , league : League
    , gameResultMeta : Dict String Posix
    , currentTime : Posix
    , settings : ChartSettings
    }
    -> Svg msg
view { timeseries, windowSize, league, gameResultMeta, currentTime, settings } =
    let
        playerLabel : ( String, Int ) -> ( String, Int )
        playerLabel ( playerId, score ) =
            let
                playerName =
                    playerIdToName league playerId
            in
            ( playerName, score )

        filteredTimeSeries =
            ChartSettingsHelper.filterTimeSeries settings gameResultMeta currentTime timeseries

        model : List ( String, Int )
        model =
            filteredTimeSeries
                |> .scores
                |> Dict.toList
                |> List.map (Tuple.mapSecond (Array.toList >> List.reverse >> List.head >> Maybe.withDefault 0))
                |> List.map playerLabel
                |> List.sortBy Tuple.second
                |> List.reverse

        chartWidth : Float
        chartWidth =
            GridHelper.fullWidth windowSize |> toFloat

        xScale : List ( String, Int ) -> BandScale String
        xScale model_ =
            Scale.band
                { defaultBandConfig | paddingInner = 0.1, paddingOuter = 0.2 }
                ( 0, chartWidth - 2 * padding )
                (List.map Tuple.first model_)

        yScale : List ( String, Int ) -> ContinuousScale Float
        yScale model_ =
            let
                ( minValue, maxValue ) =
                    model_
                        |> List.map Tuple.second
                        |> (\xs -> Maybe.map2 Tuple.pair (List.minimum xs) (List.maximum xs))
                        |> Maybe.withDefault ( 0, 0 )
                        |> Tuple.mapFirst (toFloat >> Basics.min 0)
                        |> Tuple.mapSecond toFloat
                        -- if all scores are negative add a 10% margin to the axis to prevent the names
                        -- from getting cut off
                        |> (\( min, max ) -> ( min, Basics.max max (abs min / 3) ))
            in
            Scale.linear ( h - 2 * padding, 0 ) ( minValue, maxValue )

        yAxis : ContinuousScale Float -> Svg msg
        yAxis yScale_ =
            Axis.left [ Axis.tickFormat ChartsHelper.formatTick, Axis.tickCount 10 ] yScale_

        title : String
        title =
            String.join " "
                [ ChartSettingsHelper.dataDesc filteredTimeSeries
                , ChartSettingsHelper.timeFilterDesc settings.timeFilter
                ]

        scaleY =
            yScale model

        scaleX =
            xScale model

        zeroLineWidth =
            1.0

        zeroLineX =
            Scale.convert scaleY 0.0 + padding + (zeroLineWidth / 2)

        chart : List ( String, Int ) -> Svg msg
        chart model_ =
            svg
                [ class [ "chart" ]
                , width chartWidth
                , height h
                ]
                [ text_
                    [ fontFamily [ font ]
                    , fill textColorFill
                    , fontSize 18
                    , alignmentBaseline AlignmentHanging
                    , textAnchor AnchorMiddle
                    , class [ "hidden-sm-down" ]
                    , transform [ Translate (w windowSize / 2) 10 ]
                    ]
                    [ text title ]
                , g
                    [ class [ "y-axis" ]
                    , transform [ Translate (padding - 1) padding ]
                    ]
                    [ yAxis scaleY ]
                , line
                    [ strokeWidth zeroLineWidth
                    , stroke lineColor
                    , y1 zeroLineX
                    , y2 zeroLineX
                    , x1 padding
                    , x2 (chartWidth - padding)
                    ]
                    []
                , g
                    [ transform [ Translate padding padding ]
                    , class [ "series" ]
                    ]
                  <|
                    List.map (column scaleX scaleY) model_
                ]
    in
    chart model


render :
    Maybe
        { league : League
        , timeseries : TimeSeries
        , gameResultMeta : Dict String Posix
        , currentTime : Posix
        , windowSize : Size
        , settings : ChartSettings
        }
    -> Html Msg
render model =
    case model of
        Just chartInput ->
            Html.div [ class [ "scores-chart fade-in" ] ]
                [ view chartInput ]

        Nothing ->
            Html.div
                [ class [ "scores-chart" ]
                ]
                [ spinner ]
