module Components.ResultForm exposing (ResultFormState, Score, bggid, config, gameInfo, gameSearchResults, initialState, isLoading, maxPlayers, onGameSearch, onGameSelected, onLiveDropdownStateUpdate, onPlayerInput, onPlayerSelected, onScoreInput, onSubmit, playerInputs, playerSearchResults, render, scores, submitted, update)

import Bootstrap.Button as Button
import Bootstrap.Form as Form
import Bootstrap.Form.Input as Input
import Bootstrap.Grid.Col as Col
import Bootstrap.Grid.Row as Row
import Components.LiveDropdown as LiveDropdown
import Components.Spinner exposing (spinner)
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes as Attrs exposing (class, type_)
import Html.Events as Events
import Model.Bgg exposing (GameInfo, GameSearchResult)
import Model.GameResult exposing (ScoreInput, UnsavedGameResult)
import Model.GraphQL exposing (GraphQLData)
import Model.League exposing (Player)
import RemoteData exposing (RemoteData(..), WebData)


type alias ResultFormState msg =
    { gameInfo : GraphQLData GameInfo
    , playerSearchResults : Dict Int PlayerEntry
    , gameSearchResults : List GameSearchResult
    , validation : Maybe (Result (List GameResultValidationError) UnsavedGameResult)
    , scores : Dict Int { playerName : Maybe String, score : Maybe Float }
    , liveDropdownsState : LiveDropdown.State msg
    }


initialState : ResultFormState msg
initialState =
    { gameInfo = NotAsked
    , playerSearchResults = Dict.empty
    , gameSearchResults = []
    , validation = Nothing
    , scores = Dict.empty
    , liveDropdownsState = LiveDropdown.initialState
    }


type alias Score =
    { player : Maybe PlayerEntry
    , score : Maybe Float
    }


type PlayerEntry
    = NewPlayer String
    | ExistingPlayer Player


type alias Config msg =
    { id : String
    , submitted : Bool
    , bggid : Maybe Int
    , submitMsg : msg
    , onGameSearchInput : String -> msg
    , onPlayerInput : Int -> String -> msg
    , onGameSelected : Int -> msg
    , searchResults : List GameSearchResult
    , playerSearchResults : Dict Int (List Player)
    , onPlayerSelected : Int -> String -> msg
    , onScoreInput : Int -> String -> msg
    , playerInputs : Dict Int String
    , maxPlayers : Int
    , scores : Dict Int { playerName : Maybe String, score : Maybe Float }
    , gameInfo : GraphQLData GameInfo
    , onLiveDropdownUpdate : LiveDropdown.State msg -> msg
    , isLoading : Bool
    , noop : msg
    }


initialConfig : String -> msg -> Config msg
initialConfig id_ noopMsg =
    { id = id_
    , submitted = False
    , bggid = Nothing
    , submitMsg = noopMsg
    , onGameSearchInput = \_ -> noopMsg
    , onGameSelected = \_ -> noopMsg
    , onPlayerInput = \_ _ -> noopMsg
    , searchResults = []
    , playerSearchResults = Dict.empty
    , onPlayerSelected = \_ _ -> noopMsg
    , onScoreInput = \_ _ -> noopMsg
    , playerInputs = Dict.empty
    , maxPlayers = 2
    , scores = Dict.empty
    , gameInfo = NotAsked
    , onLiveDropdownUpdate = \_ -> noopMsg
    , isLoading = False
    , noop = noopMsg
    }


config : String -> msg -> Config msg
config id_ noopMsg =
    initialConfig id_ noopMsg


onSubmit : msg -> Config msg -> Config msg
onSubmit msg config_ =
    { config_ | submitMsg = msg }


onGameSearch : (String -> msg) -> Config msg -> Config msg
onGameSearch msg config_ =
    { config_ | onGameSearchInput = msg }


onGameSelected : (Int -> msg) -> Config msg -> Config msg
onGameSelected msg config_ =
    { config_ | onGameSelected = msg }


onPlayerInput : (Int -> String -> msg) -> Config msg -> Config msg
onPlayerInput msg config_ =
    { config_ | onPlayerInput = msg }


playerInputs : Dict Int String -> Config msg -> Config msg
playerInputs playerInputs_ config_ =
    { config_ | playerInputs = playerInputs_ }


onScoreInput : (Int -> String -> msg) -> Config msg -> Config msg
onScoreInput msg config_ =
    { config_ | onScoreInput = msg }


submitted : Bool -> Config msg -> Config msg
submitted submitted_ config_ =
    { config_ | submitted = submitted_ }


scores : Dict Int { playerName : Maybe String, score : Maybe Float } -> Config msg -> Config msg
scores scores_ config_ =
    { config_ | scores = scores_ }


bggid : Maybe Int -> Config msg -> Config msg
bggid bggid_ config_ =
    { config_ | bggid = bggid_ }


gameInfo : GraphQLData GameInfo -> Config msg -> Config msg
gameInfo info config_ =
    { config_ | gameInfo = info }


onPlayerSelected : (Int -> String -> msg) -> Config msg -> Config msg
onPlayerSelected msg config_ =
    { config_ | onPlayerSelected = msg }


gameSearchResults : List GameSearchResult -> Config msg -> Config msg
gameSearchResults results config_ =
    { config_ | searchResults = results }


maxPlayers : Int -> Config msg -> Config msg
maxPlayers maxPlayers_ config_ =
    { config_ | maxPlayers = maxPlayers_ }


playerSearchResults : Dict Int (List Player) -> Config msg -> Config msg
playerSearchResults results config_ =
    { config_ | playerSearchResults = results }


onLiveDropdownStateUpdate : (LiveDropdown.State msg -> msg) -> Config msg -> Config msg
onLiveDropdownStateUpdate msg config_ =
    { config_ | onLiveDropdownUpdate = msg }


isLoading : Bool -> Config msg -> Config msg
isLoading isLoading_ config_ =
    { config_ | isLoading = isLoading_ }


update : ResultFormState msg -> LiveDropdown.State msg -> ResultFormState msg
update state dropdownState =
    { state | liveDropdownsState = dropdownState }


render : ResultFormState msg -> Config msg -> Html msg
render resultFormState config_ =
    Form.form [ Events.onSubmit config_.submitMsg ]
        [ Form.group []
            [ Form.label [] [ text "Game Search" ]
            , LiveDropdown.config config_.id config_.onLiveDropdownUpdate config_.noop
                |> LiveDropdown.inputOptions
                    [ Input.placeholder "Enter name of a board game..."
                    , Input.attrs [ Events.onInput config_.onGameSearchInput ]
                    ]
                |> LiveDropdown.initialValue (config_.gameInfo |> RemoteData.map .name |> RemoteData.toMaybe)
                |> LiveDropdown.items (searchResultsToItems config_)
                |> LiveDropdown.view resultFormState.liveDropdownsState
            , getGameErrors config_ |> renderInputErrors
            , case config_.bggid of
                Nothing ->
                    Form.help [] [ text "Start by entering the game's name here" ]

                _ ->
                    text ""
            ]
        , scoreEntries resultFormState config_ config_.onLiveDropdownUpdate
        , getFormFeedbackTexts config_
        , case config_.bggid of
            Just _ ->
                if config_.isLoading then
                    spinner

                else
                    Button.button
                        [ Button.primary
                        , Button.block
                        , Button.attrs [ type_ "submit" ]
                        , Button.disabled config_.isLoading
                        ]
                        [ text "Save Game Result" ]

            Nothing ->
                text ""
        ]


searchResultsToItems :
    Config msg
    ->
        List
            { attrs : List (Html.Attribute msg)
            , html : List (Html.Html msg)
            , value : ( String, msg )
            }
searchResultsToItems config_ =
    case config_.searchResults of
        [] ->
            LiveDropdown.noItems

        _ ->
            let
                mapHtml r =
                    [ text r.name
                    , small [ Attrs.style "margin-left" ".5em" ]
                        [ text ("(" ++ String.fromInt r.year ++ ")") ]
                    ]
            in
            List.map
                (\r ->
                    { attrs = []
                    , html = mapHtml r
                    , value = ( r.name, config_.onGameSelected r.bggid )
                    }
                )
                config_.searchResults


scoreEntries : ResultFormState msg -> Config msg -> (LiveDropdown.State msg -> msg) -> Html msg
scoreEntries resultFormState config_ msg =
    List.range 1 config_.maxPlayers
        |> List.map (scoreEntry resultFormState config_ msg)
        |> div []


scoreEntry : ResultFormState msg -> Config msg -> (LiveDropdown.State msg -> msg) -> Int -> Html msg
scoreEntry resultFormState config_ msg index =
    Form.row [ hasDangerIf <| scoreEntryHasError config_ index ]
        [ Form.col [ Col.md8 ]
            [ Form.label
                (if scoreEntryHasError config_ index then
                    [ class "text-danger" ]

                 else
                    []
                )
                [ text ("Player Name " ++ String.fromInt index) ]
            , LiveDropdown.config (config_.id ++ "-player-" ++ String.fromInt index) msg config_.noop
                |> LiveDropdown.initialValue (Dict.get index config_.scores |> Maybe.map .playerName |> Maybe.withDefault Nothing)
                |> LiveDropdown.inputOptions
                    [ Input.placeholder "Enter Player Name"
                    , Input.attrs
                        [ Events.onInput (config_.onPlayerInput index)
                        ]
                    ]
                |> LiveDropdown.items (playersToItems config_ index)
                |> LiveDropdown.view resultFormState.liveDropdownsState
            ]
        , Form.col [ Col.md4 ]
            [ Form.label [] [ text "Score" ]
            , Input.number
                ([ Input.placeholder "Score ..."
                 , Input.onInput <| config_.onScoreInput index
                 , Input.value (Dict.get index config_.scores |> Maybe.map .score |> Maybe.withDefault Nothing |> Maybe.map String.fromFloat |> Maybe.withDefault "")
                 ]
                    ++ (if List.length (getEntryErrors config_ index) > 0 then
                            [ Input.danger ]

                        else
                            []
                       )
                )
            ]
        , Form.col [ Col.xs12 ]
            [ getEntryErrors config_ index |> renderInputErrors ]
        ]


renderInputErrors : List GameResultValidationError -> Html msg
renderInputErrors errors =
    List.map (text << getFeedbackText) errors
        |> List.map (\t -> div [ class "text-danger" ] [ t ])
        |> Form.help []


playersToItems :
    Config msg
    -> Int
    ->
        List
            { attrs : List (Html.Attribute msg)
            , html : List (Html.Html msg)
            , value : ( String, msg )
            }
playersToItems config_ index =
    let
        existingPlayers =
            Dict.get index config_.playerSearchResults
                |> Maybe.withDefault []
                |> List.map ExistingPlayer

        newPlayer =
            Dict.get index config_.playerInputs
                |> Maybe.andThen
                    (\playerName ->
                        case playerName of
                            "" ->
                                Nothing

                            _ ->
                                Dict.get index config_.playerSearchResults
                                    |> Maybe.withDefault []
                                    |> List.map .name
                                    |> List.member playerName
                                    |> (\exists ->
                                            if exists then
                                                Nothing

                                            else
                                                Just playerName
                                       )
                    )
                |> Maybe.map NewPlayer
                |> Maybe.map List.singleton
                |> Maybe.withDefault []

        players =
            existingPlayers
                ++ newPlayer

        mapHtml p =
            case p of
                ExistingPlayer p_ ->
                    [ text p_.name ]

                NewPlayer p_ ->
                    [ text (p_ ++ " (new player)") ]

        mapValue p =
            case p of
                ExistingPlayer p_ ->
                    p_.name

                NewPlayer p_ ->
                    p_
    in
    List.map
        (\p ->
            { attrs = []
            , html = mapHtml p
            , value = ( mapValue p, config_.onPlayerSelected index (mapValue p) )
            }
        )
        players


getFormFeedbackTexts : Config msg -> Html msg
getFormFeedbackTexts config_ =
    validators config_
        |> List.filterMap (\validatorFn -> validatorFn config_)
        |> List.filterMap
            (\e ->
                case e of
                    NoScoresError ->
                        Just <| getFeedbackText e

                    SingleScore ->
                        Just <| getFeedbackText e

                    _ ->
                        Nothing
            )
        |> List.map (\t -> Form.help [] [ p [ class "text-danger" ] [ text t ] ])
        |> div [ class "has-danger" ]


getFeedbackText : GameResultValidationError -> String
getFeedbackText err =
    case err of
        NoScoresError ->
            "You must enter at least two player's scores."

        SingleScore ->
            "Single player results can't be saved. This might change in the future..."

        MissingScoreError _ ->
            "Score is missing."

        MissingPlayerError _ ->
            "Player is missing."

        DuplicatePlayerNameError _ ->
            "Duplicate players"

        MissingGame ->
            "A game must be selectecd."


hasDangerIf : Bool -> Row.Option msg
hasDangerIf condition =
    if condition then
        Row.attrs [ class "has-danger" ]

    else
        Row.attrs []


type GameResultValidationError
    = NoScoresError
    | SingleScore
    | MissingScoreError (List Int)
    | MissingPlayerError (List Int)
    | DuplicatePlayerNameError (List Int)
    | MissingGame


validators : Config msg -> List (Config msg -> Maybe GameResultValidationError)
validators config_ =
    case config_.submitted of
        -- no validators as long as the form hasn't been submitted
        False ->
            []

        True ->
            [ \r ->
                case r.bggid of
                    Just _ ->
                        Nothing

                    _ ->
                        Just MissingGame
            , \r ->
                case Dict.values r.scores |> List.map .score |> List.filterMap identity of
                    [] ->
                        Just NoScoresError

                    _ ->
                        Nothing
            , \r ->
                case Dict.values r.scores |> List.map .playerName |> List.filterMap identity of
                    x :: [] ->
                        Just SingleScore

                    _ ->
                        Nothing
            , \r ->
                r.scores
                    |> Dict.toList
                    |> List.filter (\( i, score ) -> score.playerName /= Nothing && score.score == Nothing)
                    |> List.map Tuple.first
                    |> mapError MissingScoreError
            , \r ->
                r.scores
                    |> Dict.toList
                    |> List.filter (\( i, score ) -> score.playerName == Nothing && score.score /= Nothing)
                    |> List.map Tuple.first
                    |> mapError MissingPlayerError
            , \r ->
                r.scores
                    |> Dict.toList
                    |> List.map (Tuple.mapSecond .playerName)
                    |> List.filterMap
                        (\( i_, playerName_ ) ->
                            Maybe.andThen (\name -> Just ( i_, name )) playerName_
                        )
                    |> List.foldl
                        (\( i_, playerName_ ) acc ->
                            Dict.update
                                playerName_
                                (\x ->
                                    case x of
                                        Just xs ->
                                            Just (i_ :: xs)

                                        Nothing ->
                                            Just [ i_ ]
                                )
                                acc
                        )
                        Dict.empty
                    |> Dict.values
                    |> List.filter (\xs -> List.length xs > 1)
                    |> List.concatMap identity
                    |> mapError DuplicatePlayerNameError
            ]


mapError : (List a -> GameResultValidationError) -> List a -> Maybe GameResultValidationError
mapError err xe =
    if List.isEmpty xe then
        Nothing

    else
        (Just << err) xe


scoreEntryHasError : Config msg -> Int -> Bool
scoreEntryHasError config_ index =
    (not << List.isEmpty << getEntryErrors config_) index


getEntryErrors : Config msg -> Int -> List GameResultValidationError
getEntryErrors config_ index =
    validators config_
        |> List.filterMap (\fn -> fn config_)
        |> List.filterMap
            (\err ->
                let
                    mapError_ errorIndices index_ err_ =
                        if List.member index_ errorIndices then
                            Just err_

                        else
                            Nothing
                in
                case err of
                    MissingScoreError indices ->
                        mapError_ indices index err

                    MissingPlayerError indices ->
                        mapError_ indices index err

                    DuplicatePlayerNameError indices ->
                        mapError_ indices index err

                    _ ->
                        Nothing
            )


getGameErrors : Config msg -> List GameResultValidationError
getGameErrors config_ =
    validators config_
        |> List.filterMap (\fn -> fn config_)
        |> List.filterMap
            (\err ->
                case err of
                    MissingGame ->
                        Just err

                    _ ->
                        Nothing
            )
