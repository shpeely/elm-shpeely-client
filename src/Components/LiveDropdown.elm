module Components.LiveDropdown exposing (Config, SingleState, State, autoSelectSingle, config, getMsg, getSingleState, getValueById, initialSingleState, initialState, initialValue, inputOptions, items, joinMaybe, noItems, noMargin, onBackspace, onBlur, onClick, onFocus, onSelect, open, renderItem, resetState, setSingleState, subscriptions, update, view)

import Bootstrap.Form as Form
import Bootstrap.Form.Input as Input
import Browser.Events
import Dict exposing (Dict)
import Html exposing (Attribute, Html, button, div, text)
import Html.Attributes as Attrs exposing (class, style)
import Html.Events as Events
import Json.Decode as Decode exposing (Decoder)


type alias Config msg =
    { toMsg : State msg -> msg
    , id : String
    , noMargin : Bool
    , autoSelectSingle : Bool
    , inputOptions : List (Input.Option msg)
    , initialValue : Maybe String
    , invalidFeedback : Maybe String
    , items :
        List
            { attrs : List (Html.Attribute msg)
            , html : List (Html.Html msg)
            , value : ( String, msg )
            }
    }


type LiveDropdownState
    = Open
    | Closed
    | ListenClicks


type alias SingleState msg =
    { visible : LiveDropdownState
    , value : Maybe msg
    , displayValue : Maybe String
    , previousDisplayValue : Maybe String
    , highlight : Maybe Int
    , debug : String
    }


type alias State msg =
    { state : Dict String (SingleState msg)
    , lastUpdate : Maybe String
    }


config :
    String
    -> (State msg -> msg)
    -> msg
    -> Config msg
config id_ toMsg noopMsg =
    { toMsg = toMsg
    , id = id_
    , noMargin = False
    , autoSelectSingle = True
    , inputOptions = []
    , items = []
    , initialValue = Nothing
    , invalidFeedback = Nothing
    }


initialState : State msg
initialState =
    { state = Dict.empty
    , lastUpdate = Nothing
    }


initialSingleState : Maybe String -> SingleState msg
initialSingleState maybeValue =
    { visible = Closed
    , value = Nothing
    , displayValue = maybeValue
    , previousDisplayValue = maybeValue
    , highlight = Nothing
    , debug = ""
    }


close : State msg -> State msg
close { state } =
    { state =
        Dict.map
            (\_ singleState ->
                { singleState
                    | visible = Closed
                    , debug = "close"
                    , displayValue = singleState.previousDisplayValue
                }
            )
            state
    , lastUpdate = Nothing
    }


open : String -> State msg -> State msg
open id_ { state } =
    { state = Dict.update id_ (Maybe.map (\v -> { v | visible = Open, debug = "open" })) state
    , lastUpdate = Nothing
    }


subscriptions : State msg -> (State msg -> msg) -> List (Sub msg)
subscriptions { state } toMsg =
    Dict.map
        (\id singleState ->
            case singleState.visible of
                ListenClicks ->
                    Browser.Events.onAnimationFrame
                        (\_ -> toMsg <| open id { lastUpdate = Nothing, state = state })

                Open ->
                    Browser.Events.onClick
                        (Decode.succeed <| toMsg <| close { lastUpdate = Nothing, state = state })

                Closed ->
                    Sub.none
        )
        state
        |> Dict.values


resetState : State msg -> String -> State msg
resetState state id =
    { state | state = Dict.update id (Maybe.map (always (initialSingleState Nothing))) state.state }


inputOptions : List (Input.Option msg) -> Config msg -> Config msg
inputOptions options config_ =
    { config_ | inputOptions = options }


initialValue : Maybe String -> Config msg -> Config msg
initialValue value_ config_ =
    { config_ | initialValue = value_ }


noMargin : Config msg -> Config msg
noMargin config_ =
    { config_ | noMargin = True }


autoSelectSingle : Bool -> Config msg -> Config msg
autoSelectSingle autoSelect config_ =
    { config_ | autoSelectSingle = autoSelect }


items :
    List
        { attrs : List (Html.Attribute msg)
        , html : List (Html.Html msg)
        , value : ( String, msg )
        }
    -> Config msg
    -> Config msg
items dropdownItems config_ =
    { config_ | items = dropdownItems }


noItems :
    List
        { attrs : List (Html.Attribute msg)
        , html : List (Html.Html msg)
        , value : ( String, v )
        }
noItems =
    []


view : State msg -> Config msg -> Html msg
view state config_ =
    let
        singleState =
            getSingleState config_ state

        visible =
            case singleState.visible of
                Closed ->
                    False

                ListenClicks ->
                    True

                Open ->
                    True

        -- hide dropdown completely if item list is empty
        dropdownClass =
            "dropdown-menu live-dropdown-menu"
                ++ (if List.isEmpty config_.items || not visible then
                        " hidden-xs-up d-none"

                    else
                        ""
                   )
                ++ (if config_.noMargin then
                        " live-dropdown-menu-no-margin"

                    else
                        ""
                   )
    in
    div [ style "position" "relative" ]
        [ Input.text <| config_.inputOptions ++ privateInputOptions config_ state
        , case config_.invalidFeedback of
            Just feedback ->
                Form.invalidFeedback [] [ text feedback ]

            Nothing ->
                text ""
        , div
            [ class dropdownClass ]
            (List.indexedMap (\index item -> renderItem config_ state (isHighlighted singleState index) item) config_.items)
        ]


isHighlighted : SingleState msg -> Int -> Bool
isHighlighted state index =
    state.highlight == Just index


privateInputOptions : Config msg -> State msg -> List (Input.Option msg)
privateInputOptions config_ state =
    let
        opts =
            case getSingleState config_ state |> .displayValue of
                Just v ->
                    [ Input.value v ]

                Nothing ->
                    []
    in
    opts
        ++ ([ Input.attrs
                [ onFocus config_ state
                , onBlur config_ state
                , onClick config_ state
                , onKeyDown config_ state
                ]
            ]
                ++ (case config_.invalidFeedback of
                        Just _ ->
                            [ Input.danger ]

                        Nothing ->
                            []
                   )
           )


renderItem :
    Config msg
    -> State msg
    -> Bool
    ->
        { attrs : List (Html.Attribute msg)
        , html : List (Html.Html msg)
        , value : ( String, msg )
        }
    -> Html.Html msg
renderItem options state highlight item =
    let
        itemClass =
            if highlight then
                "dropdown-item active"

            else
                "dropdown-item"
    in
    button
        ([ class itemClass
         , onClickPreventDefault (getSelectMsg options state item.value)
         , Attrs.tabindex 0
         ]
            ++ item.attrs
        )
        item.html


getSelectMsg : Config msg -> State msg -> ( String, msg ) -> msg
getSelectMsg config_ state value =
    let
        ( name, v ) =
            value
    in
    state
        |> privateSetValue config_ (Just v)
        |> privateSetDisplayValue config_ (Just name)
        |> privateSetPreviousDisplayValue config_ (Just name)
        |> privateSetDebug config_ "onSelect"
        |> config_.toMsg


onSelect : Config msg -> State msg -> ( String, msg ) -> Attribute msg
onSelect config_ state value =
    getSelectMsg config_ state value
        |> Events.onMouseDown


onBlur : Config msg -> State msg -> Attribute msg
onBlur config_ state =
    let
        singleState =
            getSingleState config_ state

        ( name, v ) =
            if config_.autoSelectSingle then
                case config_.items of
                    [ { value } ] ->
                        ( Just <| Tuple.first value, Just <| Tuple.second value )

                    _ ->
                        ( singleState.previousDisplayValue, Nothing )

            else
                ( singleState.previousDisplayValue, Nothing )
    in
    state
        |> privateSetHighlight config_ Nothing
        |> privateSetDebug config_ "onBlur"
        |> config_.toMsg
        |> Events.onBlur


onFocus : Config msg -> State msg -> Attribute msg
onFocus config_ state =
    state
        |> privateSetVisible config_ Open
        |> privateSetDisplayValue config_ Nothing
        |> privateSetDebug config_ "onFocus"
        |> config_.toMsg
        |> Events.onFocus


onClickPreventDefault : msg -> Attribute msg
onClickPreventDefault msg =
    Events.preventDefaultOn "click" (Decode.map (\msg_ -> ( msg_, True )) (Decode.succeed msg))


onClick : Config msg -> State msg -> Attribute msg
onClick config_ state =
    state
        |> privateSetVisible config_ Open
        |> privateSetDisplayValue config_ Nothing
        |> privateSetDebug config_ "onClick"
        |> privateSetVisible config_ ListenClicks
        |> config_.toMsg
        |> Events.onClick


onKeyDown : Config msg -> State msg -> Attribute msg
onKeyDown config_ state =
    let
        succeed preventDefault msg =
            Decode.succeed ( msg, preventDefault )
    in
    Events.preventDefaultOn "keydown"
        (Decode.field "key" Decode.string
            |> Decode.andThen
                (\key ->
                    case key of
                        "Backspace" ->
                            onBackspace config_ state |> succeed False

                        "ArrowDown" ->
                            onArrowDown config_ state |> succeed False

                        "ArrowUp" ->
                            onArrowUp config_ state |> succeed False

                        "Enter" ->
                            onEnter config_ state |> succeed True

                        "Escape" ->
                            onEscape config_ state |> succeed False

                        _ ->
                            Decode.fail ""
                )
        )


onBackspace : Config msg -> State msg -> msg
onBackspace config_ state =
    state
        |> privateSetVisible config_ Open
        |> privateSetDisplayValue config_ Nothing
        |> privateSetPreviousDisplayValue config_ Nothing
        |> privateSetDebug config_ "onBackspace"
        |> config_.toMsg


onEscape : Config msg -> State msg -> msg
onEscape config_ state =
    state
        |> privateSetVisible config_ Open
        |> config_.toMsg


onArrowDown : Config msg -> State msg -> msg
onArrowDown config_ state =
    state
        |> privateIncrementHighlight config_
        |> config_.toMsg


onArrowUp : Config msg -> State msg -> msg
onArrowUp config_ state =
    state
        |> privateDecrementHighlight config_
        |> config_.toMsg


onEnter : Config msg -> State msg -> msg
onEnter config_ state =
    state
        |> privateSetVisible config_ Open
        |> privateSetHighlightedValue config_
        |> privateSetDebug config_ "onEnter"
        |> config_.toMsg


privateSetHighlightedValue : Config msg -> State msg -> State msg
privateSetHighlightedValue config_ state =
    let
        singleState =
            getSingleState config_ state
    in
    singleState.highlight
        |> Maybe.andThen
            (\highlighted ->
                config_.items
                    |> List.indexedMap Tuple.pair
                    |> List.filter (\tuple -> Tuple.first tuple == highlighted)
                    |> List.head
                    |> Maybe.map (Tuple.second >> .value)
                    |> Maybe.map
                        (\( displayValue, value ) ->
                            state
                                |> privateSetValue config_ (Just value)
                                |> privateSetDisplayValue config_ (Just displayValue)
                                |> privateSetPreviousDisplayValue config_ (Just displayValue)
                                |> privateSetDebug config_ "setHighlightedValue"
                        )
            )
        |> Maybe.withDefault state


privateIncrementHighlight : Config msg -> State msg -> State msg
privateIncrementHighlight config_ state =
    case privateGetHighlight config_ state of
        Just highlighted ->
            privateSetHighlight config_ (Just <| highlighted + 1) state

        Nothing ->
            privateSetHighlight config_ (Just 0) state


privateDecrementHighlight : Config msg -> State msg -> State msg
privateDecrementHighlight config_ state =
    case privateGetHighlight config_ state of
        Just 0 ->
            privateSetHighlight config_ Nothing state

        Just highlighted ->
            privateSetHighlight config_ (Just <| highlighted - 1) state

        Nothing ->
            privateSetHighlight config_ Nothing state


privateSetHighlight : Config msg -> Maybe Int -> State msg -> State msg
privateSetHighlight config_ maybeHighlightIndex state =
    let
        currentState =
            getSingleState config_ state
    in
    setSingleState config_ state { currentState | highlight = maybeHighlightIndex }


privateGetHighlight : Config msg -> State msg -> Maybe Int
privateGetHighlight config_ state =
    getSingleState config_ state |> .highlight


privateSetVisible : Config msg -> LiveDropdownState -> State msg -> State msg
privateSetVisible config_ visible state =
    let
        currentState =
            getSingleState config_ state
    in
    setSingleState config_ state { currentState | visible = visible }


privateSetDebug : Config msg -> String -> State msg -> State msg
privateSetDebug config_ debugMsg state =
    let
        currentState =
            getSingleState config_ state
    in
    setSingleState config_ state { currentState | debug = debugMsg }


privateSetDisplayValue : Config msg -> Maybe String -> State msg -> State msg
privateSetDisplayValue config_ displayValue state =
    let
        currentState =
            getSingleState config_ state
    in
    setSingleState config_ state { currentState | displayValue = displayValue }


privateSetPreviousDisplayValue : Config msg -> Maybe String -> State msg -> State msg
privateSetPreviousDisplayValue config_ previousDisplayValue state =
    let
        currentState =
            getSingleState config_ state
    in
    setSingleState config_ state { currentState | previousDisplayValue = previousDisplayValue }


privateSetValue : Config msg -> Maybe msg -> State msg -> State msg
privateSetValue config_ value_ state =
    let
        currentSingleState =
            getSingleState config_ state

        newState =
            { state | lastUpdate = Just config_.id }
    in
    setSingleState config_ newState { currentSingleState | value = value_, visible = Closed }


getSingleState : Config msg -> State msg -> SingleState msg
getSingleState config_ state =
    Dict.get config_.id state.state
        |> Maybe.withDefault (initialSingleState config_.initialValue)


getValueById : State msg -> String -> Maybe msg
getValueById state id_ =
    Dict.get id_ state.state
        |> Maybe.map .value
        |> joinMaybe


setSingleState : Config msg -> State msg -> SingleState msg -> State msg
setSingleState config_ state singleState =
    { state
        | state = Dict.insert config_.id singleState state.state
    }


getMsg : State msg -> Maybe msg
getMsg state =
    state.lastUpdate
        |> Maybe.andThen (getValueById state)


update : State msg -> State msg
update state =
    { state | lastUpdate = Nothing }



-- HELPERS


joinMaybe : Maybe (Maybe a) -> Maybe a
joinMaybe mx =
    case mx of
        Just x ->
            x

        Nothing ->
            Nothing
