module Components.Spinner exposing (fullPageSpinner, spinner)

import Html exposing (Html, div, span)
import Html.Attributes exposing (class)


spinner : Html msg
spinner =
    div [ class "spinner-parent" ]
        [ div [ class "la-ball-clip-rotate-multiple la-dark" ]
            [ div []
                []
            , div []
                []
            ]
        ]


fullPageSpinner : Html msg
fullPageSpinner =
    div [ class "spinner-parent" ]
        [ div [ class "la-ball-clip-rotate-multiple la-dark" ]
            [ div []
                []
            , div []
                []
            ]
        ]
