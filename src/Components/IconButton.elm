module Components.IconButton exposing (iconButton)

import FeatherIcons exposing (Icon)
import Html exposing (..)
import Html.Events exposing (onClick)
import Svg exposing (..)
import Svg.Attributes exposing (..)


iconButtonInternal : Icon -> msg -> Html msg
iconButtonInternal icon onClickMsg =
    div
        [ class "icon-button-container" ]
        [ button
            [ class "icon-button"
            , type_ "button"
            , onClick onClickMsg
            ]
            [ icon |> FeatherIcons.toHtml [] ]
        ]


iconButton : Icon -> msg -> Html msg
iconButton icon onClickMsg =
    iconButtonInternal icon onClickMsg
