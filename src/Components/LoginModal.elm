module Components.LoginModal exposing (view)

import Bootstrap.Modal as Modal
import Components.LoginButtons as LoginButtons
import Html exposing (Html, p, text)
import Model.Main exposing (Model)
import Msgs.LoginModalMsgs exposing (LoginModalMsg(..))
import Msgs.Main exposing (Msg(..))


view : Model -> Html Msg
view model =
    Modal.config (LoginModalMsgs CloseModal)
        -- Configure the modal to use animations providing the new AnimateModal msg
        |> Modal.withAnimation (LoginModalMsgs << AnimateModal)
        |> Modal.small
        |> Modal.h2 [] [ text "Login" ]
        |> Modal.body []
            [ p [] [ text "Please select one of the flowing login options: " ]
            , LoginButtons.loginButtons model.flags
            ]
        |> Modal.view model.loginModal.visibility
