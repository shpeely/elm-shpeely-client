module Components.GameResultCol exposing (render)

import Bootstrap.Button as Button
import Bootstrap.Dropdown as Dropdown
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Components.GameResultChart as GameResultChart
import Dict
import FeatherIcons
import Helper.Grid as GridHelper exposing (defaultCol)
import Helper.League as LeagueHelper
import Helper.Time exposing (formatDate)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Model.BasicTypes exposing (Size)
import Model.GameResult exposing (BggInfo, GameResult, GameResultMeta, Score)
import Model.GraphQL exposing (GraphQLData)
import Model.League exposing (League, MemberRole(..))
import Model.Main exposing (Model)
import Model.Route exposing (LeagueSubRoute(..), ResultSubRoute(..), Route(..))
import Model.Stats exposing (GameResultStats)
import Msgs.BootstrapMsgs exposing (BootstrapMsg(..))
import Msgs.Main exposing (Msg(..))
import Msgs.ResultsViewMsgs exposing (ResultsViewMsg(..))
import RemoteData exposing (RemoteData(..), WebData)
import Time exposing (Posix)


chartInput :
    Model
    -> Maybe League
    -> String
    ->
        Maybe
            { league : League
            , gameResultStats : GameResultStats
            , windowSize : Size
            , colWidth : GridHelper.Col
            }
chartInput model maybeLeague resultId =
    Maybe.map2
        (\t s ->
            { league = t
            , gameResultStats = s
            , windowSize = model.windowSize
            , colWidth = { defaultCol | xl = 4, lg = 4, md = 6, sm = 12 }
            }
        )
        maybeLeague
        (model.gameResultStats |> Dict.get resultId |> Maybe.withDefault Loading |> RemoteData.toMaybe)


renderResultChart :
    Model
    -> Maybe League
    ->
        { a
            | id : String
            , time : Posix
            , bggInfo : BggInfo
            , scores : List Score
        }
    -> Html Msg
renderResultChart model maybeLeague result =
    GameResultChart.render <| chartInput model maybeLeague result.id


resultDropdown : String -> Html Msg
resultDropdown resultId =
    text ""


render :
    Model
    ->
        { a
            | id : String
            , time : Posix
            , bggInfo : BggInfo
            , scores : List Score
        }
    -> GraphQLData League
    -> Grid.Column Msg
render model result league =
    let
        dropdownState =
            Dict.get result.id model.bootstrapState.resultDropdownState
                |> Maybe.withDefault Dropdown.initialState

        maybeLeague =
            league
                |> RemoteData.toMaybe

        isMember =
            maybeLeague
                |> Maybe.map (\t -> LeagueHelper.isMember t model.userId)
                |> Maybe.withDefault False

        isAdmin =
            maybeLeague
                |> Maybe.map (\t -> LeagueHelper.hasRole t model.userId Admin)
                |> Maybe.withDefault False

        -- dropdown controls visible to all
        anonymousControls result_ =
            case maybeLeague of
                Just t ->
                    [ Dropdown.buttonItem
                        [ onClick <| NavigateTo <| LeagueRoute t.slug <| ResultRoute result_.id ResultDetails ]
                        [ text "Result Details" ]
                    ]

                Nothing ->
                    []

        -- dropdown controls visible only to members
        memberControls =
            []

        -- dropdown controls visible only to admins
        adminControls result_ =
            case maybeLeague of
                Just t ->
                    [ Dropdown.buttonItem
                        [ onClick <| NavigateTo <| LeagueRoute t.slug <| ResultRoute result_.id EditResult
                        ]
                        [ text "Edit Result" ]
                    , Dropdown.buttonItem
                        [ class "text-danger"
                        , onClick (ResultsViewMsgs <| DeleteResultConfirmation result.id)
                        ]
                        [ text "Delete Result" ]
                    ]

                Nothing ->
                    []

        controls result_ =
            anonymousControls result_
                ++ (if isMember then
                        []

                    else
                        []
                   )
                ++ (if isAdmin then
                        adminControls result_

                    else
                        []
                   )

        finalControls =
            controls result
    in
    Grid.col [ Col.lg4, Col.md6, Col.sm12 ]
        [ div [ style "display" "flex" ]
            [ h4 [ class "mb-0 eclipse-text" ]
                [ text result.bggInfo.name ]
            , if List.length (controls result) > 0 then
                Dropdown.dropdown
                    dropdownState
                    { options = [ Dropdown.dropLeft ]
                    , toggleMsg = ToggleResultMoreDropdown result.id >> BootstrapMsgs
                    , toggleButton =
                        Dropdown.toggle
                            [ Button.roleLink
                            , Button.attrs [ class "dropdown-toggle-none p-0" ]
                            ]
                            [ FeatherIcons.toHtml [] FeatherIcons.moreVertical
                            ]
                    , items = finalControls
                    }

              else
                text ""
            ]
        , div []
            [ span [] [ text <| formatDate result.time ]
            , span [ class "float-right mr-1" ]
                [ text <| (String.fromInt <| List.length result.scores) ++ " players" ]
            ]
        , renderResultChart model maybeLeague result
        ]
