module Components.GamePreview exposing (render)

import Bootstrap.Alert as Alert
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Components.Spinner exposing (spinner)
import Html exposing (..)
import Html.Attributes exposing (..)
import Model.Bgg exposing (GameInfo)
import Msgs.Main exposing (Msg)
import RemoteData exposing (RemoteData)


render : RemoteData e GameInfo -> Int -> Html Msg
render response descriptionCutOff =
    case response of
        RemoteData.Success gameInfo ->
            Grid.row []
                [ Grid.col [ Col.xs12 ]
                    [ h2 []
                        [ text gameInfo.name
                        , small [ class "ml-2" ]
                            [ text ("(" ++ String.fromInt gameInfo.yearPublished ++ ")") ]
                        ]
                    ]
                , Grid.col [ Col.xs12 ]
                    [ div [ class "media" ]
                        [ img [ alt gameInfo.name, class "d-flex mr-3", src gameInfo.thumbnail ] []
                        , div [ class "media-body" ]
                            [ gameDescription gameInfo
                            ]
                        ]
                    ]
                , Grid.col [ Col.xs12 ]
                    [ if descriptionCutOff > 0 then
                        p [] [ text (gameInfo.description |> String.left descriptionCutOff |> (\b a -> String.append a b) "...") ]

                      else
                        text ""
                    ]
                ]

        RemoteData.Loading ->
            spinner

        RemoteData.NotAsked ->
            text ""

        RemoteData.Failure err ->
            Alert.simpleDanger [] [ text "Failed to load game information from Board Game Geek :(" ]


gameDescription : GameInfo -> Html Msg
gameDescription gameInfo =
    dl [ class "row" ]
        [ dt [ class "col-sm-6" ]
            [ text "Rating" ]
        , dd [ class "col-sm-5" ]
            [ text <| String.left 5 <| String.fromFloat gameInfo.rating ]
        , dt [ class "col-sm-6" ]
            [ text "Weight" ]
        , dd [ class "col-sm-5" ]
            [ text <| String.left 5 <| String.fromFloat gameInfo.weight ]
        , dt [ class "col-sm-6 d-none d-sm-block" ]
            [ text "Owned By" ]
        , dd [ class "col-sm-5 d-none d-sm-block" ]
            [ text <| String.fromInt gameInfo.ownedBy ]
        , dt [ class "col-sm-6" ]
            [ text "Max Players" ]
        , dd [ class "col-sm-5" ]
            [ text <| String.fromInt gameInfo.maxPlayers ]
        , dt [ class "col-sm-6" ]
            [ text "Play Time" ]
        , dd [ class "col-sm-5" ]
            [ text <| String.fromInt gameInfo.playTime ++ "min" ]
        , dt [ class "col-sm-6 d-none d-sm-block" ]
            [ text "Rank" ]
        , dd [ class "col-sm-5 d-none d-sm-block" ]
            [ text <| String.fromInt gameInfo.rank ]
        ]
