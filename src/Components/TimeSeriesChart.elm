module Components.TimeSeriesChart exposing (render)

import Array
import Axis
import Color exposing (Color)
import Components.Spinner exposing (spinner)
import Dict exposing (Dict)
import Helper.ChartSettings as ChartSettingsHelper
import Helper.Charts as ChartsHelper
import Helper.Grid as GridHelper
import Helper.League exposing (playerIdToName)
import Html exposing (Html)
import Html.Attributes as HtmlAttrs
import List exposing (range)
import Model.BasicTypes exposing (Size)
import Model.GameResult exposing (GameResultMeta)
import Model.League exposing (League)
import Model.Stats exposing (ChartSettings, TimeSeries)
import Msgs.Main exposing (..)
import Path exposing (Path)
import Scale exposing (ContinuousScale, OrdinalScale)
import Scale.Color
import Shape
import Time exposing (Posix)
import TypedSvg exposing (g, svg, text_)
import TypedSvg.Attributes exposing (alignmentBaseline, class, d, dy, fill, fontFamily, stroke, textAnchor, transform, viewBox)
import TypedSvg.Attributes.InPx exposing (fontSize, height, strokeWidth, width, x, y)
import TypedSvg.Core exposing (Svg, text)
import TypedSvg.Types exposing (AlignmentBaseline(..), AnchorAlignment(..), Fill(..), Length(..), Transform(..), em)


w : Size -> Float
w size =
    GridHelper.fullWidth size |> toFloat


h : Float
h =
    450


font : String
font =
    "Questrial"


textColorFill : Fill
textColorFill =
    Fill (Color.fromRgba { red = 41, green = 43, blue = 44, alpha = 0 })


padding : Float
padding =
    40


view :
    { timeseries : TimeSeries
    , windowSize : Size
    , league : League
    , gameResultMeta : Dict String Posix
    , currentTime : Posix
    , settings : ChartSettings
    }
    -> Svg msg
view { timeseries, windowSize, league, gameResultMeta, currentTime, settings } =
    let
        model =
            ChartSettingsHelper.filterTimeSeries settings gameResultMeta currentTime timeseries

        xMax : Int
        xMax =
            model.scores
                |> Dict.toList
                |> List.head
                |> Maybe.map Tuple.second
                |> Maybe.withDefault Array.empty
                |> Array.length

        xScale : ContinuousScale Float
        xScale =
            Scale.linear ( 0, w windowSize - 2 * padding ) ( 0, toFloat xMax )

        yScale : ContinuousScale Float
        yScale =
            model
                |> .scores
                |> Dict.toList
                |> List.map Tuple.second
                |> List.map Array.toList
                |> List.concat
                |> (\xs -> Maybe.map2 Tuple.pair (List.maximum xs) (List.minimum xs))
                |> Maybe.withDefault ( 0, 0 )
                |> Tuple.mapFirst toFloat
                |> Tuple.mapSecond toFloat
                |> Scale.linear ( 0, h - 2 * padding )

        xAxis : Svg msg
        xAxis =
            Axis.bottom [ Axis.tickCount (Basics.min xMax 10) ] xScale

        yAxis : Svg msg
        yAxis =
            Axis.left [ Axis.tickFormat ChartsHelper.formatTick ] yScale

        colorScale : OrdinalScale String Color
        colorScale =
            Scale.ordinal Scale.Color.category10 (Dict.keys model.scores)

        color : String -> Color
        color =
            Scale.convert colorScale >> Maybe.withDefault Color.black

        lineGenerator : ( Int, Int ) -> Maybe ( Float, Float )
        lineGenerator ( x, y ) =
            Just ( Scale.convert xScale (toFloat x), Scale.convert yScale (toFloat y) )

        line : String -> Path
        line player =
            model
                |> .scores
                |> Dict.get player
                |> Maybe.withDefault Array.empty
                |> Array.append (Array.fromList [ 0 ])
                |> Array.indexedMap (\a b -> ( a, b ))
                |> Array.toList
                |> List.map lineGenerator
                |> Shape.line Shape.linearCurve

        finalScore : String -> Int
        finalScore player =
            Dict.get player model.scores
                |> Maybe.withDefault Array.empty
                |> Array.toList
                |> List.reverse
                |> List.head
                |> Maybe.withDefault 0

        hasGames : Bool
        hasGames =
            Dict.values model.scores
                |> List.any (Array.isEmpty >> not)

        chartTitle : TimeSeries -> ChartSettings -> Svg msg
        chartTitle timeseries_ settings_ =
            let
                title =
                    String.join " "
                        [ ChartSettingsHelper.dataDesc timeseries_
                        , ChartSettingsHelper.timeFilterDesc settings_.timeFilter
                        ]
            in
            text_
                [ fontFamily [ font ]
                , fill textColorFill
                , textAnchor AnchorMiddle
                , fontSize 18
                , alignmentBaseline AlignmentHanging
                , class [ "hidden-sm-down" ]
                , transform [ Translate (w windowSize / 2) 10 ]
                ]
                [ text title ]
    in
    svg
        [ width (w windowSize)
        , height h
        , class [ "chart" ]
        ]
        [ chartTitle model settings
        , g
            [ transform [ Translate (padding - 1) (h - padding) ]
            , class [ "x-axis" ]
            ]
            [ xAxis ]
        , g
            [ transform [ Translate (padding - 1) padding ]
            , class [ "y-axis" ]
            ]
            [ yAxis
            , text_ [ fontFamily [ font ], x 5, y 5 ] [ text "Points" ]
            ]
        , g
            [ transform [ Translate padding padding ]
            , class [ "series" ]
            ]
            (List.map
                (\player ->
                    Path.element (line player)
                        [ stroke (color player)
                        , strokeWidth 2
                        , fill FillNone
                        ]
                )
                (Dict.keys model.scores)
            )
        , text_
            [ fontFamily [ font ]
            , textAnchor AnchorEnd
            , transform [ Translate (w windowSize - padding + 10) (h - (padding / 3)) ]
            ]
            [ text "Number of Games" ]
        , if hasGames then
            -- show player names
            g [ fontFamily [ font ], fontSize 14 ]
                (List.map
                    (\player ->
                        g
                            [ textAnchor AnchorEnd
                            , transform [ Translate (w windowSize - padding + 10) (padding + Scale.convert yScale (toFloat <| finalScore player)) ]
                            ]
                            [ text_ [ fill <| Fill (color player) ] [ text <| playerIdToName league player ] ]
                    )
                    (Dict.keys model.scores)
                )

          else
            text_
                [ fontFamily [ font ]
                , textAnchor AnchorMiddle
                , transform [ Translate ((w windowSize - padding) / 2) ((h - padding) / 2) ]
                ]
                [ text "No Games" ]
        ]


render :
    Maybe
        { league : League
        , timeseries : TimeSeries
        , gameResultMeta : Dict String Posix
        , currentTime : Posix
        , windowSize : Size
        , settings : ChartSettings
        }
    -> Html Msg
render model =
    case model of
        Just chartInput ->
            Html.div [ HtmlAttrs.class "timeseries-chart fade-in" ]
                [ view chartInput ]

        Nothing ->
            Html.div
                [ HtmlAttrs.class "timeseries-chart"
                ]
                [ spinner ]
