module Components.EmptyStateNotice exposing (renderEmptyState)

import Bootstrap.Alert as Alert
import Bootstrap.Button as Button
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Html exposing (Html, p, text)
import Html.Attributes exposing (class, href)
import Model.Route exposing (LeagueSubRoute(..), Route(..))
import Routing


renderEmptyState : Bool -> String -> String -> String -> String -> Html msg
renderEmptyState loggedIn leagueId title messageAnonymous messageLoggedIn =
    case loggedIn of
        False ->
            Alert.simpleInfo [ class "mt-2" ]
                [ Alert.h5 [] [ text title ]
                , p [] [ text messageAnonymous ]
                ]

        True ->
            Card.config []
                |> Card.block []
                    [ Block.titleH4 [] [ text title ]
                    , Block.text [] [ text messageLoggedIn ]
                    , Block.text []
                        [ Button.linkButton
                            [ Button.attrs
                                [ href <| Routing.reverseRoute <| LeagueRoute leagueId AddNewResult ]
                            , Button.primary
                            ]
                            [ text "Add a Result" ]
                        ]
                    ]
                |> Card.view
