module Api.GameStats exposing (gameResultStatsUrl, gameStatsUrl, getGameResultStats, getGameStats, getPlayerStats, getTimeseries, timeseriesUrl, url)

import Http
import Model.Flags exposing (Flags)
import Model.Main exposing (Model)
import Model.Stats exposing (..)
import Msgs.GameStatsMsgs exposing (..)
import Msgs.Main exposing (..)
import RemoteData exposing (RemoteData)


url : Flags -> String -> String
url flags endpoint =
    flags.gameStatsApiUrl ++ endpoint


timeseriesUrl : Model -> String -> String
timeseriesUrl model leagueId =
    url model.flags ("/league/" ++ leagueId ++ "/timeseries")


gameResultStatsUrl : Model -> String -> String -> String
gameResultStatsUrl model leagueId resultId =
    url model.flags ("/league/" ++ leagueId ++ "/gameresult/" ++ resultId ++ "/stats")


gameStatsUrl : Model -> String -> String
gameStatsUrl model leagueId =
    url model.flags ("/league/" ++ leagueId ++ "/games/stats")


playerStatsUrl : Model -> String -> String
playerStatsUrl model leagueId =
    url model.flags ("/league/" ++ leagueId ++ "/players/stats")


getTimeseries : Model -> String -> Cmd Msg
getTimeseries model leagueId =
    Http.get (timeseriesUrl model leagueId) decodeTimeSeries
        |> RemoteData.sendRequest
        |> Cmd.map (GameStatsMsg << TimeSeriesResponse)


getGameResultStats : Model -> String -> String -> Cmd Msg
getGameResultStats model leagueId resultId =
    Http.get (gameResultStatsUrl model leagueId resultId) decodeGameResultStats
        |> RemoteData.sendRequest
        |> Cmd.map (GameStatsMsg << GameResultStatsResponse)


getGameStats : Model -> String -> Cmd Msg
getGameStats model leagueId =
    Http.get (gameStatsUrl model leagueId) decodeGameStats
        |> RemoteData.sendRequest
        |> Cmd.map (GameStatsMsg << GameStatsResponse)


getPlayerStats : Model -> String -> Cmd Msg
getPlayerStats model leagueId =
    Http.get (playerStatsUrl model leagueId) decodePlayerStats
        |> RemoteData.sendRequest
        |> Cmd.map (GameStatsMsg << PlayerStatsResponse)
