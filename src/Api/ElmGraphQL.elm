module Api.ElmGraphQL exposing (TimeType(..), acceptInvitation, applyGameFilter, applyNumPlayerFilter, applyPlayerFilters, authenticateUser, authenticateUserMutation, bggInfo, createGameResult, createGameResultMutation, createInvitation, createInvitationMutation, createLeague, createLeagueMutation, date, deleteGameResult, deleteGameResultMutation, gameResultMetaQuery, gameResultQuery, gameResultQueryRequest, gameresult, gamesQuery, getHeaders, invitation, invitationQuery, invitationResponse, league, leagueMember, leagueQuery, leagueSummary, loadGameInfo, loadGameInfoQuery, loadGameResultDetails, loadGameResultMeta, loadGameResults, loadGameResultsWithLimit, loadGames, loadInvitation, loadLeague, loadMyLeagues, loadRecentLeagues, loadRecentResults, memberRole, player, playerFilter, recentLeaguesQuery, rejectInvitation, requestOptions, score, searchGame, searchGameQuery, sendMutationRequest, sendQueryRequest, setMemberRole, toGraphQlFilters, updateGameResult, user)

import GraphQL.Client.Http as GraphQLClient exposing (Error(..), RequestOptions)
import GraphQL.Request.Builder exposing (..)
import GraphQL.Request.Builder.Arg as Arg
import GraphQL.Request.Builder.Variable as Var exposing (VariableSpec)
import Http
import Iso8601
import Json.Decode as Decode
import Model.Bgg exposing (GameInfo, GameSearchResult)
import Model.Flags exposing (Flags)
import Model.GameResult exposing (BggInfo, GameResult, GameResultAndLeague, GameResultMeta, Score, ScoreInput, UnsavedGameResult)
import Model.GraphQL exposing (..)
import Model.Invitation exposing (Invitation, InvitationResponse(..))
import Model.League exposing (Game, IdOnly, League, LeagueMember, LeagueSummary, MemberRole(..), Player, UnsavedLeague)
import Model.LoginData exposing (LoginStatus(..))
import Model.Main exposing (Model)
import Model.User exposing (Auth0UserProfile, User)
import Msgs.Main exposing (..)
import Task exposing (Task)
import Time exposing (Posix)


type TimeType
    = TimeType


{-| custom type for dates
-}
date : ValueSpec NonNull TimeType Posix vars
date =
    Decode.string
        |> Decode.andThen
            (\timeString ->
                case Iso8601.toTime timeString of
                    Ok time ->
                        Decode.succeed time

                    Err _ ->
                        Decode.fail ("failed to decode time " ++ timeString)
            )
        |> customScalar TimeType


requestOptions : Flags -> List Http.Header -> RequestOptions
requestOptions flags headers =
    { method = "POST"
    , headers = headers
    , url = flags.graphQlUrl
    , timeout = Nothing
    , withCredentials = False
    }


getHeaders : Model -> List Http.Header
getHeaders model =
    case model.loginStatus of
        LoggedIn token ->
            [ Http.header "Authorization" ("Bearer " ++ token) ]

        _ ->
            []


sendQueryRequest : Model -> Request Query a -> Task GraphQLClient.Error a
sendQueryRequest model request =
    GraphQLClient.customSendQuery (requestOptions model.flags <| getHeaders model) request


sendMutationRequest : Model -> Request Mutation a -> Task GraphQLClient.Error a
sendMutationRequest model request =
    GraphQLClient.customSendMutation (requestOptions model.flags <| getHeaders model) request


idOnly : ValueSpec NonNull ObjectType IdOnly vars
idOnly =
    object IdOnly
        |> with (field "id" [] string)


player : ValueSpec NonNull ObjectType Player vars
player =
    object Player
        |> with (field "name" [] string)
        |> with (field "id" [] string)


score : ValueSpec NonNull ObjectType Score vars
score =
    object Score
        |> with (field "player" [] player)
        |> with (field "score" [] float)


bggInfo : ValueSpec NonNull ObjectType BggInfo vars
bggInfo =
    object BggInfo
        |> with (field "bggid" [] int)
        |> with (field "name" [] string)


gameresult : ValueSpec NonNull ObjectType GameResult vars
gameresult =
    object GameResult
        |> with (field "id" [] string)
        |> with (field "time" [] date)
        |> with (field "bggInfo" [] bggInfo)
        |> with (field "scores" [] (list score))


gameresultAndLeague : ValueSpec NonNull ObjectType GameResultAndLeague vars
gameresultAndLeague =
    object GameResultAndLeague
        |> with (field "id" [] string)
        |> with (field "time" [] date)
        |> with (field "bggInfo" [] bggInfo)
        |> with (field "scores" [] (list score))
        |> with (field "tournament" [] league)


user : ValueSpec NonNull ObjectType User vars
user =
    object User
        |> with (field "auth0id" [] string)
        |> with (field "avatar" [] string)
        |> with (field "username" [] string)


memberRole : ValueSpec NonNull EnumType MemberRole vars
memberRole =
    enum
        [ ( "MEMBER", Member )
        , ( "ADMIN", Admin )
        ]


leagueMember : ValueSpec NonNull ObjectType LeagueMember vars
leagueMember =
    object LeagueMember
        |> with (field "id" [] string)
        |> with (field "user" [] user)
        |> with (field "role" [] memberRole)


league : ValueSpec NonNull ObjectType League vars
league =
    object League
        |> with (field "name" [] string)
        |> with (field "slug" [] string)
        |> with (field "players" [] (list player))
        |> with (field "members" [] (list leagueMember))


leagueSummary =
    object LeagueSummary
        |> with (field "name" [] string)
        |> with (field "slug" [] string)
        |> with (field "numResults" [] int)
        |> with (field "numPlayers" [] int)
        |> with (field "numGames" [] int)
        |> with (field "lastGameresultAt" [] (nullable date))
        |> with (field "createdAt" [] date)


invitationResponse : ValueSpec NonNull EnumType InvitationResponse vars
invitationResponse =
    enum
        [ ( "PENDING", Pending )
        , ( "ACCEPTED", Accepted )
        , ( "REJECTED", Rejected )
        ]


leagueMemberToEnumSymbol : MemberRole -> String
leagueMemberToEnumSymbol leagueMember_ =
    case leagueMember_ of
        Admin ->
            "ADMIN"

        Member ->
            "MEMBER"


leagueMemberRoleEnum : VariableSpec Var.NonNull MemberRole
leagueMemberRoleEnum =
    Var.enum "LeagueMemberRole" leagueMemberToEnumSymbol


invitation =
    object Invitation
        |> with (field "state" [] invitationResponse)
        |> with (field "tournament" [] leagueSummary)
        |> with (field "inviter" [] user)


loadInvitation : Model -> String -> Cmd Msg
loadInvitation model token =
    sendQueryRequest model (request { token = token } invitationQuery)
        |> Task.attempt InvitationResponse


invitationQuery : Document Query (Maybe Invitation) { vars | token : String }
invitationQuery =
    let
        tokenVar =
            Var.required "token" .token Var.string

        queryRoot =
            extract
                (field "invitation"
                    [ ( "where"
                      , Arg.object [ ( "token", Arg.variable tokenVar ) ]
                      )
                    ]
                    (nullable invitation)
                )
    in
    queryDocument queryRoot


acceptInviationMutation : Document Mutation (Maybe Invitation) { vars | token : String }
acceptInviationMutation =
    let
        tokenVar =
            Var.required "token" .token Var.string

        queryRoot =
            extract
                (field "acceptInvitation"
                    [ ( "where", Arg.object [ ( "token", Arg.variable tokenVar ) ] )
                    ]
                    (nullable invitation)
                )
    in
    mutationDocument queryRoot


acceptInvitation : Model -> String -> Cmd Msg
acceptInvitation model token =
    sendMutationRequest model (request { token = token } acceptInviationMutation)
        |> Task.attempt InvitationResponse


rejectInviationMutation : Document Mutation (Maybe Invitation) { vars | token : String }
rejectInviationMutation =
    let
        tokenVar =
            Var.required "token" .token Var.string

        queryRoot =
            extract
                (field "rejectInvitation"
                    [ ( "where", Arg.object [ ( "token", Arg.variable tokenVar ) ] )
                    ]
                    (nullable invitation)
                )
    in
    mutationDocument queryRoot


rejectInvitation : Model -> String -> Cmd Msg
rejectInvitation model token =
    sendMutationRequest model (request { token = token } rejectInviationMutation)
        |> Task.attempt InvitationResponse


leagueQuery : Document Query League { vars | slug : String }
leagueQuery =
    let
        slugVar =
            Var.required "slug" .slug Var.string

        queryRoot =
            extract
                (field "tournament"
                    [ ( "where"
                      , Arg.object [ ( "slug", Arg.variable slugVar ) ]
                      )
                    ]
                    league
                )
    in
    queryDocument queryRoot


loadLeague : Model -> String -> Cmd Msg
loadLeague model slug =
    sendQueryRequest model (request { slug = slug } leagueQuery)
        |> Task.attempt LeagueResponse


recentLeaguesQuery : Bool -> Document Query (List LeagueSummary) { vars | auth0id : Maybe String }
recentLeaguesQuery myLeagues =
    let
        auth0idVar =
            Var.optional "auth0id" .auth0id Var.string ""

        filter =
            if myLeagues then
                "members_some"

            else
                "members_none"

        first =
            if myLeagues then
                100

            else
                4

        additionalFilters =
            if myLeagues then
                []

            else
                [ ( "NOT"
                  , Arg.object
                        [ ( "lastGameresultAt"
                          , Arg.null
                          )
                        ]
                  )
                ]

        queryRoot =
            extract
                (field "tournaments"
                    [ ( "first", Arg.int first )
                    , ( "orderBy", Arg.enum "lastGameresultAt_DESC" )
                    , ( "where"
                      , Arg.object
                            ([ ( filter
                               , Arg.object
                                    [ ( "user"
                                      , Arg.object
                                            [ ( "auth0id", Arg.variable auth0idVar ) ]
                                      )
                                    ]
                               )
                             ]
                                ++ additionalFilters
                            )
                      )
                    ]
                    (list leagueSummary)
                )
    in
    queryDocument queryRoot


recentResultsQuery : Document Query (List GameResultAndLeague) vars
recentResultsQuery =
    let
        queryRoot =
            extract
                (field "gameresults"
                    [ ( "first", Arg.int 3 )
                    , ( "orderBy", Arg.enum "time_DESC" )
                    ]
                    (list gameresultAndLeague)
                )
    in
    queryDocument queryRoot


loadRecentLeagues : Model -> Cmd Msg
loadRecentLeagues model =
    sendQueryRequest model (request { auth0id = model.userId } (recentLeaguesQuery False))
        |> Task.attempt RecentLeaguesResponse


loadMyLeagues : Model -> Cmd Msg
loadMyLeagues model =
    sendQueryRequest model (request { auth0id = model.userId } (recentLeaguesQuery True))
        |> Task.attempt MyLeaguesResponse


loadRecentResults : Model -> Cmd Msg
loadRecentResults model =
    sendQueryRequest model (request {} recentResultsQuery)
        |> Task.attempt RecentResultsResponse


playerFilter : Bool -> List String -> ( String, Arg.Value vars )
playerFilter together playerIds =
    playerIds
        |> List.map
            (\id ->
                Arg.object
                    [ ( "scores_some"
                      , Arg.object
                            [ ( "player"
                              , Arg.object
                                    [ ( "id", Arg.string id )
                                    ]
                              )
                            ]
                      )
                    ]
            )
        |> (\f ->
                ( if together then
                    "AND"

                  else
                    "OR"
                , Arg.list f
                )
           )


applyGameFilter : ResultsFilter -> List ( String, Arg.Value vars ) -> List ( String, Arg.Value vars )
applyGameFilter filter acc =
    case filter.gameFilter of
        Just gameId ->
            acc ++ [ ( "bggInfo", Arg.object [ ( "id", Arg.string gameId ) ] ) ]

        Nothing ->
            acc


toGraphQlFilters : ResultsFilter -> List ( String, Arg.Value vars )
toGraphQlFilters filter =
    []
        |> applyGameFilter filter
        |> applyPlayerFilters filter
        |> applyNumPlayerFilter filter
        |> applyIdFilter filter


applyIdFilter : ResultsFilter -> List ( String, Arg.Value vars ) -> List ( String, Arg.Value vars )
applyIdFilter filter acc =
    case filter.idFilter of
        Just ids ->
            acc ++ [ ( "id_in", Arg.list (List.map Arg.string ids) ) ]

        Nothing ->
            acc


applyNumPlayerFilter : ResultsFilter -> List ( String, Arg.Value vars ) -> List ( String, Arg.Value vars )
applyNumPlayerFilter filter acc =
    case filter.numPlayerFilter of
        Just numPlayers ->
            acc ++ [ ( "numPlayers", Arg.int numPlayers ) ]

        Nothing ->
            acc


applyPlayerFilters : ResultsFilter -> List ( String, Arg.Value vars ) -> List ( String, Arg.Value vars )
applyPlayerFilters filter acc =
    case filter.playerFilter.playerIds of
        [] ->
            acc

        _ ->
            acc ++ [ playerFilter filter.playerFilter.together filter.playerFilter.playerIds ]


gameResultQuery : ResultsFilter -> Document Query (List GameResult) { vars | first : Int, slug : String }
gameResultQuery resultsFilter =
    let
        firstVar =
            Var.required "first" .first Var.int

        slugVar =
            Var.required "slug" .slug Var.string

        additionalFilters =
            toGraphQlFilters resultsFilter

        -- base filters
        filters =
            [ ( "first", Arg.variable firstVar )
            , ( "orderBy", Arg.enum "time_DESC" )
            , ( "where"
              , Arg.object
                    [ ( "AND"
                      , Arg.object
                            ([ ( "tournament"
                               , Arg.object
                                    [ ( "slug", Arg.variable slugVar )
                                    ]
                               )
                             ]
                                ++ additionalFilters
                            )
                      )
                    ]
              )
            ]

        queryRoot =
            extract
                (field "gameresults" filters (list gameresult))
    in
    queryDocument queryRoot


gameResultQueryRequest : Int -> String -> ResultsFilter -> Request Query (List GameResult)
gameResultQueryRequest limit slug filters =
    gameResultQuery filters
        |> request { first = limit, slug = slug }


loadGameResults : Model -> String -> (GraphQLGameResultResponse -> Msg) -> Cmd Msg
loadGameResults =
    loadGameResultsWithLimit 20


loadGameResultsWithLimit : Int -> Model -> String -> (GraphQLGameResultResponse -> Msg) -> Cmd Msg
loadGameResultsWithLimit limit model slug msg =
    sendQueryRequest model (gameResultQueryRequest limit slug model.resultsView.resultsFilter)
        |> Task.attempt msg


authenticateUserMutation : Document Mutation (Maybe UserId) { vars | accessToken : String }
authenticateUserMutation =
    let
        authUser =
            object UserId
                |> with (field "auth0id" [] string)

        accessTokenVar =
            Var.required "idToken" .accessToken Var.string

        queryRoot =
            extract
                (field "authenticate"
                    [ ( "idToken", Arg.variable accessTokenVar ) ]
                    (nullable authUser)
                )
    in
    mutationDocument queryRoot


authenticateUser : Model -> Cmd Msg
authenticateUser model =
    case model.loginStatus of
        LoggedIn token ->
            sendMutationRequest model (request { accessToken = token } authenticateUserMutation)
                |> Task.attempt UserIdResponse

        _ ->
            Cmd.none


createLeagueMutation : Document Mutation League { vars | name : String }
createLeagueMutation =
    let
        nameVar =
            Var.required "name" .name Var.string

        args =
            [ ( "data"
              , Arg.object
                    [ ( "name", Arg.variable nameVar ) ]
              )
            ]

        queryRoot =
            extract
                (field "createTournament"
                    args
                    league
                )
    in
    mutationDocument queryRoot


createLeague : Model -> Cmd Msg
createLeague model =
    sendMutationRequest model (request { name = model.newLeague.name } createLeagueMutation)
        |> Task.attempt CreateLeagueResponse


createInvitationMutation : Document Mutation Invitation { vars | email : String, slug : String }
createInvitationMutation =
    let
        emailVar =
            Var.required "email" .email Var.string

        slugVar =
            Var.required "slug" .slug Var.string

        args =
            [ ( "data"
              , Arg.object
                    [ ( "email", Arg.variable emailVar )
                    , ( "slug", Arg.variable slugVar )
                    ]
              )
            ]

        queryRoot =
            extract
                (field "createInvitation"
                    args
                    invitation
                )
    in
    mutationDocument queryRoot


createInvitation : Model -> String -> String -> Cmd Msg
createInvitation model slug email =
    sendMutationRequest model (request { slug = slug, email = email } createInvitationMutation)
        |> Task.attempt CreateInvitationResponse


createGameResultMutation : Document Mutation GameResult { vars | bggid : Int, slug : String, scores : List ScoreInput }
createGameResultMutation =
    let
        bggIdVar =
            Var.required "bggid" .bggid Var.int

        slugVar =
            Var.required "slug" .slug Var.string

        scoresVar =
            Var.required "scores"
                .scores
                (Var.list
                    (Var.object "ScoreCreateInput"
                        [ Var.field "playerName" .playerName Var.string
                        , Var.field "score" .score Var.float
                        ]
                    )
                )

        args =
            [ ( "data"
              , Arg.object
                    [ ( "bggid", Arg.variable bggIdVar )
                    , ( "slug", Arg.variable slugVar )
                    , ( "scores", Arg.variable scoresVar )
                    ]
              )
            ]

        queryRoot =
            extract
                (field "createGameresult"
                    args
                    gameresult
                )
    in
    mutationDocument queryRoot


createGameResult : Model -> String -> UnsavedGameResult -> (Result Error GameResult -> Msg) -> Cmd Msg
createGameResult model slug unsavedResult msg =
    let
        variables =
            { slug = slug
            , bggid = unsavedResult.bggid
            , scores = unsavedResult.scores
            }
    in
    sendMutationRequest model (request variables createGameResultMutation)
        |> Task.attempt msg


updateGameResultMutation : Document Mutation GameResult { vars | bggid : Int, resultId : String, scores : List ScoreInput }
updateGameResultMutation =
    let
        bggIdVar =
            Var.required "bggid" .bggid Var.int

        resultIdVar =
            Var.required "resultId" .resultId Var.id

        scoresVar =
            Var.required "scores"
                .scores
                (Var.list
                    (Var.object "ScoreCreateInput"
                        [ Var.field "playerName" .playerName Var.string
                        , Var.field "score" .score Var.float
                        ]
                    )
                )

        args =
            [ ( "where", Arg.object [ ( "id", Arg.variable resultIdVar ) ] )
            , ( "data"
              , Arg.object
                    [ ( "bggid", Arg.variable bggIdVar )
                    , ( "scores", Arg.variable scoresVar )
                    ]
              )
            ]

        queryRoot =
            extract
                (field "updateGameresult"
                    args
                    gameresult
                )
    in
    mutationDocument queryRoot


updateGameResult : Model -> String -> UnsavedGameResult -> (Result Error GameResult -> Msg) -> Cmd Msg
updateGameResult model resultId unsavedResult msg =
    let
        variables =
            { resultId = resultId
            , bggid = unsavedResult.bggid
            , scores = unsavedResult.scores
            }
    in
    sendMutationRequest model (request variables updateGameResultMutation)
        |> Task.attempt msg


deleteGameResultMutation : Document Mutation IdOnly { vars | resultId : String }
deleteGameResultMutation =
    let
        resultIdVar =
            Var.required "resultId" .resultId Var.id

        args =
            [ ( "where", Arg.object [ ( "id", Arg.variable resultIdVar ) ] ) ]

        queryRoot =
            extract
                (field "deleteGameresult"
                    args
                    idOnly
                )
    in
    mutationDocument queryRoot


deleteGameResult : Model -> String -> Cmd Msg
deleteGameResult model resultId =
    sendMutationRequest model (request { resultId = resultId } deleteGameResultMutation)
        |> Task.attempt DeleteGameResultResponse


gameResultDetailsQuery : Document Query GameResult { vars | resultId : String }
gameResultDetailsQuery =
    let
        resultIdVar =
            Var.required "id" .resultId Var.id

        queryRoot =
            extract
                (field "gameresult"
                    [ ( "where", Arg.object [ ( "id", Arg.variable resultIdVar ) ] )
                    ]
                    gameresult
                )
    in
    queryDocument queryRoot


loadGameResultDetails : Model -> String -> (Result Error GameResult -> Msg) -> Cmd Msg
loadGameResultDetails model resultId msg =
    sendQueryRequest model (request { resultId = resultId } gameResultDetailsQuery)
        |> Task.attempt msg


gameResultMetaQuery : Document Query (List GameResultMeta) { vars | slug : String }
gameResultMetaQuery =
    let
        slugVar =
            Var.required "slug" .slug Var.string

        gameresultMeta =
            object GameResultMeta
                |> with (field "id" [] string)
                |> with (field "time" [] date)

        queryRoot =
            extract
                (field "gameresults"
                    [ ( "orderBy", Arg.enum "time_ASC" )
                    , ( "where"
                      , Arg.object
                            [ ( "tournament"
                              , Arg.object
                                    [ ( "slug", Arg.variable slugVar ) ]
                              )
                            ]
                      )
                    ]
                    (list gameresultMeta)
                )
    in
    queryDocument queryRoot


loadGameResultMeta : Model -> String -> Cmd Msg
loadGameResultMeta model slug =
    sendQueryRequest model (request { slug = slug } gameResultMetaQuery)
        |> Task.attempt GameResultMetaResponse


gamesQuery : Document Query (List Game) { vars | slug : String }
gamesQuery =
    let
        slugVar =
            Var.required "slug" .slug Var.string

        game =
            object Game
                |> with (field "id" [] string)
                |> with (field "name" [] string)
                |> with (field "bggid" [] int)
                |> with (field "weight" [] float)
                |> with (field "thumbnail" [] string)
                |> with (field "playTime" [] int)
                |> with (field "maxPlayers" [] int)

        queryRoot =
            extract
                (field "bggInfoes"
                    [ ( "where"
                      , Arg.object
                            [ ( "gameresults_some"
                              , Arg.object
                                    [ ( "tournament"
                                      , Arg.object
                                            [ ( "slug", Arg.variable slugVar ) ]
                                      )
                                    ]
                              )
                            ]
                      )
                    ]
                    (list game)
                )
    in
    queryDocument queryRoot


loadGames : Model -> String -> Cmd Msg
loadGames model slug =
    sendQueryRequest model (request { slug = slug } gamesQuery)
        |> Task.attempt GamesResponse


searchGameQuery : Document Query (List GameSearchResult) { vars | search : String }
searchGameQuery =
    let
        searchVar =
            Var.required "search" .search Var.string

        searchResult =
            object GameSearchResult
                |> with (field "bggid" [] int)
                |> with (field "year" [] int)
                |> with (field "name" [] string)

        queryRoot =
            extract
                (field "searchBggGame"
                    [ ( "search", Arg.variable searchVar )
                    ]
                    (list searchResult)
                )
    in
    queryDocument queryRoot


searchGame : Model -> (GraphQLGameSearchResponse -> Msg) -> String -> Cmd Msg
searchGame model msg search =
    sendQueryRequest model (request { search = search } searchGameQuery)
        |> Task.attempt msg


loadGameInfoQuery : Document Query GameInfo { vars | bggid : Int }
loadGameInfoQuery =
    let
        bggidVar =
            Var.required "bggid" .bggid Var.int

        gameInfo =
            object GameInfo
                |> with (field "bggid" [] int)
                |> with (field "id" [] string)
                |> with (field "name" [] string)
                |> with (field "weight" [] float)
                |> with (field "maxPlayers" [] int)
                |> with (field "thumbnail" [] string)
                |> with (field "description" [] string)
                |> with (field "rating" [] float)
                |> with (field "rank" [] int)
                |> with (field "ownedBy" [] int)
                |> with (field "playTime" [] int)
                |> with (field "yearPublished" [] int)

        queryRoot =
            extract
                (field "bggGameInfo"
                    [ ( "where"
                      , Arg.object [ ( "bggid", Arg.variable bggidVar ) ]
                      )
                    ]
                    gameInfo
                )
    in
    queryDocument queryRoot


loadGameInfo : Model -> (GraphQLGameInfoResponse -> Msg) -> Int -> Cmd Msg
loadGameInfo model msg bggid =
    sendQueryRequest model (request { bggid = bggid } loadGameInfoQuery)
        |> Task.attempt msg


setMemberRoleMutation : Document Mutation LeagueMember { vars | memberId : String, role : MemberRole }
setMemberRoleMutation =
    let
        memberIdVar =
            Var.required "id" .memberId Var.id

        roleVar =
            Var.required "role" .role leagueMemberRoleEnum

        queryRoot =
            extract
                (field "updateLeagueMember"
                    [ ( "where"
                      , Arg.object [ ( "id", Arg.variable memberIdVar ) ]
                      )
                    , ( "data"
                      , Arg.object [ ( "role", Arg.variable roleVar ) ]
                      )
                    ]
                    leagueMember
                )
    in
    mutationDocument queryRoot


setMemberRole : Model -> String -> MemberRole -> (GraphQLLeagueMemberResponse -> Msg) -> Cmd Msg
setMemberRole model memberId role msg =
    sendMutationRequest model (request { memberId = memberId, role = role } setMemberRoleMutation)
        |> Task.attempt msg
