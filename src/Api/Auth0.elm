module Api.Auth0 exposing (authGet, decodeUserProfile, loadUserProfile, login)

import Http
import Json.Decode as D
import Jwt
import Model.Flags exposing (Flags)
import Model.LoginData exposing (JwtToken, LoginProvider(..), LoginStatus(..))
import Model.Main exposing (Model)
import Model.User exposing (Auth0UserProfile)
import Msgs.Main exposing (..)
import Ports.LocalStorage.LocalStorage as LocalStorage
import RemoteData exposing (RemoteData(..))
import Routing


login : Model -> Cmd Msg
login model =
    Routing.reverseRoute model.route |> LocalStorage.saveRedirectUrl


decodeUserProfile : D.Decoder Auth0UserProfile
decodeUserProfile =
    D.map2 Auth0UserProfile
        (D.field "picture" D.string)
        (D.field "nickname" D.string)


loadUserProfile : Model -> Cmd Msg
loadUserProfile model =
    case model.loginStatus of
        LoggedIn token ->
            authGet model.flags token "/userinfo" decodeUserProfile
                |> RemoteData.sendRequest
                |> Cmd.map Auth0UserProfileResponse

        _ ->
            Cmd.none


authGet : Flags -> String -> String -> D.Decoder a -> Http.Request a
authGet flags token endpoint decoder =
    Jwt.get token (flags.auth0Domain ++ endpoint) decoder
