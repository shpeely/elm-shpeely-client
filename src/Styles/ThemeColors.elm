module Styles.ThemeColors exposing (body, bodyColor, colorDanger, colorPrimary, danger, greyDark, greyDarkColor, greyLight, greyLightColor, primary, toColor)

import Color exposing (Color)
import Color.Convert exposing (hexToColor)


primary : String
primary =
    "#2c3e50"


danger : String
danger =
    "#e74c3c"


body : String
body =
    "#292b2c"


greyLight : String
greyLight =
    "#636c72"


greyDark : String
greyDark =
    "#292b2c"


colorPrimary : Color
colorPrimary =
    toColor primary


colorDanger : Color
colorDanger =
    toColor danger


bodyColor : Color
bodyColor =
    toColor body


greyLightColor : Color
greyLightColor =
    toColor greyLight


greyDarkColor : Color
greyDarkColor =
    toColor greyDark


toColor : String -> Color
toColor hex =
    case hexToColor hex of
        Result.Ok color ->
            color

        Result.Err _ ->
            -- TODO
            Color.black
