module ErrorHandler exposing (handleGraphQLError, handleHttpError, handleRequestError)

import Bootstrap.Modal as Modal
import GraphQL.Client.Http as GraphQLClient
import Http exposing (Error(..))
import Model.Main exposing (..)
import Model.Route exposing (..)
import Msgs.LoginModalMsgs exposing (LoginModalMsg(..))
import Msgs.Main exposing (Msg)
import RemoteData exposing (WebData)
import Update.LoginModalUpdate as LoginModalUpdate exposing (update)


handleRequestError : WebData a -> ( Model, Cmd msg ) -> ( Model, Cmd msg )
handleRequestError webData ( model, cmd ) =
    case webData of
        RemoteData.Failure err ->
            case err of
                BadPayload s r ->
                    ( { model | error = s, modalState = Modal.shown }, cmd )

                NetworkError ->
                    ( { model | route = OfflineRoute }, cmd )

                BadStatus result ->
                    ( { model
                        | error = "Oops, something went wrong. Got a " ++ String.fromInt result.status.code
                        , modalState = Modal.shown
                      }
                    , cmd
                    )

                _ ->
                    ( model, cmd )

        _ ->
            ( model, cmd )


handleHttpError : Http.Error -> ( Model, Cmd msg ) -> ( Model, Cmd msg )
handleHttpError err ( model, cmd ) =
    case err of
        BadPayload s r ->
            ( { model | error = s, modalState = Modal.shown }, cmd )

        NetworkError ->
            ( { model | route = OfflineRoute }, cmd )

        _ ->
            ( model, cmd )


handleGraphQLError : Result GraphQLClient.Error a -> ( Model, Cmd Msg ) -> ( Model, Cmd Msg )
handleGraphQLError result ( model, cmd ) =
    case result of
        Result.Err err ->
            case err of
                GraphQLClient.HttpError httpErr ->
                    handleHttpError httpErr ( model, cmd )

                GraphQLClient.GraphQLError graphQlErrors ->
                    case List.any (.message >> (==) "LOGIN_REQUIRED") graphQlErrors of
                        True ->
                            LoginModalUpdate.update ShowModal model

                        False ->
                            let
                                errorMessages messages =
                                    List.map .message messages
                                        |> String.join "\n"
                            in
                            ( { model | error = errorMessages graphQlErrors, modalState = Modal.shown }, cmd )

        Result.Ok _ ->
            ( model, cmd )
